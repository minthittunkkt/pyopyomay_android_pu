package com.keoeoetech.pyopyomay;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.dialog.ImageViewDialog;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import io.realm.Realm;

public class NewsFeedByCategoryDetailActivity extends BaseActivity {


    @BindView(R.id.tv_newsfeed_by_category_detail_title)
    MyanTextView tv_newsfeed_by_category_detail_title;
    @BindView(R.id.tv_newsfeed_by_category_detail_category_name)
    MyanTextView tv_newsfeed_by_category_detail_category_name;
    @BindView(R.id.tv_newsfeed_by_category_detail_date)
    MyanTextView tv_newsfeed_by_category_detail_date;

    @BindView(R.id.layout_news_feed_by_category_detail_video_view)
    RelativeLayout layout_news_feed_by_category_detail_video_view;
    @BindView(R.id.new_feed_by_category_detail_image)
    ImageView new_feed_by_category_detail_image;
    @BindView(R.id.news_feed_by_category_detail_play_button)
    ImageView news_feed_by_category_detail_play_button;
    @BindView(R.id.newsfeed_by_category_detail_content)
    MyanTextView newsfeed_by_category_detail_content;

    ArticleModel articleModel;
    Realm realm;
    MyDateFormat myDateFormat;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_news_feed_by_category_detail;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(true);
        setupToolbarText(" ");
        articleModel = getIntent().getParcelableExtra("obj");
        init();

    }

    private void init() {

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_DETAIL_ACTIVITY);

        myDateFormat = new MyDateFormat();
        tv_newsfeed_by_category_detail_category_name.setMyanmarText(articleModel.getCategoryName());
        try {
            tv_newsfeed_by_category_detail_date.setMyanmarText(myDateFormat.DATE_FORMAT_DMY_TEXT.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(articleModel.getCreateDate().replace("T", " "))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tv_newsfeed_by_category_detail_title.setMyanmarText(articleModel.getTitle());
        newsfeed_by_category_detail_content.setMovementMethod(LinkMovementMethod.getInstance());
        newsfeed_by_category_detail_content.setMyanmarText(articleModel.getContent());

        if (articleModel.getArticleType() != null) {
            switch (articleModel.getArticleType()) {
                case PyoPyoMayConstant.PHOTO:
                    news_feed_by_category_detail_play_button.setVisibility(View.GONE);

                    int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(this)
                            .load(articleModel.getPhoto())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerInside()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(new_feed_by_category_detail_image);

                    break;
                case PyoPyoMayConstant.VIDEO:

                    news_feed_by_category_detail_play_button.setVisibility(View.VISIBLE);
                    size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(this)
                            .load(articleModel.getThumbnail())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(new_feed_by_category_detail_image);

                    break;

                default:
                    break;
            }
        } else {
            return;
        }


        System.out.println("Video Title :" + articleModel.getTitle());

        layout_news_feed_by_category_detail_video_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (articleModel.getArticleType()) {
                    case PyoPyoMayConstant.PHOTO:
                        ImageViewDialog dialog = ImageViewDialog.newInstance(articleModel.getPhoto());
                        dialog.show(getSupportFragmentManager(), dialog.getClass().getName());
                        break;
                    case PyoPyoMayConstant.VIDEO:
                        Intent intentPlayer = new Intent(getApplicationContext(), PlayerActivity.class);
                        intentPlayer.putExtra(PlayerActivity.VIDEO_URL, articleModel.getVideoUrl());
                        intentPlayer.putExtra(PlayerActivity.VIDEO_TITLE, articleModel.getTitle());
                        startActivity(intentPlayer);
                        break;
                    default:
                        break;
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.favorite_menu, menu);

        if (isSaved(articleModel.getUniqueKey())) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.mipmap.like_pink_64));

        } else {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.mipmap.like_grey_64));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {

            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_DETAIL_ACTIVITY,
                    PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_DETAIL_CLICK,
                    String.valueOf(articleModel.getUniqueKey()));

            if (isSaved(articleModel.getUniqueKey())) {

                item.setIcon(R.mipmap.like_grey_64);

                realm = Realm.getDefaultInstance();
                final ArticleModel results = realm.where(ArticleModel.class)
                        .equalTo("UniqueKey", articleModel.getUniqueKey()).findFirst();

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        results.deleteFromRealm();
                    }
                });

            } else {
                item.setIcon(R.mipmap.like_pink_64);

                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(articleModel);
                realm.commitTransaction();
            }


            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean isSaved(String UniqueKey) {
        ArticleModel model = Realm.getDefaultInstance().where(ArticleModel.class).equalTo("UniqueKey", UniqueKey).findFirst();

        if (model == null) {
            return false;
        } else {
            return true;
        }

    }

}



