package com.keoeoetech.pyopyomay;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;
import android.widget.LinearLayout;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.keoeoetech.pyopyomay.adapter.NewsFeedByCategoryAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.common.EndlessRecyclerViewScrollListener;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.CheckScreenSizeHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.CardModel;
import com.keoeoetech.pyopyomay.model.NewsFeedCustomModel;
import com.keoeoetech.pyopyomay.model.NewsFeedModel;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsFeedByCategoryActivity extends BaseActivity implements
        NewsFeedByCategoryAdapter.OnNewsFeedBtnClickedListener,
        SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.rv_newsfeed_by_category)
    ShimmerRecyclerView rv_newsfeed_by_category;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    private ServiceHelper.ApiService service;
    private Call<NewsFeedCustomModel> newsFeedCustomModelCall;
    private NewsFeedByCategoryAdapter newsFeedByCategoryAdapter;
    CardModel cardModel;
    private boolean isLoading;
    private boolean isEnd;
    private int page = 1;
    private int pageSize = 10;
    String categoryName = "";
    int ResId;
    String name;
    private Realm realm;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_news_feed_by_category;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(true);
        cardModel = (CardModel) getIntent().getSerializableExtra("obj");
        categoryName = cardModel.getCategoryNameForAPI();
        setupToolbarText(" ");

        init();
        getHeader();
        getNewsFeedByCategoryData();

    }

    private void getHeader() {

        newsFeedByCategoryAdapter.addHeader(cardModel);
    }

    private void init() {

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_ACTIVITY);

        service = ServiceHelper.getClient(this);
        newsFeedByCategoryAdapter = new NewsFeedByCategoryAdapter();
        swipe_refresh_layout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        swipe_refresh_layout.setOnRefreshListener(this);
        rv_newsfeed_by_category.setHasFixedSize(true);

        tv_empty.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_NO_DATA_AVAILABLE));

        if (CheckScreenSizeHelper.isTablet(this)) {
            rv_newsfeed_by_category.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            rv_newsfeed_by_category.setLayoutManager(new GridLayoutManager(this, 1));
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_newsfeed_by_category.setLayoutManager(linearLayoutManager);
        rv_newsfeed_by_category.setAdapter(newsFeedByCategoryAdapter);

        rv_newsfeed_by_category.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager)
                rv_newsfeed_by_category.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isLoading && !isEnd) {
                    getNewsFeedByCategoryData();
                } else if (isEnd) {
                    //showToastMsg(getString(R.string.string_no_more_data));
                }
            }
        });

        newsFeedByCategoryAdapter.setListener(this);
    }


    public void getNewsFeedByCategoryData() {
        if (page == 1) {
            rv_newsfeed_by_category.showShimmerAdapter();
        }
        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        newsFeedByCategoryAdapter.clearFooter();
        rv_newsfeed_by_category.post(() -> {
            newsFeedByCategoryAdapter.showLoading();
            emptyView.setVisibility(View.GONE);
        });

        newsFeedCustomModelCall = service.getNewsFeedByCategory(categoryName, page, pageSize);
        newsFeedCustomModelCall.enqueue(new Callback<NewsFeedCustomModel>() {
            @Override
            public void onResponse(Call<NewsFeedCustomModel> call, Response<NewsFeedCustomModel> response) {


                if (page == 1) {
                    rv_newsfeed_by_category.hideShimmerAdapter();
                }
                isLoading = false;
                newsFeedByCategoryAdapter.clearFooter();

                if (response.isSuccessful()) {
                    NewsFeedCustomModel result = response.body();
                    ArrayList<NewsFeedModel> resultList = result.getResults();


                    if (page == 1 && resultList.isEmpty()) {
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        emptyView.setVisibility(View.GONE);
                    }

                    isEnd = result.getResults().isEmpty();
                    for (int i = 0; i < resultList.size(); i++) {

                        if(resultList.get(i).getAds()!=null) {
                            newsFeedByCategoryAdapter.add(resultList.get(i).getAds());
                        }
                        newsFeedByCategoryAdapter.add(resultList.get(i).getArticle());

                    }

                    page++;

                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<NewsFeedCustomModel> call, Throwable t) {
                handleFailure();
            }


            public void handleFailure() {
                newsFeedByCategoryAdapter.clearFooter();
                newsFeedByCategoryAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, new BaseAdapter.OnRetryListener() {
                    @Override
                    public void onRetry() {
                        getNewsFeedByCategoryData();
                    }
                });
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (newsFeedCustomModelCall != null) {
            newsFeedCustomModelCall.cancel();
        }
    }


    @Override
    public void onRefresh() {
        page = 1;
        newsFeedByCategoryAdapter.clear();
        getHeader();
        getNewsFeedByCategoryData();
    }

    public void onVideoClicked(String videoUrl, String videoTitle) {
        Intent intentPlayer = new Intent(this, PlayerActivity.class);
        intentPlayer.putExtra(PlayerActivity.VIDEO_URL, videoUrl);
        intentPlayer.putExtra(PlayerActivity.VIDEO_TITLE, videoTitle);
        startActivity(intentPlayer);

    }

    @Override
    protected void onResume() {
        super.onResume();
        newsFeedByCategoryAdapter.notifyDataSetChanged();
    }
}
