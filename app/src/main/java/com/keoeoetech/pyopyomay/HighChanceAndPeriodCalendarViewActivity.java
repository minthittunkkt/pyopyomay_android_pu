package com.keoeoetech.pyopyomay;

import android.os.Bundle;
import android.util.Log;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import io.realm.Realm;

public class HighChanceAndPeriodCalendarViewActivity extends BaseActivity {

    @BindView(R.id.calendarView)
    CalendarView calendarView;

    Realm realm;
    PeriodTrackerModel periodTrackerModel;
    MyDateFormat myDateFormat;
    int cycleDays;
    Calendar current;
    int estimateStartDay;
    private Date lmp;
    private Calendar temp;
    private Date periodStartLmp;
    private Calendar periodStartTemp;
    private ArrayList<Calendar> dateList;
    private ArrayList<Calendar> highChanceList;
    private ArrayList<Calendar> periodStartDayList;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_highchance_and_period_calendar_view;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        highChanceAndPeriodDayCalculation();

    }


    public void init() {
        setupToolbar(true);
        realm = Realm.getDefaultInstance();
        periodTrackerModel = realm.where(PeriodTrackerModel.class).findFirst();
        current = Calendar.getInstance();
        dateList = new ArrayList<>();
        periodStartDayList = new ArrayList<>();
        highChanceList = new ArrayList<>();
        myDateFormat = new MyDateFormat();
        cycleDays = periodTrackerModel.getPCycle() - 1;
        estimateStartDay = periodTrackerModel.getPCycle() - 19;

        try {
            lmp = myDateFormat.DATE_FORMAT_DMY.parse(periodTrackerModel.getLMD());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        temp = Calendar.getInstance();
        temp.setTime(lmp);

        try {
            periodStartLmp = myDateFormat.DATE_FORMAT_DMY.parse(periodTrackerModel.getLMD());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        periodStartTemp = Calendar.getInstance();
        periodStartTemp.setTime(periodStartLmp);


    }


    private void highChanceAndPeriodDayCalculation() {

        Calendar current = Calendar.getInstance();
        current.setTime(temp.getTime());
        dateList.add(current);
        periodStartDayList.add(current);


        for (int i = 0; i < 14; i++) {
            temp.add(Calendar.DAY_OF_MONTH, cycleDays);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp.getTime());
            dateList.add(calendar);
        }


        for (int i = 0; i < 14; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateList.get(i).getTime());
            calendar.add(Calendar.DAY_OF_MONTH, estimateStartDay);
            highChanceList.add(calendar);
        }


        for (int i = 0; i < 14; i++) {
            periodStartTemp.add(Calendar.DAY_OF_MONTH, cycleDays);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(periodStartTemp.getTime());
            periodStartDayList.add(calendar);

        }
        getSelectedDays();

        //calendarView.setSelectedDates(getSelectedDays());

        Log.d("HighChanceDate", highChanceList.size() + " ");

        Log.d("Dates", dateList.size() + " ");


    }


    private void getSelectedDays() {


        List<EventDay> periodEvents = new ArrayList<>();


        for (int j = 0; j < 11; j++) {

            for (int i = 0; i < periodTrackerModel.getPLength(); i++) {
                Calendar periodOne = DateUtils.getCalendar();
                periodOne.setTime(periodStartDayList.get(j).getTime());
                periodOne.add(Calendar.DAY_OF_MONTH, i);
                periodEvents.add(new EventDay(periodOne, R.drawable.sample_circle));
            }
            for (int i = 0; i < 8; i++) {
                Calendar one = DateUtils.getCalendar();
                one.setTime(highChanceList.get(j).getTime());
                one.add(Calendar.DAY_OF_MONTH, i);
                periodEvents.add(new EventDay(one, R.drawable.sample_rectangle));
            }
            calendarView.setEvents(periodEvents);

        }




    }

}


