package com.keoeoetech.pyopyomay;

import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;

import com.keoeoetech.pyopyomay.common.BaseActivity;

import butterknife.BindView;

public class CSOCategoryActivity extends BaseActivity {

    @BindView(R.id.cv_health)
    CardView cv_health;
    @BindView(R.id.cv_weak)
    CardView cv_weak;
    @BindView(R.id.cv_education)
    CardView cv_education;
    /*@BindView(R.id.cv_employee)
    CardView cv_employee;*/
    @BindView(R.id.cv_law)
    CardView cv_law;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_csocategory;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        clickEvent();

    }

    private void init() {
        setupToolbar(true);
    }

    private void clickEvent() {
        cv_health.setOnClickListener(v -> {

            Intent intent = new Intent(this, CSODirectoryActivity.class);
            intent.putExtra("category", getString(R.string.str_health_category));
            startActivity(intent);

        });

        cv_weak.setOnClickListener(v -> {

            Intent intent = new Intent(this, CSODirectoryActivity.class);
            intent.putExtra("category", getString(R.string.str_weak_category));
            startActivity(intent);

        });

        cv_education.setOnClickListener(v -> {

            Intent intent = new Intent(this, CSODirectoryActivity.class);
            intent.putExtra("category", getString(R.string.str_education_category));
            startActivity(intent);

        });

       /* cv_employee.setOnClickListener(v -> {

            Intent intent = new Intent(this, CSODirectoryActivity.class);
            intent.putExtra("category", getString(R.string.str_employee_category));
            startActivity(intent);

        });*/

        cv_law.setOnClickListener(v -> {

            Intent intent = new Intent(this, CSODirectoryActivity.class);
            intent.putExtra("category", getString(R.string.str_law_category));
            startActivity(intent);

        });
    }


}