package com.keoeoetech.pyopyomay;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DialogHelperWithOkCancelButton;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PeriodAlarmHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.NotificationModel;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import butterknife.BindView;
import io.realm.Realm;

public class PeriodTrackerActivity extends BaseActivity {


    final Context context = this;
    @BindView(R.id.tv_period_start_message)
    MyanTextView tv_period_start_message;
    @BindView(R.id.btn_period_start)
    MyanButton btn_period_start;
    @BindView(R.id.tv_highChanceStartDate)
    MyanTextView tv_highChanceStartDate;
    @BindView(R.id.label_highChanceStartDate)
    MyanTextView label_highChanceStartDate;
    @BindView(R.id.label_last_period_day)
    MyanTextView label_last_period_day;
    @BindView(R.id.tv_last_period_day)
    MyanTextView tv_last_period_day;
    @BindView(R.id.label_period_suggestion_date)
    MyanTextView label_period_suggestion_date;
    @BindView(R.id.tv_period_suggestion_date)
    MyanTextView tv_period_suggestion_date;
    @BindView(R.id.label_ovulation_date)
    MyanTextView label_ovulation_date;
    @BindView(R.id.tv_ovulation_date)
    MyanTextView tv_ovulation_date;
    @BindView(R.id.label_period_cycle)
    MyanTextView label_period_cycle;
    @BindView(R.id.tv_period_cycle)
    MyanTextView tv_period_cycle;
    @BindView(R.id.label_period_duration)
    MyanTextView label_period_duration;
    @BindView(R.id.tv_period_duration)
    MyanTextView tv_period_duration;
    @BindView(R.id.tv_period_left_day)
    MyanBoldTextView tv_period_left_day;
    @BindView(R.id.pg_bar)
    ProgressBar pg_bar;
    @BindView(R.id.linear_main)
    LinearLayout linear_main;
    @BindView(R.id.cv_highchance_calendar_show)
    CardView cv_highchance_calendar_show;
    @BindView(R.id.label_highChanceStartDateCalendar)
    MyanTextView label_highChanceStartDateCalendar;
    @BindView(R.id.btn_period_menu)
    MyanButton btn_period_menu;
    PeriodTrackerModel oldModel;
    Realm realm;
    String periodStartDate, ovulationDate, highChanceStartDate,
            highChanceEndDate,
            selectedDate, lastPeriodStartDate, highChanceEndDateText,
            highChanceStartDateText, ovulationDateText, periodStartDateText;
    int estimateStartDay, estimateEndDay;
    String periodText;
    int highchanceStartDay, cycleday;
    Date periodDate, todayDate;
    long difference, daysBetween;
    Calendar cal;

    DialogHelperWithOkCancelButton dialogHelperWithOkCancelButton;
    String id, tv;
    ProgressDialog progressDialog;
    Handler handler;
    Calendar selectedCalendar;
    DatePicker datePicker;
    private MyDateFormat myDateFormat;
    private PeriodAlarmHelper periodAlarmHelper;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_period_tracker;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getNoti();
        setMyanmarText();

    }

    private void init() {

        setupToolbar(true);
        setupToolbarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_WOMAN_HEALTH));

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading....");
        handler = new Handler();

        periodAlarmHelper = new PeriodAlarmHelper(this);
        dialogHelperWithOkCancelButton = new DialogHelperWithOkCancelButton(this);
        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.PERIOD_TRACKER_ACTIVITY);
        realm = Realm.getDefaultInstance();
        myDateFormat = new MyDateFormat();
        cal = Calendar.getInstance();
        todayDate = Calendar.getInstance().getTime();

        btn_period_menu.setOnClickListener(v -> {

            Intent intent = new Intent(this,
                    PeriodActivity.class);
            startActivity(intent);
        });


    }


    private void getNoti() {
        tv = getIntent().getStringExtra("activity");
        if (getIntent().getStringExtra("activity") != null) {
            if (tv.equals("noti")) {
                id = getIntent().getStringExtra("id");
                Log.d("THN", "PTA id " + id);
                getDetail(id);
            }
        }
    }

    private void getDetail(String id) {

        NotificationModel model = realm.where(NotificationModel.class)
                .equalTo("id", id).findFirst();

        realm.beginTransaction();
        model.setSeen(true);
        realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();

    }


    private void setMyanmarText() {

        label_last_period_day.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_LAST_DAY_OF_PERIOD));
        label_ovulation_date.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_OVULATION_DATE));
        label_period_duration.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_DURATION));
        label_period_suggestion_date.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_SUGGESTION_DATE));
        label_period_cycle.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_CYCLE));
        btn_period_start.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_BTN_PERIOD_START));
        label_highChanceStartDate.setMyanmarText(getResources().getString(R.string.lbl_tv_highchance_day));
        //label_periodStartDateCalendar.setMyanmarText(getResources().getString(R.string.lbl_periodStartCalendarShow));
        label_highChanceStartDateCalendar.setMyanmarText(getResources().getString(R.string.lbl_highChance_and_period_calendar_Show));


    }

    private void showData() {

        linear_main.setVisibility(View.GONE);
        pg_bar.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(() -> {

            pg_bar.setVisibility(View.GONE);
            linear_main.setVisibility(View.VISIBLE);

        }, 1000);

        oldModel = realm.where(PeriodTrackerModel.class).findFirst();

        estimateStartDay = oldModel.getPCycle() - 18;
        estimateEndDay = oldModel.getPCycle() - 11;

        cycleday = oldModel.getPCycle() - 1;

        Calendar lsdCalendar = Calendar.getInstance();
        try {
            lsdCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(oldModel.getLMD()));
            lastPeriodStartDate = myDateFormat.DATE_FORMAT_DMY_TEXT.format(lsdCalendar.getTime());
            lsdCalendar.add(Calendar.DATE, cycleday);
            periodStartDateText = myDateFormat.DATE_FORMAT_DMY_TEXT.format(lsdCalendar.getTime());
            periodStartDate = myDateFormat.DATE_FORMAT_DMY.format(lsdCalendar.getTime());
            periodDate = myDateFormat.DATE_FORMAT_DMY.parse(periodStartDate);
            todayDate = Calendar.getInstance().getTime();

            difference = periodDate.getTime() - todayDate.getTime();
            daysBetween = (difference / (1000 * 60 * 60 * 24));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        String periodLeftDay = daysBetween + " ရက်";

        if (daysBetween == 0) {
            periodText = "သင်၏ရာသီစက်ဝန်း မနက်ဖြန်တွင်စတင်ပါမည်။";
            tv_period_left_day.setVisibility(View.GONE);

        } else {

            if (daysBetween > 0) {
                periodText = "ရာသီလာရန် <font color=#F1958D>" + daysBetween + "</font> ရက်သာလိုပါသည်။";
                tv_period_left_day.setVisibility(View.VISIBLE);
                tv_period_left_day.setTextSize(18);
                tv_period_left_day.setMyanmarText(periodLeftDay);
            } else {
                periodText = "ရာသီလာရန် " + periodLeftDay.replace("-", "") + " ကျော်လွန်နေပါပြီ။";
                tv_period_left_day.setVisibility(View.VISIBLE);
                tv_period_left_day.setTextSize(14);
                tv_period_left_day.setMyanmarText(periodLeftDay.replace("-", "") + "\nကျော်နေပြီနော်");
            }

        }


        tv_period_start_message.setMyanmarHtmlText(periodText);

        Calendar highChanceStartDateCalendar = Calendar.getInstance();
        Calendar higChanceEndDateCalendar = Calendar.getInstance();

        try {
            highChanceStartDateCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(oldModel.getLMD()));
            highChanceStartDateCalendar.add(Calendar.DATE, estimateStartDay - 1);
            highChanceStartDate = myDateFormat.DATE_FORMAT_DMY.format(highChanceStartDateCalendar.getTime());
            highChanceStartDateText = myDateFormat.DATE_FORMAT_DMY_TEXT.format(highChanceStartDateCalendar.getTime());

            higChanceEndDateCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(highChanceStartDate));
            higChanceEndDateCalendar.add(Calendar.DATE, 7);
            highChanceEndDate = myDateFormat.DATE_FORMAT_DMY.format(higChanceEndDateCalendar.getTime());
            highChanceEndDateText = myDateFormat.DATE_FORMAT_DMY_TEXT.format(higChanceEndDateCalendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        highchanceStartDay = highChanceStartDateCalendar.get(Calendar.DATE);

        Log.d("THN", "HighChanceStartDay  " + highchanceStartDay);


        String highChanceStartText = highChanceStartDateText + " မှ " +
                highChanceEndDateText
                + " အထိ";
        tv_highChanceStartDate.setMyanmarHtmlText(highChanceStartText);

        Calendar ovulationCalendar = Calendar.getInstance();

        try {
            ovulationCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(highChanceEndDate));
            ovulationCalendar.add(Calendar.DATE, -1);
            ovulationDate = myDateFormat.DATE_FORMAT_DMY.format(ovulationCalendar.getTime());
            ovulationDateText = myDateFormat.DATE_FORMAT_DMY_TEXT.format(ovulationCalendar.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        tv_last_period_day.setMyanmarText(lastPeriodStartDate);
        tv_period_suggestion_date.setMyanmarText(periodStartDateText);
        tv_ovulation_date.setMyanmarText(ovulationDateText);
        tv_period_cycle.setMyanmarText(oldModel.getPCycle() + " ရက်");
        tv_period_duration.setMyanmarText(oldModel.getPLength() + " ရက်");


        btn_period_start.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.custom_dialog_with_datepicker);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            datePicker = dialog.findViewById(R.id.datePicker);
            MyanButton btn_ok = dialog.findViewById(R.id.btn_ok);
            MyanButton btn_cancel = dialog.findViewById(R.id.btn_cancel);


            btn_ok.setOnClickListener(v -> {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year = datePicker.getYear();

                selectedCalendar = Calendar.getInstance();
                selectedCalendar.set(year, month, day);

                selectedDate = myDateFormat.DATE_FORMAT_DMY.format
                        (selectedCalendar.getTime());


                Log.d("THN", "Date" + selectedDate);

                if (oldModel == null) {
                    //add new
                    realm.beginTransaction();
                    PeriodTrackerModel periodTrackerModel = getDataFromRealm();
                    periodTrackerModel.setId(UUID.randomUUID().toString());
                    periodTrackerModel.setLMD(oldModel.getLMD());
                    realm.copyToRealmOrUpdate(periodTrackerModel); //close the database
                    realm.commitTransaction();
                    setPeriodAlarm();

                } else {
                    //update
                    realm.beginTransaction();
                    PeriodTrackerModel periodTrackerModel = getDataFromRealm();
                    periodTrackerModel.setId(oldModel.getId());
                    periodTrackerModel.setLMD(selectedDate);
                    realm.copyToRealmOrUpdate(periodTrackerModel); //close the database
                    realm.commitTransaction();
                    setPeriodAlarm();

                }
                dialog.dismiss();
                showData();

            });
            btn_cancel.setOnClickListener(v -> dialog.dismiss());


            dialog.show();

        });


        cv_highchance_calendar_show.setOnClickListener(v -> {

            Intent highChanceCalendarIntent = new Intent(getApplicationContext(), HighChanceAndPeriodCalendarViewActivity.class);
            highChanceCalendarIntent.putExtra("obj", oldModel);
            startActivity(highChanceCalendarIntent);
        });


    }

    public void setPeriodAlarm() {
        Log.d("THN", "It is first period alarm in PT.");

        oldModel = realm.where(PeriodTrackerModel.class).findFirst();

        if (oldModel != null) {
            Calendar lsdCalendar = Calendar.getInstance();
            try {
                lsdCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(oldModel.getLMD()));
                lsdCalendar.add(Calendar.DATE, oldModel.getPCycle());
                periodStartDate = myDateFormat.DATE_FORMAT_DMY.format(lsdCalendar.getTime());
                periodDate = myDateFormat.DATE_FORMAT_DMY.parse(periodStartDate);
                periodAlarmHelper.startAlarm(periodDate);


            } catch (ParseException e) {
                e.printStackTrace();
            }


        }

    }

    public PeriodTrackerModel getDataFromRealm() {
        PeriodTrackerModel model = new PeriodTrackerModel();
        model.setPLength(oldModel.getPLength());
        model.setPCycle(oldModel.getPCycle());
        return model;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (id != null) {
            getDetail(id);
        }

        if (Realm.getDefaultInstance().where(PeriodTrackerModel.class).count() == 0) {
            dialogHelperWithOkCancelButton.showDialog(R.mipmap.period_data_fill, "အချက်အလက်ဖြည့်မည်", "အသေးစိတ်ဖြည့်ပါ", "ထွက်မည်");
            dialogHelperWithOkCancelButton.getBtnOk().setOnClickListener(v -> {

                dialogHelperWithOkCancelButton.hideDialog();
                Intent intent = new Intent(PeriodTrackerActivity.this, PeriodActivity.class);
                startActivity(intent);

            });

            dialogHelperWithOkCancelButton.getBtnCancel().setOnClickListener(v -> {
                dialogHelperWithOkCancelButton.hideDialog();
                finish();
            });
        } else {
            showData();
        }

    }

}

