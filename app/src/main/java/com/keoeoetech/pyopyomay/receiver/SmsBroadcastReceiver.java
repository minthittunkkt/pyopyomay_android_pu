package com.keoeoetech.pyopyomay.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "SmsBroadcastReceiver";
    OTPReceiverInterface otpReceiveInterface = null;

    public void setOtpReceiveInterface(OTPReceiverInterface otpReceiveInterface) {
        this.otpReceiveInterface = otpReceiveInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive: ");
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status mStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            switch (mStatus.getStatusCode()) {

                case CommonStatusCodes.SUCCESS:
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    Log.d(TAG, "onReceive: failure " + message);
                    if (otpReceiveInterface != null) {
                        otpReceiveInterface.onOTPReceived(message);
                        Log.d(TAG,"OTPMessage "+message);
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    Log.d(TAG, "onReceive: failure");
                    if (otpReceiveInterface != null) {
                        otpReceiveInterface.onOTPTimeOut();
                    }
                    break;


                case CommonStatusCodes.API_NOT_CONNECTED:
                    if (otpReceiveInterface != null) {
                        otpReceiveInterface.onOTPReceivedError("API NOT CONNECTED");
                    }
                    break;
                case CommonStatusCodes.NETWORK_ERROR:
                    if (otpReceiveInterface != null) {
                        otpReceiveInterface.onOTPReceivedError("NETWORK ERROR");
                    }
                    break;
                case CommonStatusCodes.ERROR:
                    if (otpReceiveInterface != null) {
                        otpReceiveInterface.onOTPReceivedError("SOME THING WENT WRONG");
                    }
                    break;

            }
        }
    }

    public interface OTPReceiverInterface {

        void onOTPReceived(String otp);

        void onOTPTimeOut();

        void onOTPReceivedError(String error);
    }
}