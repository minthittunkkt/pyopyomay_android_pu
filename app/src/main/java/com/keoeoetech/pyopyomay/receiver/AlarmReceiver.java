package com.keoeoetech.pyopyomay.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;


import com.keoeoetech.pyopyomay.MainActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.model.NotificationModel;

import java.util.Calendar;
import java.util.UUID;


import io.realm.Realm;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_BADGE_BROADCAST_INTENT;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_BADGE_BROADCAST_MESSAGE;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_TYPE_PERIOD_TRACKER;

public class AlarmReceiver extends BroadcastReceiver {
    private static final String CHANNEL_ID
            = "com.singhajit.notificationDemo.channelId";

    Realm realm;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra("noti", "xxx");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);

        Notification notification = builder.setContentTitle(MyanTextProcessor.
                processText(context, "အသိပေးချက်"))
                .setContentText(MyanTextProcessor.processText
                        (context, "သင့်ရာသီစက်ဝန်း မနက်ဖန် စတင်ပါမည်"))
                .setTicker("New Message Alert!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notification);

        MyDateFormat myDateFormat = new MyDateFormat();
        NotificationModel model = new NotificationModel();
        model.setId(UUID.randomUUID().toString());
        model.setContent("သင့်ရာသီစက်ဝန်း မနက်ဖန် စတင်ပါမည်");
        model.setDatetime(myDateFormat.DATE_FORMAT_YMD
                .format(Calendar.getInstance().getTime()));
        model.setType(NOTI_TYPE_PERIOD_TRACKER);
        model.setSeen(false);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();

        Intent i = new Intent(NOTI_BADGE_BROADCAST_INTENT)
                .putExtra(NOTI_BADGE_BROADCAST_MESSAGE, "Noti received.");
        context.sendBroadcast(i);

    }

}


