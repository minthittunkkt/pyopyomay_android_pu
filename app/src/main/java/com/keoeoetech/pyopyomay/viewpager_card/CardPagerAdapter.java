package com.keoeoetech.pyopyomay.viewpager_card;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.keoeoetech.pyopyomay.NewsFeedByCategoryActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.model.CardModel;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CardModel> mData;
    private float mBaseElevation;

    CardView cardView;

    public CardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CardModel item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.item_card_adapter, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        cardView = view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }


        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        cardView.setRadius(10);
        mViews.set(position, cardView);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(final CardModel cardModel, View view) {

        final Context context = view.getContext();
        CardView cardView=view.findViewById(R.id.cardView);
        LinearLayout linearLayout = view.findViewById(R.id.linear_card);
        MyanTextView titleText = view.findViewById(R.id.tv_title);
        ImageView imgCard = view.findViewById(R.id.imgCard);
        titleText.setMyanmarText(cardModel.getCategoryNameForAPI());
        imgCard.setImageResource(cardModel.getImageID());
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_FRAGMENT,
                        PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_CLICK, cardModel.getCategoryNameForAPI());

                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            }
        });


    }

}
