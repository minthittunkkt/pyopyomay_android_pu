package com.keoeoetech.pyopyomay;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.utils.DateUtils;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import io.realm.Realm;

public class PeriodStartDayCalendarViewActivity extends BaseActivity {

    @BindView(R.id.calendarView)
    CalendarView calendarView;
    Realm realm;
    PeriodTrackerModel periodTrackerModel;
    MyDateFormat myDateFormat;

    private Date lmp;
    private Calendar temp;
    private ArrayList<Calendar> dateList;
    int cycleDays;
    Calendar current;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_period_start_day_calendar_view;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {
        init();
        periodStartDayCalculation();
    }

    private void init() {

        setupToolbar(true);
        realm = Realm.getDefaultInstance();
        periodTrackerModel = realm.where(PeriodTrackerModel.class).findFirst();
        current=Calendar.getInstance();
        dateList = new ArrayList<>();
        myDateFormat = new MyDateFormat();
        cycleDays = periodTrackerModel.getPCycle() - 1;

        try {
            lmp = myDateFormat.DATE_FORMAT_DMY.parse(periodTrackerModel.getLMD());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        temp = Calendar.getInstance();
        temp.setTime(lmp);


       /* try {
            lmp = myDateFormat.DATE_FORMAT_DMY.parse(periodTrackerModel.getLMD());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        temp = Calendar.getInstance();
        temp.setTime(lmp);*/
    }





    private void periodStartDayCalculation() {

        Calendar current = Calendar.getInstance();
        current.setTime(temp.getTime());

        dateList.add(current);

        for (int i = 0; i < 14; i++) {
            temp.add(Calendar.DAY_OF_MONTH, cycleDays);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp.getTime());
            dateList.add(calendar);

        }
        calendarView.setSelectedDates(getSelectedDays());

        Log.d("Dates",dateList.size()+" ");

        /*Calendar current = Calendar.getInstance();
        current.setTime(temp.getTime());

        dateList.add(current);

        for (int i = 0; i < 11; i++) {
            temp.add(Calendar.DAY_OF_MONTH, cycleDays);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp.getTime());
            dateList.add(calendar);

        }
        calendarView.setSelectedDates(dateList);*/

    }


    private List<Calendar> getSelectedDays() {
        List<Calendar> calendars = new ArrayList<>();


        for (int i = 0; i < periodTrackerModel.getPLength(); i++) {
            Calendar one = DateUtils.getCalendar();
            one.setTime(dateList.get(0).getTime());
            one.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(one);

            Calendar two = DateUtils.getCalendar();
            two.setTime(dateList.get(1).getTime());
            two.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(two);

            Calendar three = DateUtils.getCalendar();
            three.setTime(dateList.get(2).getTime());
            three.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(three);

            Calendar four = DateUtils.getCalendar();
            four.setTime(dateList.get(3).getTime());
            four.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(four);

            Calendar five = DateUtils.getCalendar();
            five.setTime(dateList.get(4).getTime());
            five.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(five);

            Calendar six = DateUtils.getCalendar();
            six.setTime(dateList.get(5).getTime());
            six.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(six);

            Calendar seven = DateUtils.getCalendar();
            seven.setTime(dateList.get(6).getTime());
            seven.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(seven);

            Calendar eight = DateUtils.getCalendar();
            eight.setTime(dateList.get(7).getTime());
            eight.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(eight);

            Calendar nine = DateUtils.getCalendar();
            nine.setTime(dateList.get(8).getTime());
            nine.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(nine);

            Calendar ten = DateUtils.getCalendar();
            ten.setTime(dateList.get(9).getTime());
            ten.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(ten);

            Calendar eleven = DateUtils.getCalendar();
            eleven.setTime(dateList.get(10).getTime());
            eleven.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(eleven);

            Calendar twelve = DateUtils.getCalendar();
            twelve.setTime(dateList.get(11).getTime());
            twelve.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(twelve);

            Calendar thirteen = DateUtils.getCalendar();
            thirteen.setTime(dateList.get(12).getTime());
            thirteen.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(thirteen);

            Calendar forteen = DateUtils.getCalendar();
            forteen.setTime(dateList.get(13).getTime());
            forteen.add(Calendar.DAY_OF_MONTH,i);
            calendars.add(forteen);


        }

        return calendars;
    }

}


