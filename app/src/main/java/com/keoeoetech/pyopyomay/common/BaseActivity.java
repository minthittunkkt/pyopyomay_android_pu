package com.keoeoetech.pyopyomay.common;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.custom_control.MyanToast;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by user on 7/12/2015.
 * Removed warning code by AAM on 27-Jun-17
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Unbinder unbinder;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_text)
    MyanBoldTextView toolbar_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        unbinder = ButterKnife.bind(this);

        setUpContents(savedInstanceState);
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorTextColorPrimary));
    }

    protected void setupToolbar(boolean isChild) {

        if (toolbar != null)
            setSupportActionBar(toolbar);

        if (isChild) {
            if (getSupportActionBar() != null) {

//                final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
                final Drawable upArrow = ContextCompat.getDrawable(getApplicationContext(), R.mipmap.back_arrow);
                upArrow.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);


                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    protected void setupToolbarText(String text) {
        if (getSupportActionBar() != null) {

            //getSupportActionBar().setTitle(MyanTextProcessor.processText(getApplicationContext(), text));
            toolbar_text.setMyanmarText(text);
        }
    }


    protected void setupOptionMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            menuItem.setTitle(MyanTextProcessor.processText(getApplicationContext(), menuItem.getTitle().toString()));

            if (menu.getItem(i).getSubMenu() != null && menu.getItem(i).getSubMenu().size() > 0) {
                for (int j = 0; j < menu.getItem(i).getSubMenu().size(); j++) {
                    MenuItem subMenuItem = menu.getItem(i).getSubMenu().getItem(j);
                    subMenuItem.setTitle(MyanTextProcessor.processText(getApplicationContext(), subMenuItem.getTitle().toString()));
                }
            }
        }
    }

    protected void showToastMsg(String msg) {
        MyanToast.makeText(getBaseContext(), msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    protected abstract void setUpContents(Bundle savedInstanceState);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
