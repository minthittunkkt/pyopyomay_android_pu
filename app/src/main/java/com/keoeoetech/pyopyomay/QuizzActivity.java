package com.keoeoetech.pyopyomay;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import com.keoeoetech.pyopyomay.adapter.LessonAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.LessonModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;

public class QuizzActivity extends BaseActivity{


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_answer_lesson)
    MyanTextView tv_answer_lesson;

    private LessonAdapter lessonAdapter;
    private String type;
    private PyoPyoMayUserModel pyoPyoMayUserModel;
    Realm realm;



    @Override
    protected int getLayoutResource() {
        return R.layout.activity_quizz;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getAllLessons();

    }

    private void init() {

        setupToolbar(true);
        setupToolbarText(" ");

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.QUIZZ_ACTIVITY);

        tv_answer_lesson.setMyanmarText(TextDictionaryHelper.getText(this,TextDictionaryHelper.TEXT_PYOPYOMAY_ANSWER_QUIZZ));
        realm=Realm.getDefaultInstance();
        pyoPyoMayUserModel=realm.where(PyoPyoMayUserModel.class).findFirst();
        type=pyoPyoMayUserModel.getUserType();

        Log.d("Quizz Activity","User Type"+ type);
        lessonAdapter = new LessonAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(lessonAdapter);
    }


    private void getAllLessons() {
        lessonAdapter.clear();

        RealmResults<LessonModel> result = Realm.getDefaultInstance().where(LessonModel.class)
                .equalTo("Type",type)
                .findAll();
        for (int i = 0; i < result.size(); i++) {
            lessonAdapter.add(result.get(i));
        }
    }


}

