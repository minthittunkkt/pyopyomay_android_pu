package com.keoeoetech.pyopyomay;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.helper.CustomDialogHelper;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.model.ReferalResponseModel;
import com.keoeoetech.pyopyomay.model.ReferralModel;

import java.util.Calendar;

import me.dm7.barcodescanner.zbar.ZBarScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferralCodeScannerActivity extends AppCompatActivity
        implements ZBarScannerView.ResultHandler {


    String imei, referralDate = "";
    Call<ReferalResponseModel> callReferral;
    ServiceHelper.ApiService service;
    MyDateFormat myDateFormat;
    SharePreferenceHelper sharePreferenceHelper;
    Dialog myDialog;
    Calendar calendar;
    private ProgressDialog progressDialog;
    private CustomDialogHelper referralResultDialog;

    private ZBarScannerView mScannerView;
    Handler handler;
    ReferralModel referralModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);

        init();
    }


    private void init() {
        PyoPyoMay.getInstance().trackScreenView
                (PyoPyoMayConstant.REFERAL_CODE_SCANNER_ACTIVITY);

        referralResultDialog = new CustomDialogHelper(ReferralCodeScannerActivity.this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.setCancelable(false);

        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);
        calendar = Calendar.getInstance();
        myDateFormat = new MyDateFormat();
        referralDate = myDateFormat.DATE_FORMAT_YMD_HMS.format(calendar.getTime());
        handler = new Handler();
    }

    private void confirmDialogBox(final ReferralModel model) {
        myDialog = new Dialog(this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.dialog_box_referral);
        final MyanButton btn_yes = myDialog.findViewById(R.id.btn_yes);
        final MyanButton btn_no = myDialog.findViewById(R.id.btn_no);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(false);
        myDialog.show();

        btn_yes.setOnClickListener(v -> callWebService(model));

        btn_no.setOnClickListener(v -> {
            myDialog.dismiss();
            progressDialog.show();
            handler.postDelayed(() -> progressDialog.hide(), 400);
            finish();
        });

    }

    private void callWebService(final ReferralModel model) {
        progressDialog.show();
        callReferral = service.postReferral(model);
        callReferral.enqueue(new Callback<ReferalResponseModel>() {
            @Override
            public void onResponse(Call<ReferalResponseModel> call,
                                   Response<ReferalResponseModel> response) {

                if (response.isSuccessful()) {
                    progressDialog.hide();
                    myDialog.dismiss();
                    ReferalResponseModel referalResponseModel = response.body();

                    referralResultDialog.showDialog(R.mipmap.ask_delete_512, referalResponseModel.getMessage(),
                            "ပိတ်မည်");

                    referralResultDialog.getBtnOk().setOnClickListener(v -> {

                        referralResultDialog.hideDialog();
                        progressDialog.show();
                        handler.postDelayed(() -> progressDialog.hide(), 500);
                        finish();

                    });

                    Log.d("ReferralResponseModel", referalResponseModel.getCode());
                    Log.d("ReferralResponseModel", referalResponseModel.getMessage());


                } else {
                    progressDialog.hide();
                    myDialog.dismiss();
                    referralResultDialog.showDialog(R.mipmap.ask_delete_512, "လုပ်ဆောင်ချက်မအောင်မြင်ပါ၍ ပြန်လည်ပေးပို့ပါ။",
                            "ပိတ်မည်");

                    referralResultDialog.getBtnOk().setOnClickListener(v -> {
                        referralResultDialog.hideDialog();
                        progressDialog.show();
                        handler.postDelayed(() -> progressDialog.hide(), 500);
                        finish();

                    });
                }

            }

            @Override
            public void onFailure(Call<ReferalResponseModel> call, Throwable t) {
                progressDialog.hide();
                myDialog.dismiss();
                referralResultDialog.showDialog(R.mipmap.ask_delete_512, "လုပ်ဆောင်ချက်မအောင်မြင်ပါ၍ ပြန်လည်ပေးပို့ပါ။",
                        "ပိတ်မည်");

                referralResultDialog.getBtnOk().setOnClickListener(v -> {
                    referralResultDialog.hideDialog();
                    progressDialog.show();
                    handler.postDelayed(() -> progressDialog.hide(), 500);
                    finish();

                });


            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(me.dm7.barcodescanner.zbar.Result result) {

        String[] temp = result.getContents().split(",");

        if (temp.length == 2) {
            referralModel = new ReferralModel();
            referralModel.setApplication("PyoPyoMay");
            referralModel.setReferralPhoneNumber(sharePreferenceHelper.getPhoneNo());
            referralModel.setUserPhoneNumber(temp[0]);
            referralModel.setReferDate(referralDate);
            referralModel.setIMEI(temp[1]);

            confirmDialogBox(referralModel);

            Log.d("ReferralModel", "All Information" +
                    referralModel.getReferralPhoneNumber()
                    + "," + referralModel.getUserPhoneNumber() +
                    "," + referralModel.getReferDate() +
                    "," + referralModel.getIMEI());
        } else {
            referralResultDialog.showDialog(R.mipmap.ask_delete_512,
                    "QR Code မှားယွင်းနေပါသည်။",
                    "ပိတ်မည်");

            referralResultDialog.getBtnOk().setOnClickListener(v -> {
                referralResultDialog.hideDialog();
                progressDialog.show();
                handler.postDelayed(() -> progressDialog.hide(), 500);
                finish();

            });
        }

    }

    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

}
