package com.keoeoetech.pyopyomay;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.model.OrganizationModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class CSODetailActivity extends BaseActivity {

    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.tv_organization_name)
    MyanBoldTextView tv_organization_name;
    @BindView(R.id.tv_service_title)
    MyanTextView tv_service_title;
    @BindView(R.id.tv_service)
    MyanTextView tv_service;
    @BindView(R.id.tv_contact_title)
    MyanTextView tv_contact_title;
    @BindView(R.id.tv_contact)
    MyanTextView tv_contact;
    @BindView(R.id.phone_icon)
    ImageView phone_icon;
    @BindView(R.id.tv_hotline_title)
    MyanTextView tv_hotline_title;
    @BindView(R.id.tv_hotline)
    MyanTextView tv_hotline;
    @BindView(R.id.important_phone_icon)
    ImageView important_phone_icon;
    @BindView(R.id.tv_email_title)
    MyanTextView tv_email_title;
    @BindView(R.id.tv_email)
    MyanTextView tv_email;
    @BindView(R.id.tv_website_title)
    MyanTextView tv_website_title;
    @BindView(R.id.tv_website)
    MyanTextView tv_website;
    @BindView(R.id.tv_address_title)
    MyanTextView tv_address_title;
    @BindView(R.id.tv_address)
    MyanTextView tv_address;
    @BindView(R.id.tv_township)
    MyanTextView tv_township;
    @BindView(R.id.tv_state)
    MyanTextView tv_state;

    OrganizationModel organizationModel;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_csodetail;

    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {
        init();
        setData();
    }

    private void init() {

        organizationModel = (OrganizationModel) getIntent().getSerializableExtra("obj");
        setupToolbar(true);
    }

    private void setData() {

        int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
        Picasso.with(CSODetailActivity.this)
                .load(organizationModel.getPhotoUrl())
                .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                .resize(size, size)
                .centerInside()
                .placeholder(R.mipmap.ppm_placeholder)
                .error(R.mipmap.ppm_placeholder)
                .into(profile_pic);

        tv_organization_name.setMyanmarText(organizationModel.getCSOName());

        tv_service_title.setMyanmarText(getString(R.string.str_service));
        tv_service.setMyanmarText(organizationModel.getService());


        tv_contact_title.setMyanmarText(getString(R.string.str_phone_no));
        if (TextUtils.isEmpty(organizationModel.getContactNo())) {
            phone_icon.setVisibility(View.GONE);

        } else {
            phone_icon.setVisibility(View.VISIBLE);
        }

        tv_contact.setMyanmarText(organizationModel.getContactNo());
        phone_icon.setOnClickListener(v -> showPhonePopup());

        tv_hotline_title.setMyanmarText(getString(R.string.str_hotline));

        if (organizationModel.getHotline() == null) {
            important_phone_icon.setVisibility(View.GONE);

        } else {
            important_phone_icon.setVisibility(View.VISIBLE);
        }

        tv_hotline.setMyanmarText(organizationModel.getHotline());
        important_phone_icon.setOnClickListener(v -> showHotlinePhonePopup());

        tv_email_title.setMyanmarText(getString(R.string.str_email));
        tv_email.setMyanmarText(organizationModel.getGmail());

        tv_website_title.setMyanmarText(getString(R.string.str_website));
        tv_website.setMyanmarText(organizationModel.getWebsite());

        tv_address_title.setMyanmarText(getString(R.string.str_address));


        if (organizationModel.getAddress() == null) {
            tv_address.setVisibility(View.GONE);
        } else {
            tv_address.setVisibility(View.VISIBLE);
            tv_address.setMyanmarText((organizationModel.getAddress()) + "၊ ");
        }

        if (organizationModel.getTownship() == null) {
            tv_township.setVisibility(View.GONE);
        } else {
            tv_township.setVisibility(View.VISIBLE);
            tv_township.setMyanmarText(organizationModel.getTownship() + "၊ ");
        }

        if (organizationModel.getStateDivision() == null) {
            tv_state.setVisibility(View.GONE);
        } else {
            tv_state.setVisibility(View.VISIBLE);
            tv_state.setMyanmarText(organizationModel.getStateDivision() + "။");
        }

    }

    private void showHotlinePhonePopup() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.mipmap.ppm_phonebook_200);
        builderSingle.setTitle(MyanTextProcessor.processText(CSODetailActivity.this, "ခေါ်ဆိုမည့်နံပါတ်ကိုနှိပ်ပါ"));

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item);

        String[] phones = organizationModel.getHotline().split(",");

        for (int i = 0; i < phones.length; i++) {
            arrayAdapter.add(phones[i]);
        }


        builderSingle.setNegativeButton("cancel", (dialog, which) -> dialog.dismiss());

        builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {

            dialog.dismiss();

            String phoneNo = arrayAdapter.getItem(which);

            if (ContextCompat.checkSelfPermission(CSODetailActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNo.trim()));
                startActivity(intent);
            } else {
                ActivityCompat.requestPermissions(CSODetailActivity.this, new String[]{
                        Manifest.permission.CALL_PHONE}, 1);
            }


        });
        builderSingle.show();
    }

    private void showPhonePopup() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.mipmap.ppm_phonebook_200);
        builderSingle.setTitle(MyanTextProcessor.processText(CSODetailActivity.this, "ခေါ်ဆိုမည့်နံပါတ်ကိုနှိပ်ပါ"));

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item);

        String[] phones = organizationModel.getContactNo().split(",");

        for (int i = 0; i < phones.length; i++) {
            arrayAdapter.add(phones[i]);
        }


        builderSingle.setNegativeButton("cancel", (dialog, which) -> dialog.dismiss());

        builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {

            dialog.dismiss();

            String phoneNo = arrayAdapter.getItem(which);

            if (ContextCompat.checkSelfPermission(CSODetailActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNo.trim()));
                startActivity(intent);
            } else {
                ActivityCompat.requestPermissions(CSODetailActivity.this, new String[]{
                        Manifest.permission.CALL_PHONE}, 1);
            }


        });
        builderSingle.show();

    }
}
