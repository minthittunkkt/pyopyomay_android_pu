package com.keoeoetech.pyopyomay;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanEditText;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DialogHelperWithOkCancelButton;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PeriodAlarmHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.NotificationModel;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;
import com.keoeoetech.pyopyomay.receiver.AlarmReceiver;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import butterknife.BindView;
import io.realm.Realm;

public class PeriodActivity extends BaseActivity {

    @BindView(R.id.label_last_period_day)
    MyanTextView label_last_period_day;
    @BindView(R.id.edt_last_period_day)
    MyanEditText edt_last_period_day;
    @BindView(R.id.label_period_duration)
    MyanTextView label_period_duration;
    @BindView(R.id.edt_period_duration)
    MyanEditText edt_period_duration;
    @BindView(R.id.label_period_cycle)
    MyanTextView label_period_cycle;
    @BindView(R.id.edt_period_cycle)
    MyanEditText edt_period_cycle;
    @BindView(R.id.btn_show_data)
    MyanButton btn_show_data;
    @BindView(R.id.tv_periodCycle_tip)
    MyanTextView tv_periodCycle_tip;
    @BindView(R.id.tv_periodDuration_tip)
    MyanTextView tv_periodDuration_tip;

    Calendar cal;
    Realm realm;
    Date periodDate;
    String periodStartDate;

    private PeriodTrackerModel oldModel = null;
    private MyDateFormat myDateFormat;

    private PeriodAlarmHelper periodAlarmHelper;

    DatePickerDialog datePickerDialog;
    int mYear, mMonth, mDay;
    DialogHelperWithOkCancelButton dialogHelperWithOkCancelButton;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_period;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        setMyanmarText();

        if (isProfileSetup()) {
            bindData();
        }
        showData();



    }

    private void setMyanmarText() {
        label_last_period_day.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_LAST_DAY_OF_PERIOD));
        label_period_duration.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_DURATION));
        label_period_cycle.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_CYCLE));
        btn_show_data.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_BTN_SHOW_WOMAN_DATA));
        tv_periodCycle_tip.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PERIOD_CYCLE_TIP));
        tv_periodDuration_tip.setMyanmarText(TextDictionaryHelper.getText(this,
                TextDictionaryHelper.TEXT_PERIOD_DURATION_TIP));
    }

    private void init() {
        setupToolbar(true);
        setupToolbarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_WOMAN_HEALTH));

        periodAlarmHelper = new PeriodAlarmHelper(this);
        dialogHelperWithOkCancelButton = new DialogHelperWithOkCancelButton(this);


        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.PERIOD_ACTIVITY);

        realm = Realm.getDefaultInstance();
        oldModel = realm.where(PeriodTrackerModel.class).findFirst();

        myDateFormat = new MyDateFormat();
        cal = Calendar.getInstance();
        edt_last_period_day.setOnClickListener(v -> {

            if (oldModel == null) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
            }

            else {
                final Calendar lsdCalendar = Calendar.getInstance();
                try {
                    lsdCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(oldModel.getLMD()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                mYear = lsdCalendar.get(Calendar.YEAR);
                mMonth = lsdCalendar.get(Calendar.MONTH);
                mDay = lsdCalendar.get(Calendar.DAY_OF_MONTH);
            }



            datePickerDialog = new DatePickerDialog(PeriodActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT,
                    (view, year, monthOfYear, dayOfMonth) -> edt_last_period_day.setMyanmarText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + (year)), mYear, mMonth, mDay);



            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            datePickerDialog.show();
        });
    }

    private void bindData() {

        if(oldModel!=null) {
            edt_period_duration.setMyanmarText(String.valueOf(oldModel.getPLength()));
            edt_period_cycle.setMyanmarText(String.valueOf(oldModel.getPCycle()));
            edt_last_period_day.setMyanmarText(String.valueOf(oldModel.getLMD()));
        }

    }

    private PeriodTrackerModel getDataFromControl() {
        PeriodTrackerModel model = new PeriodTrackerModel();

        model.setPLength(Integer.parseInt(edt_period_duration.getText().toString()));
        model.setPCycle(Integer.parseInt(edt_period_cycle.getText().toString()));
        model.setLMD(edt_last_period_day.getMyanmarText());

        return model;
    }


    private boolean isProfileSetup() {
        if (realm.where(PeriodTrackerModel.class).count() > 0) {
            return true;
        } else {
            return false;
        }
    }


    private void showData() {
        btn_show_data.setOnClickListener(view -> {

            if (validation()) {

                if (oldModel == null) {
                    //add new
                    realm.beginTransaction();
                    PeriodTrackerModel periodTrackerModel = getDataFromControl();
                    periodTrackerModel.setId(UUID.randomUUID().toString());
                    realm.copyToRealmOrUpdate(periodTrackerModel); //close the database
                    realm.commitTransaction();
                    bindData();
                    //set alarm
                    setPeriodAlarm();

                } else {
                    //update
                    realm.beginTransaction();
                    PeriodTrackerModel profileSetUpModel = getDataFromControl();
                    profileSetUpModel.setId(oldModel.getId());
                    realm.copyToRealmOrUpdate(profileSetUpModel); //close the database
                    realm.commitTransaction();
                    bindData();
                    //set alarm
                    setPeriodAlarm();
                }
                finish();
            }
        });

    }

    private boolean validation() {

        if (edt_period_duration.getMyanmarText().equals("")) {
            Toast toast = Toast.makeText(this, MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_PERIOD_DATA_FILL)), Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 230);
            toast.show();

            return false;
        }
        if (edt_last_period_day.getMyanmarText().equals("")) {

            Toast toast = Toast.makeText(this, MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_PERIOD_DATA_FILL)), Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 230);
            toast.show();

            return false;
        }
        if (edt_period_cycle.getMyanmarText().equals("")) {
            Toast toast = Toast.makeText(this, MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_PERIOD_DATA_FILL)), Toast.LENGTH_SHORT);


            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 230);
            toast.show();

            return false;
        }


        return true;
    }

    public void setPeriodAlarm() {

        Log.d("THN","It is first period alarm in PA.");

        oldModel = realm.where(PeriodTrackerModel.class).findFirst();

        if (oldModel != null) {
            Calendar lsdCalendar = Calendar.getInstance();
            try {
                lsdCalendar.setTime(myDateFormat.DATE_FORMAT_DMY.parse(oldModel.getLMD()));
                lsdCalendar.add(Calendar.DATE, oldModel.getPCycle());
                periodStartDate = myDateFormat.DATE_FORMAT_DMY.format(lsdCalendar.getTime());
                periodDate = myDateFormat.DATE_FORMAT_DMY.parse(periodStartDate);


                Log.d("PeriodDate", myDateFormat.DATE_FORMAT_YMD.format(periodDate));
                periodAlarmHelper.startAlarm(periodDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {

        dialogHelperWithOkCancelButton.showDialog(R.mipmap.profile_data_fill,
                "သေချာပါသလား", "ထွက်မည်",
                "မထွက်ပါ");

        dialogHelperWithOkCancelButton.getBtnOk().
                setOnClickListener(v -> {
                    Intent intent =new Intent(PeriodActivity.this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                });

        dialogHelperWithOkCancelButton.getBtnCancel()
                .setOnClickListener(v -> dialogHelperWithOkCancelButton.hideDialog());

    }

}


