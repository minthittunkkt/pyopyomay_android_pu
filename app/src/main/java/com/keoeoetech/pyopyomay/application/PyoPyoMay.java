package com.keoeoetech.pyopyomay.application;

import android.app.Application;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.keoeoetech.pyopyomay.BuildConfig;
import com.keoeoetech.pyopyomay.google_analytic.AnalyticsTrackers;
import com.keoeoetech.pyopyomay.helper.FontSharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.MyRealmMigration;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;

import net.gotev.uploadservice.UploadService;

import java.io.File;


import io.realm.Realm;
import io.realm.RealmConfiguration;


/*Created by HELLO on 6/16/2016.*/

public class PyoPyoMay extends Application {

    private FontSharePreferenceHelper fontSharePreferenceHelper;
    private SharePreferenceHelper sharePreferenceHelper;
    public static final String TAG = "PyoPyoMay";
    protected String userAgent;


    private static PyoPyoMay mInstance;


    private boolean fontChecked = false;

    @Override
    public void onCreate() {
        super.onCreate();

        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        Realm.init(this);
        //1.0.4 => 2
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(2)
                .migration(new MyRealmMigration())
                .build();
        Realm.setDefaultConfiguration(config);
        init();
        googleAnalyticSetUp();

    }


    private void init() {
        sharePreferenceHelper=new SharePreferenceHelper(this);
        fontSharePreferenceHelper = new FontSharePreferenceHelper(this);
        userAgent = Util.getUserAgent(this, "PyoPyoMay");
        //Credit: Myat Min Soe MDetect.
        fontInit(getApplicationContext());
    }


    private void fontInit(Context context) {
        if (fontChecked) {
            return; }

        TextView textView = new TextView(context, null);
        textView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        textView.setText("\u1000");
        textView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int length1 = textView.getMeasuredWidth();

        textView.setText("\u1000\u1039\u1000");
        textView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int length2 = textView.getMeasuredWidth();

        if (length1 == length2) {
            //mm3
            changeFont(PyoPyoMayConstant.MM3_FONT);
            fontChecked = true;
        } else {
            //zg
            changeFont(PyoPyoMayConstant.ZG_FONT);
            fontChecked = true;
        }

    }


    //for MyanTextProcessor
    public String getSelectedFont() {
        return fontSharePreferenceHelper.getFont();
    }

    public String getSelectedLanguage() {
        return fontSharePreferenceHelper.getLanguage();
    }


    //zg or mm3
    public void changeFont(String font) {
        fontSharePreferenceHelper.setFont(font);
    }

    public void changeLanguage(String language) {
        fontSharePreferenceHelper.setLanguage(language);
    }



    public DataSource.Factory buildDataSourceFactory(final DefaultBandwidthMeter bandwidthMeter, boolean cache) {
        if (!cache) {
            return new DefaultDataSourceFactory(this, bandwidthMeter,
                    buildHttpDataSourceFactory(bandwidthMeter));
        } else {
            return () -> {
                LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(100 * 1024 * 1024);
                SimpleCache simpleCache = new SimpleCache(new File(getCacheDir(), "media_cache"), evictor);
                return new CacheDataSource(simpleCache, buildCachedHttpDataSourceFactory(bandwidthMeter).createDataSource(),
                        new FileDataSource(), new CacheDataSink(simpleCache, 10 * 1024 * 1024),
                        CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
            };
        }
    }

    private HttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter);
    }

    private DefaultDataSource.Factory buildCachedHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultDataSourceFactory(this, bandwidthMeter, buildHttpDataSourceFactory(bandwidthMeter));
    }


    //GoogleAnalytic
    private void googleAnalyticSetUp() {
        mInstance = this;

        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    }

    public static synchronized PyoPyoMay getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);
        t.set("&uid", String.valueOf(sharePreferenceHelper.getPhoneNo()));

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

}
