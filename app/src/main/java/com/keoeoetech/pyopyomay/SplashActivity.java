package com.keoeoetech.pyopyomay;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.JsonDbSetup;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;

import java.io.IOException;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.linear_logo)
    LinearLayout linear_logo;

    @BindView(R.id.cv_item)
    CardView cv_item;

    private JsonDbSetup jsonDbSetup;

    @BindView(R.id.tv_age)
    MyanTextView tv_age;

    private SharePreferenceHelper sharePreferenceHelper;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(false);
        getSupportActionBar().hide();
        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.SPLASH_ACTIVITY);
        tv_age.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_AGE));

        sharePreferenceHelper = new SharePreferenceHelper(this);
        jsonDbSetup = new JsonDbSetup(this);


        if (jsonDbSetup.isDbsetupDone()) {

            cv_item.setVisibility(View.GONE);
            linear_logo.setVisibility(View.VISIBLE);

            new Handler().postDelayed(() -> {
                // TODO Auto-generated method stub

                initProcess();

            }, 2300);
        } else {


            cv_item.setVisibility(View.VISIBLE);
            linear_logo.setVisibility(View.GONE);
            try {
                jsonDbSetup.jsonDatasetup();
                initProcess();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void initProcess() {
        if (sharePreferenceHelper.isPhoneNoSetUp()) {
            Intent intent = new Intent(this, MainActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            startActivity(intent);
        }

    }

    private String getVersionName() throws PackageManager.NameNotFoundException {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                this.getPackageName(), 0);

        return info.versionName;
    }

}
