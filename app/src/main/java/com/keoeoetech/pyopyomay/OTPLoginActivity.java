package com.keoeoetech.pyopyomay;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanEditText;
import butterknife.BindView;

public class OTPLoginActivity extends BaseActivity{

    @BindView(R.id.edt_otp_phone)
    MyanEditText edt_otp_phone;
    @BindView(R.id.btn_otp_login)
    MyanButton btn_otp_login;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_otplogin;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        btn_otp_login.setOnClickListener(v -> nextPage());
    }

    private void nextPage() {
        if(!(edt_otp_phone.getMyanmarText().equals("")))
        {
            Intent intent=new Intent(OTPLoginActivity.this,OTPCodeActivity.class);
            intent.putExtra("phone",edt_otp_phone.getMyanmarText());
            startActivity(intent);
            finish();

        }
        else
        {
            Toast.makeText(OTPLoginActivity.this,"Please enter Phone Number", Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        setupToolbar(true);
        setupToolbarText(" ");
        getSupportActionBar().hide();
    }


}