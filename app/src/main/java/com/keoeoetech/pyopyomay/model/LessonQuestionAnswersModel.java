package com.keoeoetech.pyopyomay.model;

import java.util.ArrayList;

public class LessonQuestionAnswersModel {

    private ArrayList<LessonModel> Lesson;
    private ArrayList<QuestionModel> Question;
    private ArrayList<AnswersModel> Answers;

    public ArrayList<LessonModel> getLesson() {
        return Lesson;
    }

    public void setLesson(ArrayList<LessonModel> lesson) {
        Lesson = lesson;
    }

    public ArrayList<QuestionModel> getQuestion() {
        return Question;
    }

    public void setQuestion(ArrayList<QuestionModel> question) {
        Question = question;
    }

    public ArrayList<AnswersModel> getAnswers() {
        return Answers;
    }

    public void setAnswers(ArrayList<AnswersModel> answers) {
        Answers = answers;
    }
}
