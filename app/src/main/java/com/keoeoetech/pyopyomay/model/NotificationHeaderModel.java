package com.keoeoetech.pyopyomay.model;

import com.keoeoetech.pyopyomay.common.Pageable;

public class NotificationHeaderModel implements Pageable {


    public NotificationHeaderModel(int ResID,String headerText) {

        this.imageId=ResID;
        this.textHeader = headerText;

    }

    private int imageId;
    private String textHeader;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTextHeader() {
        return textHeader;
    }

    public void setTextHeader(String textHeader) {
        this.textHeader = textHeader;
    }
}
