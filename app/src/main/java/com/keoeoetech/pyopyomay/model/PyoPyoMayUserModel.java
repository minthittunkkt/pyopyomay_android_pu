package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PyoPyoMayUserModel extends RealmObject implements Pageable,Serializable {

    @PrimaryKey
    @SerializedName("MemberId")
    @Expose
    private int MemberId;

    @SerializedName("MemberName")
    @Expose
    private String MemberName;

    @SerializedName("PhoneNumber")
    @Expose
    private String PhoneNumber;

    @SerializedName("DOB")
    @Expose
    private String DOB;

    @SerializedName("Location")
    @Expose
    private String Location;

    @SerializedName("Accesstime")
    @Expose
    private String Accesstime;

    @SerializedName("AndriodToken")
    @Expose
    private String AndriodToken;

    @SerializedName("UserType")
    @Expose
    private String UserType;

    @SerializedName("MemberProfilePhoto")
    @Expose
    private String MemberProfilePhoto;

    @SerializedName("FacebookId")
    @Expose
    private String FacebookId;

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int memberId) {
        MemberId = memberId;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public String getAndriodToken() {
        return AndriodToken;
    }

    public void setAndriodToken(String andriodToken) {
        AndriodToken = andriodToken;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getMemberProfilePhoto() {
        return MemberProfilePhoto;
    }

    public void setMemberProfilePhoto(String memberProfilePhoto) {
        MemberProfilePhoto = memberProfilePhoto;
    }

    public String getFacebookId() {
        return FacebookId;
    }

    public void setFacebookId(String facebookId) {
        FacebookId = facebookId;
    }
}
