package com.keoeoetech.pyopyomay.model;


import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

public class CardModel implements Pageable,Serializable {

    private int imageID;
    private String categoryNameForAPI;

    public CardModel(String categoryName) {
        categoryNameForAPI=categoryName;
    }

    public CardModel(int imageID, String categoryNameForAPI) {
        this.imageID = imageID;
        this.categoryNameForAPI = categoryNameForAPI;
    }

    public int getImageID() {
        return imageID;
    }

    public String getCategoryNameForAPI() {
        return categoryNameForAPI;
    }
}
