package com.keoeoetech.pyopyomay.model;



import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class QuestionModel extends RealmObject implements Serializable, Pageable {

    @PrimaryKey
    public int QuestionId;
    public int LessonId;
    public String Type;
    public String QuestionInEnglish;
    public String QuestionInUnicode;
    public String QuestionInZawgyi;
    public String Photo;
    public String Explanation;
    public boolean IsDeleted;
    public String Accesstime;
    public boolean Active;
    public String WhoPosted;
    public int PostedUser;
    public String QuestionInMon;
    public String QuestionInSagawKaren;
    public String QuestionInShan;
    public String QuestionInPoeKaren;
    public String QuestionInKaChin;
    public String QuestionInRaKhine;

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public int getLessonId() {
        return LessonId;
    }

    public void setLessonId(int lessonId) {
        LessonId = lessonId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getQuestionInEnglish() {
        return QuestionInEnglish;
    }

    public void setQuestionInEnglish(String questionInEnglish) {
        QuestionInEnglish = questionInEnglish;
    }

    public String getQuestionInUnicode() {
        return QuestionInUnicode;
    }

    public void setQuestionInUnicode(String questionInUnicode) {
        QuestionInUnicode = questionInUnicode;
    }

    public String getQuestionInZawgyi() {
        return QuestionInZawgyi;
    }

    public void setQuestionInZawgyi(String questionInZawgyi) {
        QuestionInZawgyi = questionInZawgyi;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getExplanation() {
        return Explanation;
    }

    public void setExplanation(String explanation) {
        Explanation = explanation;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getWhoPosted() {
        return WhoPosted;
    }

    public void setWhoPosted(String whoPosted) {
        WhoPosted = whoPosted;
    }

    public int getPostedUser() {
        return PostedUser;
    }

    public void setPostedUser(int postedUser) {
        PostedUser = postedUser;
    }

    public String getQuestionInMon() {
        return QuestionInMon;
    }

    public void setQuestionInMon(String questionInMon) {
        QuestionInMon = questionInMon;
    }

    public String getQuestionInSagawKaren() {
        return QuestionInSagawKaren;
    }

    public void setQuestionInSagawKaren(String questionInSagawKaren) {
        QuestionInSagawKaren = questionInSagawKaren;
    }

    public String getQuestionInShan() {
        return QuestionInShan;
    }

    public void setQuestionInShan(String questionInShan) {
        QuestionInShan = questionInShan;
    }

    public String getQuestionInPoeKaren() {
        return QuestionInPoeKaren;
    }

    public void setQuestionInPoeKaren(String questionInPoeKaren) {
        QuestionInPoeKaren = questionInPoeKaren;
    }

    public String getQuestionInKaChin() {
        return QuestionInKaChin;
    }

    public void setQuestionInKaChin(String questionInKaChin) {
        QuestionInKaChin = questionInKaChin;
    }

    public String getQuestionInRaKhine() {
        return QuestionInRaKhine;
    }

    public void setQuestionInRaKhine(String questionInRaKhine) {
        QuestionInRaKhine = questionInRaKhine;
    }
}
