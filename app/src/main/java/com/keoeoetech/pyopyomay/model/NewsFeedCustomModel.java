package com.keoeoetech.pyopyomay.model;




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedCustomModel implements Pageable {

    @Expose
    @SerializedName("nextLink")
    private String nextLink;
    @Expose
    @SerializedName("prevLink")
    private String prevLink;
    @Expose
    @SerializedName("Results")
    private ArrayList<NewsFeedModel> Results;
    @Expose
    @SerializedName("TotalCount")
    private int totalCount;
    @Expose
    @SerializedName("TotalPages")
    private int totalPages;


    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }


    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public ArrayList<NewsFeedModel> getResults() {
        return Results;
    }

    public void setResults(ArrayList<NewsFeedModel> results) {
        Results = results;
    }

    @Override
    public String toString() {
        return "NewsFeedResultModel{" +
                "nextLink='" + nextLink + '\'' +
                ", prevLink='" + prevLink + '\'' +
                ", Results=" + Results +
                ", totalCount=" + totalCount +
                ", totalPages=" + totalPages +
                '}';
    }
}
