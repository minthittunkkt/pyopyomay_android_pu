package com.keoeoetech.pyopyomay.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ArticleModel extends RealmObject implements Parcelable, Pageable {


    @PrimaryKey
    @SerializedName("UniqueKey")
    @Expose
    public String UniqueKey;

    @SerializedName("CategoryID")
    @Expose
    public int CategoryID;

    @SerializedName("CategoryName")
    @Expose
    public String CategoryName;

    @SerializedName("CategoryNameEnglish")
    @Expose
    public String CategoryNameEnglish;

    @SerializedName("Title")
    @Expose
    public String Title;

    @SerializedName("Title_In_English")
    @Expose
    public String Title_In_English;

    @SerializedName("Content")
    @Expose
    public String Content;

    @SerializedName("Content_In_English")
    @Expose
    public String Content_In_English;

    @SerializedName("Photo")
    @Expose
    public String Photo;

    @SerializedName("CreateDate")
    @Expose
    public String CreateDate;

    @SerializedName("LikeCount")
    @Expose
    public int LikeCount;

    @SerializedName("UsefulCount")
    @Expose
    public int UsefulCount;

    @SerializedName("ThankCount")
    @Expose
    public int ThankCount;

    @SerializedName("CommentCount")
    @Expose
    public int CommentCount;

    @SerializedName("Accesstime")
    @Expose
    public String Accesstime;

    @SerializedName("Active")
    @Expose
    public boolean Active;

    @SerializedName("ArticleType")
    @Expose
    public String ArticleType;

    @SerializedName("Thumbnail")
    @Expose
    public String Thumbnail;

    @SerializedName("VideoUrl")
    @Expose
    public String VideoUrl;

    @SerializedName("TopandDown")
    @Expose
    public boolean TopandDown;

    @SerializedName("TopDays")
    @Expose
    public int TopDays;

    public String getUniqueKey() {
        return UniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        UniqueKey = uniqueKey;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCategoryNameEnglish() {
        return CategoryNameEnglish;
    }

    public void setCategoryNameEnglish(String categoryNameEnglish) {
        CategoryNameEnglish = categoryNameEnglish;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getTitle_In_English() {
        return Title_In_English;
    }

    public void setTitle_In_English(String title_In_English) {
        Title_In_English = title_In_English;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getContent_In_English() {
        return Content_In_English;
    }

    public void setContent_In_English(String content_In_English) {
        Content_In_English = content_In_English;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public int getLikeCount() {
        return LikeCount;
    }

    public void setLikeCount(int likeCount) {
        LikeCount = likeCount;
    }

    public int getUsefulCount() {
        return UsefulCount;
    }

    public void setUsefulCount(int usefulCount) {
        UsefulCount = usefulCount;
    }

    public int getThankCount() {
        return ThankCount;
    }

    public void setThankCount(int thankCount) {
        ThankCount = thankCount;
    }

    public int getCommentCount() {
        return CommentCount;
    }

    public void setCommentCount(int commentCount) {
        CommentCount = commentCount;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getArticleType() {
        return ArticleType;
    }

    public void setArticleType(String articleType) {
        ArticleType = articleType;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }

    public boolean isTopandDown() {
        return TopandDown;
    }

    public void setTopandDown(boolean topandDown) {
        TopandDown = topandDown;
    }

    public int getTopDays() {
        return TopDays;
    }

    public void setTopDays(int topDays) {
        TopDays = topDays;
    }

    public ArticleModel() {
    }

    protected ArticleModel(Parcel in) {
        UniqueKey = in.readString();
        CategoryID = in.readInt();
        CategoryName = in.readString();
        CategoryNameEnglish = in.readString();
        Title = in.readString();
        Title_In_English = in.readString();
        Content = in.readString();
        Content_In_English = in.readString();
        Photo = in.readString();
        CreateDate = in.readString();
        LikeCount = in.readInt();
        UsefulCount = in.readInt();
        ThankCount = in.readInt();
        CommentCount = in.readInt();
        Accesstime = in.readString();
        Active = in.readByte() != 0;
        ArticleType = in.readString();
        Thumbnail = in.readString();
        VideoUrl = in.readString();
        TopandDown = in.readByte() != 0;
        TopDays = in.readInt();
    }

    public static final Creator<ArticleModel> CREATOR = new Creator<ArticleModel>() {
        @Override
        public ArticleModel createFromParcel(Parcel in) {
            return new ArticleModel(in);
        }

        @Override
        public ArticleModel[] newArray(int size) {
            return new ArticleModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(UniqueKey);
        dest.writeInt(CategoryID);
        dest.writeString(CategoryName);
        dest.writeString(CategoryNameEnglish);
        dest.writeString(Title);
        dest.writeString(Title_In_English);
        dest.writeString(Content);
        dest.writeString(Content_In_English);
        dest.writeString(Photo);
        dest.writeString(CreateDate);
        dest.writeInt(LikeCount);
        dest.writeInt(UsefulCount);
        dest.writeInt(ThankCount);
        dest.writeInt(CommentCount);
        dest.writeString(Accesstime);
        dest.writeByte((byte) (Active ? 1 : 0));
        dest.writeString(ArticleType);
        dest.writeString(Thumbnail);
        dest.writeString(VideoUrl);
        dest.writeByte((byte) (TopandDown ? 1 : 0));
        dest.writeInt(TopDays);
    }
}
