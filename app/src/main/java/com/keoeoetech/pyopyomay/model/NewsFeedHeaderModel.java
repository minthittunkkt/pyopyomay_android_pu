package com.keoeoetech.pyopyomay.model;

import com.keoeoetech.pyopyomay.common.Pageable;

public class NewsFeedHeaderModel implements Pageable {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
