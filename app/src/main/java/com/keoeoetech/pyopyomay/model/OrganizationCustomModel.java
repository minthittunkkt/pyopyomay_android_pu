package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OrganizationCustomModel implements Serializable {

    @Expose
    @SerializedName("nextLink")
    private String nextLink;

    @Expose
    @SerializedName("prevLink")
    private String prevLink;

    @Expose
    @SerializedName("TotalCount")
    private int TotalCount;

    @Expose
    @SerializedName("TotalPages")
    private int TotalPages;

    @Expose
    @SerializedName("Results")
    private ArrayList<OrganizationModel> organizationModelArrayList;

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(String prevLink) {
        this.prevLink = prevLink;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public int getTotalPages() {
        return TotalPages;
    }

    public void setTotalPages(int totalPages) {
        TotalPages = totalPages;
    }

    public ArrayList<OrganizationModel> getOrganizationModelArrayList() {
        return organizationModelArrayList;
    }

    public void setOrganizationModelArrayList(ArrayList<OrganizationModel> organizationModelArrayList) {
        this.organizationModelArrayList = organizationModelArrayList;
    }
}
