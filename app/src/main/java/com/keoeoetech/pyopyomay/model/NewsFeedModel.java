package com.keoeoetech.pyopyomay.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class NewsFeedModel implements Pageable,Parcelable {



    @SerializedName("UniqueKey")
    @Expose
    private String uniqueKey;

    @SerializedName("Article")
    @Expose
    private ArticleModel Article;

    @SerializedName("ads")
    @Expose
    private NewsFeedAdsModel ads;

    @SerializedName("ProviderName")
    @Expose
    private String ProviderName;

    @SerializedName("ProviderGuid")
    @Expose
    private String ProviderGuid;

    @SerializedName("ProviderPhoto")
    @Expose
    private String ProviderPhoto;

    @SerializedName("reacttype")
    @Expose
    private String reacttype;

    @SerializedName("isFollow")
    @Expose
    private boolean isFollow;

    @SerializedName("score")
    @Expose
    private int score;

    @SerializedName("Application")
    @Expose
    private String Application;

    @SerializedName("language")
    @Expose
    private String language;

    public NewsFeedModel() {
    }

    protected NewsFeedModel(Parcel in) {
        uniqueKey = in.readString();
        Article = in.readParcelable(ArticleModel.class.getClassLoader());
        ProviderName = in.readString();
        ProviderGuid = in.readString();
        ProviderPhoto = in.readString();
        reacttype = in.readString();
        isFollow = in.readByte() != 0;
        score = in.readInt();
        Application = in.readString();
        language = in.readString();
    }

    public static final Creator<NewsFeedModel> CREATOR = new Creator<NewsFeedModel>() {
        @Override
        public NewsFeedModel createFromParcel(Parcel in) {
            return new NewsFeedModel(in);
        }

        @Override
        public NewsFeedModel[] newArray(int size) {
            return new NewsFeedModel[size];
        }
    };

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public ArticleModel getArticle() {
        return Article;
    }

    public void setArticle(ArticleModel article) {
        Article = article;
    }

    public NewsFeedAdsModel getAds() {
        return ads;
    }

    public void setAds(NewsFeedAdsModel ads) {
        this.ads = ads;
    }

    public String getProviderName() {
        return ProviderName;
    }

    public void setProviderName(String providerName) {
        ProviderName = providerName;
    }

    public String getProviderGuid() {
        return ProviderGuid;
    }

    public void setProviderGuid(String providerGuid) {
        ProviderGuid = providerGuid;
    }

    public String getProviderPhoto() {
        return ProviderPhoto;
    }

    public void setProviderPhoto(String providerPhoto) {
        ProviderPhoto = providerPhoto;
    }

    public String getReacttype() {
        return reacttype;
    }

    public void setReacttype(String reacttype) {
        this.reacttype = reacttype;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getApplication() {
        return Application;
    }

    public void setApplication(String application) {
        Application = application;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uniqueKey);
        dest.writeParcelable(Article, flags);
        dest.writeString(ProviderName);
        dest.writeString(ProviderGuid);
        dest.writeString(ProviderPhoto);
        dest.writeString(reacttype);
        dest.writeByte((byte) (isFollow ? 1 : 0));
        dest.writeInt(score);
        dest.writeString(Application);
        dest.writeString(language);
    }



    /*@Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uniqueKey);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(reference);
        dest.writeString(uploadType);
        dest.writeString(photoUrl);
        dest.writeString(videoUrl);
        dest.writeString(organizationName);
        dest.writeInt(organizationId);
        dest.writeString(accesstime);
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(remark);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeInt(likeCount);
        dest.writeString(thumbnail);
        dest.writeString(Subscription);
    }
*/


}

