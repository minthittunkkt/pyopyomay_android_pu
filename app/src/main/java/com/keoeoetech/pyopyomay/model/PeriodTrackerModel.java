package com.keoeoetech.pyopyomay.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PeriodTrackerModel extends RealmObject implements Parcelable {

    @PrimaryKey
    private String Id;
    private int PCycle;
    private int PLength;
    private String LMD;

    public PeriodTrackerModel(Parcel in) {
        Id = in.readString();
        PCycle = in.readInt();
        PLength = in.readInt();
        LMD = in.readString();
    }

    public static final Creator<PeriodTrackerModel> CREATOR = new Creator<PeriodTrackerModel>() {
        @Override
        public PeriodTrackerModel createFromParcel(Parcel in) {
            return new PeriodTrackerModel(in);
        }

        @Override
        public PeriodTrackerModel[] newArray(int size) {
            return new PeriodTrackerModel[size];
        }
    };

    public PeriodTrackerModel() {

    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getPCycle() {
        return PCycle;
    }

    public void setPCycle(int PCycle) {
        this.PCycle = PCycle;
    }

    public int getPLength() {
        return PLength;
    }

    public void setPLength(int PLength) {
        this.PLength = PLength;
    }

    public String getLMD() {
        return LMD;
    }

    public void setLMD(String LMD) {
        this.LMD = LMD;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeInt(PCycle);
        dest.writeInt(PLength);
        dest.writeString(LMD);
    }
}
