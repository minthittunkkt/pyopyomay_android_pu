package com.keoeoetech.pyopyomay.model;


import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

public class AnswersViewModel implements Serializable, Pageable {

    public int AnswerId;
    public int QuestionId;
    public int LessonId;
    public String AnswerInEnglish;
    public String AnswerInUnicode;
    public String AnswerInZawgyi;
    public int Point;
    public boolean IsRight;
    public String TipsInEnglish;
    public String TipsInUnicode;
    public String TipsInZawgyi;
    public String Photo;
    public boolean IsDeleted;
    public String Accesstime;
    public boolean Active;
    public int PostedUser;
    public String AnswerInMon;
    public String AnswerInSagawKaren;
    public String TipsInMon;
    public String TipsInSagawKaren;
    public String AnswerInShan;
    public String AnswerInPoeKaren;
    public String TipsInShan;
    public String TipsInPoeKaren;
    public String AnswerInKaChin;
    public String TipsInKaChin;
    public String AnswerInRaKhine;
    public String TipsInRaKhine;
    public boolean isSelected;

    public int getAnswerId() {
        return AnswerId;
    }

    public void setAnswerId(int answerId) {
        AnswerId = answerId;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public int getLessonId() {
        return LessonId;
    }

    public void setLessonId(int lessonId) {
        LessonId = lessonId;
    }

    public String getAnswerInEnglish() {
        return AnswerInEnglish;
    }

    public void setAnswerInEnglish(String answerInEnglish) {
        AnswerInEnglish = answerInEnglish;
    }

    public String getAnswerInUnicode() {
        return AnswerInUnicode;
    }

    public void setAnswerInUnicode(String answerInUnicode) {
        AnswerInUnicode = answerInUnicode;
    }

    public String getAnswerInZawgyi() {
        return AnswerInZawgyi;
    }

    public void setAnswerInZawgyi(String answerInZawgyi) {
        AnswerInZawgyi = answerInZawgyi;
    }

    public int getPoint() {
        return Point;
    }

    public void setPoint(int point) {
        Point = point;
    }

    public boolean isRight() {
        return IsRight;
    }

    public void setRight(boolean right) {
        IsRight = right;
    }

    public String getTipsInEnglish() {
        return TipsInEnglish;
    }

    public void setTipsInEnglish(String tipsInEnglish) {
        TipsInEnglish = tipsInEnglish;
    }

    public String getTipsInUnicode() {
        return TipsInUnicode;
    }

    public void setTipsInUnicode(String tipsInUnicode) {
        TipsInUnicode = tipsInUnicode;
    }

    public String getTipsInZawgyi() {
        return TipsInZawgyi;
    }

    public void setTipsInZawgyi(String tipsInZawgyi) {
        TipsInZawgyi = tipsInZawgyi;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public int getPostedUser() {
        return PostedUser;
    }

    public void setPostedUser(int postedUser) {
        PostedUser = postedUser;
    }

    public String getAnswerInMon() {
        return AnswerInMon;
    }

    public void setAnswerInMon(String answerInMon) {
        AnswerInMon = answerInMon;
    }

    public String getAnswerInSagawKaren() {
        return AnswerInSagawKaren;
    }

    public void setAnswerInSagawKaren(String answerInSagawKaren) {
        AnswerInSagawKaren = answerInSagawKaren;
    }

    public String getTipsInMon() {
        return TipsInMon;
    }

    public void setTipsInMon(String tipsInMon) {
        TipsInMon = tipsInMon;
    }

    public String getTipsInSagawKaren() {
        return TipsInSagawKaren;
    }

    public void setTipsInSagawKaren(String tipsInSagawKaren) {
        TipsInSagawKaren = tipsInSagawKaren;
    }

    public String getAnswerInShan() {
        return AnswerInShan;
    }

    public void setAnswerInShan(String answerInShan) {
        AnswerInShan = answerInShan;
    }

    public String getAnswerInPoeKaren() {
        return AnswerInPoeKaren;
    }

    public void setAnswerInPoeKaren(String answerInPoeKaren) {
        AnswerInPoeKaren = answerInPoeKaren;
    }

    public String getTipsInShan() {
        return TipsInShan;
    }

    public void setTipsInShan(String tipsInShan) {
        TipsInShan = tipsInShan;
    }

    public String getTipsInPoeKaren() {
        return TipsInPoeKaren;
    }

    public void setTipsInPoeKaren(String tipsInPoeKaren) {
        TipsInPoeKaren = tipsInPoeKaren;
    }

    public String getAnswerInKaChin() {
        return AnswerInKaChin;
    }

    public void setAnswerInKaChin(String answerInKaChin) {
        AnswerInKaChin = answerInKaChin;
    }

    public String getTipsInKaChin() {
        return TipsInKaChin;
    }

    public void setTipsInKaChin(String tipsInKaChin) {
        TipsInKaChin = tipsInKaChin;
    }

    public String getAnswerInRaKhine() {
        return AnswerInRaKhine;
    }

    public void setAnswerInRaKhine(String answerInRaKhine) {
        AnswerInRaKhine = answerInRaKhine;
    }

    public String getTipsInRaKhine() {
        return TipsInRaKhine;
    }

    public void setTipsInRaKhine(String tipsInRaKhine) {
        TipsInRaKhine = tipsInRaKhine;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "AnswersViewModel{" +
                "AnswerID=" + AnswerId +
                ", QuestionID=" + QuestionId +
                ", LessonID=" + LessonId +
                ", AnswerBody='" + AnswerInUnicode + '\'' +
                ", Tip='" + TipsInUnicode + '\'' +
                '}';
    }
}
