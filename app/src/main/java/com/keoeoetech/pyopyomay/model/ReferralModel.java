package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

public class ReferralModel implements Serializable, Pageable {

    @SerializedName("ReferId")
    private int ReferId;
    @SerializedName("ReferralPhoneNumber")
    private String ReferralPhoneNumber;
    @SerializedName("UserPhoneNumber")
    private String UserPhoneNumber;
    @SerializedName("ReferDate")
    private String ReferDate;
    @SerializedName("IMEI")
    private String IMEI;
    @SerializedName("Application")
    private String Application;

    public String getApplication() {
        return Application;
    }

    public void setApplication(String application) {
        Application = application;
    }

    public int getReferId() {
        return ReferId;
    }

    public void setReferId(int referId) {
        ReferId = referId;
    }

    public String getReferralPhoneNumber() {
        return ReferralPhoneNumber;
    }

    public void setReferralPhoneNumber(String referralPhoneNumber) {
        ReferralPhoneNumber = referralPhoneNumber;
    }

    public String getUserPhoneNumber() {
        return UserPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        UserPhoneNumber = userPhoneNumber;
    }

    public String getReferDate() {
        return ReferDate;
    }

    public void setReferDate(String referDate) {
        ReferDate = referDate;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    @Override
    public String toString() {
        return "ReferralModel{" +
                "ReferId=" + ReferId +
                ", ReferralPhoneNumber='" + ReferralPhoneNumber + '\'' +
                ", UserPhoneNumber='" + UserPhoneNumber + '\'' +
                ", ReferDate='" + ReferDate + '\'' +
                ", IMEI='" + IMEI + '\'' +
                ", Application='" + Application + '\'' +
                '}';
    }
}
