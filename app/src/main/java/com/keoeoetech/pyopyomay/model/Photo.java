package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

public class Photo extends RealmObject implements Serializable
{
    @Expose
    @SerializedName("ID")
    private int ID;

    @Expose
    @SerializedName("UniqueKey")
    private String uniqueKey;

    @Expose
    @SerializedName("OrganizationID")
    private int organizationID;

    @Expose
    @SerializedName("NewsFeedID")
    private String newsfeedId;

    @Expose
    @SerializedName("PhotoUrl")
    private String photoUrl;

    @Expose
    @SerializedName("Accesstime")
    private String accesstime;

    @Expose
    @SerializedName("IsDeleted")
    private Boolean isDeleted;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public String getNewsfeedId() {
        return newsfeedId;
    }

    public void setNewsfeedId(String newsfeedId) {
        this.newsfeedId = newsfeedId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getAccesstime() {
        return accesstime;
    }

    public void setAccesstime(String accesstime) {
        this.accesstime = accesstime;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
