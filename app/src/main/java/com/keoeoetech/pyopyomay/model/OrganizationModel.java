package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

public class OrganizationModel implements Pageable, Serializable {

    @Expose
    @SerializedName("Id")
    private int Id;

    @Expose
    @SerializedName("OrganizationID")
    private int OrganizationID;

    @Expose
    @SerializedName("CSOName")
    private String CSOName;

    @Expose
    @SerializedName("Service")
    private String Service;

    @Expose
    @SerializedName("ContactNo")
    private String ContactNo;

    @Expose
    @SerializedName("Category")
    private String Category;

    @Expose
    @SerializedName("SubCategory")
    private String SubCategory;

    @Expose
    @SerializedName("StateDivision")
    private String StateDivision;

    @Expose
    @SerializedName("Township")
    private String Township;

    @Expose
    @SerializedName("Address")
    private String Address;

    @Expose
    @SerializedName("Gmail")
    private String Gmail;

    @Expose

    @SerializedName("Website")
    private String Website;

    @Expose
    @SerializedName("Hotline")
    private String Hotline;

    @Expose
    @SerializedName("PhotoUrl")
    private String PhotoUrl;

    @Expose
    @SerializedName("Remark")
    private String Remark;

    @Expose
    @SerializedName("IsDeleted")
    private boolean IsDeleted;

    @Expose
    @SerializedName("IsActive")
    private boolean IsActive;

    @Expose
    @SerializedName("Accesstime")
    private String Accesstime;

    @Expose
    @SerializedName("Specialization")
    private String Specialization;

    public int getID() {
        return Id;
    }

    public void setID(int Id) {
        this.Id = Id;
    }

    public int getOrganizationID() {
        return OrganizationID;
    }

    public void setOrganizationID(int organizationID) {
        OrganizationID = organizationID;
    }

    public String getCSOName() {
        return CSOName;
    }

    public void setCSOName(String CSOName) {
        this.CSOName = CSOName;
    }

    public String getService() {
        return Service;
    }

    public void setService(String service) {
        Service = service;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getStateDivision() {
        return StateDivision;
    }

    public void setStateDivision(String stateDivision) {
        StateDivision = stateDivision;
    }

    public String getTownship() {
        return Township;
    }

    public void setTownship(String township) {
        Township = township;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getGmail() {
        return Gmail;
    }

    public void setGmail(String gmail) {
        Gmail = gmail;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getHotline() {
        return Hotline;
    }

    public void setHotline(String hotline) {
        Hotline = hotline;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getSubCategory() {
        return SubCategory;
    }

    public void setSubCategory(String subCategory) {
        SubCategory = subCategory;
    }

    public String getSpecialization() {
        return Specialization;
    }

    public void setSpecialization(String specialization) {
        Specialization = specialization;
    }
}
