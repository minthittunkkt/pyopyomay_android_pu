package com.keoeoetech.pyopyomay.model;


import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LessonModel extends RealmObject implements Serializable, Pageable {

    @PrimaryKey
    public int LessonId;
    public int Breakpoint;
    public int Week;
    public int BabyMonth;
    public String Type;
    public boolean IsDeleted;
    public String Accesstime;
    public String TitleInEnglish;
    public String TitleInMyanmarUnicode;
    public String TitleInMyanmarZawgyi;
    public int PostedUser;
    public boolean Active;
    public String Language;
    public String TitleInMon;
    public String TitleInSagawKaren;
    public String TitleInShan;
    public String TitleInPoeKaren;
    public String TitleInKaChin;
    public String TitleInRaKhine;

    public int getLessonId() {
        return LessonId;
    }

    public void setLessonId(int lessonId) {
        LessonId = lessonId;
    }

    public int getBreakpoint() {
        return Breakpoint;
    }

    public void setBreakpoint(int breakpoint) {
        Breakpoint = breakpoint;
    }

    public int getWeek() {
        return Week;
    }

    public void setWeek(int week) {
        Week = week;
    }

    public int getBabyMonth() {
        return BabyMonth;
    }

    public void setBabyMonth(int babyMonth) {
        BabyMonth = babyMonth;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }

    public String getTitleInEnglish() {
        return TitleInEnglish;
    }

    public void setTitleInEnglish(String titleInEnglish) {
        TitleInEnglish = titleInEnglish;
    }

    public String getTitleInMyanmarUnicode() {
        return TitleInMyanmarUnicode;
    }

    public void setTitleInMyanmarUnicode(String titleInMyanmarUnicode) {
        TitleInMyanmarUnicode = titleInMyanmarUnicode;
    }

    public String getTitleInMyanmarZawgyi() {
        return TitleInMyanmarZawgyi;
    }

    public void setTitleInMyanmarZawgyi(String titleInMyanmarZawgyi) {
        TitleInMyanmarZawgyi = titleInMyanmarZawgyi;
    }

    public int getPostedUser() {
        return PostedUser;
    }

    public void setPostedUser(int postedUser) {
        PostedUser = postedUser;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getTitleInMon() {
        return TitleInMon;
    }

    public void setTitleInMon(String titleInMon) {
        TitleInMon = titleInMon;
    }

    public String getTitleInSagawKaren() {
        return TitleInSagawKaren;
    }

    public void setTitleInSagawKaren(String titleInSagawKaren) {
        TitleInSagawKaren = titleInSagawKaren;
    }

    public String getTitleInShan() {
        return TitleInShan;
    }

    public void setTitleInShan(String titleInShan) {
        TitleInShan = titleInShan;
    }

    public String getTitleInPoeKaren() {
        return TitleInPoeKaren;
    }

    public void setTitleInPoeKaren(String titleInPoeKaren) {
        TitleInPoeKaren = titleInPoeKaren;
    }

    public String getTitleInKaChin() {
        return TitleInKaChin;
    }

    public void setTitleInKaChin(String titleInKaChin) {
        TitleInKaChin = titleInKaChin;
    }

    public String getTitleInRaKhine() {
        return TitleInRaKhine;
    }

    public void setTitleInRaKhine(String titleInRaKhine) {
        TitleInRaKhine = titleInRaKhine;
    }
}
