package com.keoeoetech.pyopyomay.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.keoeoetech.pyopyomay.common.Pageable;

import java.io.Serializable;

public class NewsFeedAdsModel implements Pageable,Serializable {

    @SerializedName("UniqueKey")
    @Expose
    public String UniqueKey;

    @SerializedName("Picture")
    @Expose
    public String Picture;
    @SerializedName("weborfblink")
    @Expose
    public String weborfblink;

    @SerializedName("LinkText")
    @Expose
    public String LinkText;

    @SerializedName("Description")
    @Expose
    public String Description;

    @SerializedName("Title")
    @Expose
    public String Title;

    @SerializedName("Active")
    @Expose
    public boolean Active;

    @SerializedName("Order")
    @Expose
    public int Order;

    @SerializedName("CreatedDate")
    @Expose
    public String CreatedDate;

    @SerializedName("Accesstime")
    @Expose
    public String Accesstime;

    public String getUniqueKey() {
        return UniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        UniqueKey = uniqueKey;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getWeborfblink() {
        return weborfblink;
    }

    public void setWeborfblink(String weborfblink) {
        this.weborfblink = weborfblink;
    }

    public String getLinkText() {
        return LinkText;
    }

    public void setLinkText(String linkText) {
        LinkText = linkText;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getAccesstime() {
        return Accesstime;
    }

    public void setAccesstime(String accesstime) {
        Accesstime = accesstime;
    }
}
