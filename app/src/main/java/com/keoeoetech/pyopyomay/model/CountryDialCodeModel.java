package com.keoeoetech.pyopyomay.model;

/**
 * Created by hello on 1/17/18.
 */

public class CountryDialCodeModel {

    private String name;
    private String code;
    private String callingCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }
}
