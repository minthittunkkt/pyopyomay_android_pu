package com.keoeoetech.pyopyomay.model;

import com.keoeoetech.pyopyomay.common.Pageable;

public class LessonHeaderModel implements Pageable {


    private int ResID;


    public LessonHeaderModel(int ResID) {

        this.ResID=ResID;

    }

    public int getResID() {
        return ResID;
    }

    public void setResID(int resID) {
        ResID = resID;
    }

}
