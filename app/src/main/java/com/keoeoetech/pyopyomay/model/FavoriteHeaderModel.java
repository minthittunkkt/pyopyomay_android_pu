package com.keoeoetech.pyopyomay.model;


import com.keoeoetech.pyopyomay.common.Pageable;

public class FavoriteHeaderModel implements Pageable {


    private int ResID;
    private String headerText;

    public FavoriteHeaderModel(int ResID,String headerText) {

        this.ResID=ResID;
        this.headerText = headerText;

    }

    public int getResID() {
        return ResID;
    }

    public void setResID(int resID) {
        ResID = resID;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }
}
