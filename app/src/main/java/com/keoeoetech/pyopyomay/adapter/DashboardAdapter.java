package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.keoeoetech.pyopyomay.CSOCategoryActivity;
import com.keoeoetech.pyopyomay.FavoriteActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.model.DashboardModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardAdapter extends BaseAdapter {
    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dashboard, parent, false);
        return new DashboardAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DashboardAdapter.ViewHolder) holder).bindPost((DashboardModel) getItemsList().get(position), position);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linear_click)
        LinearLayout linear_click;
        @BindView(R.id.tv_title)
        MyanTextView tv_title;
        @BindView(R.id.img_icon)
        ImageView img_icon;
        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            context=itemView.getContext();
            ButterKnife.bind(this,itemView);
        }

        public void bindPost(DashboardModel model, int position) {

            tv_title.setMyanmarText(model.getTitle());
            img_icon.setImageResource(model.getIcon());

            linear_click.setOnClickListener(v -> {
                if(model.getId()==1)
                {
                    Intent intent=new Intent(context, FavoriteActivity.class);
                    context.startActivity(intent);
                }

                else if(model.getId()==2)
                {
                    Intent intent=new Intent(context, CSOCategoryActivity.class);
                    context.startActivity(intent);
                }

            });

        }
    }
}
