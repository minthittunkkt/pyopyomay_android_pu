package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keoeoetech.pyopyomay.QuestionAnswerActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.LessonModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LessonAdapter extends BaseAdapter {
    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lesson, parent, false);
        return new LessonAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((LessonAdapter.ViewHolder) holder).bindPost((LessonModel) getItemsList().get(position), position);
    }

    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item)
        CardView cv_item;
        @BindView(R.id.tv_lesson)
        MyanTextView tv_lesson;
        @BindView(R.id.tv_start_lesson)
        MyanTextView tv_start_lesson;
        private Context context;
        @BindView(R.id.btn_answer_quizz)
        MyanButton btn_answer_quizz;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final LessonModel model, final int position) {

            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 10), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 10), DpToPxHelper.dpToPx(context, 20));
                cv_item.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 10), DpToPxHelper.dpToPx(context, 20));
                cv_item.requestLayout();
            }


            tv_lesson.setMyanmarText(model.getTitleInMyanmarUnicode());
            tv_start_lesson.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_START_LESSON));

            btn_answer_quizz.setMyanmarText(TextDictionaryHelper.getText(context,TextDictionaryHelper.TEXT_BTN_ANSWER_QUIZZ));
            btn_answer_quizz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.QUIZZ_ACTIVITY,
                            PyoPyoMayConstant.LESSON_CLICK,
                            String.valueOf(model.getLessonId()));

                    Intent intent = new Intent(context, QuestionAnswerActivity.class);
                    intent.putExtra("id", model.getLessonId());
                    context.startActivity(intent);

                }
            });

        }
    }


}


