package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.interfaces.AnswerSelectionCallback;
import com.keoeoetech.pyopyomay.model.AnswersViewModel;
import com.keoeoetech.pyopyomay.model.QuestionModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionAnswerAdapter extends BaseAdapter {

    private AnswerSelectionCallback answerSelectionCallback;


    public QuestionAnswerAdapter(AnswerSelectionCallback answerSelectionCallback)
    {
        this.answerSelectionCallback = answerSelectionCallback;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer, parent, false);
        return new QuestionAnswerAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((QuestionAnswerAdapter.ViewHolder) holder).bindPost((AnswersViewModel) getItemsList().get(position), position);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        /*View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_answer_header, parent, false);
        return new QuestionAnswerAdapter.ViewHolderHeader(view);*/

        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        /*RecyclerHeader recyclerHeader = (RecyclerHeader) getItemsList().get(position);
        ((QuestionAnswerAdapter.ViewHolderHeader) holder).bindHeader((QuestionModel) recyclerHeader.getHeaderData());
   */ }




    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item)
        CardView cv_item;
        @BindView(R.id.tv_answer)
        MyanTextView tv_answer;
        @BindView(R.id.img_select)
        ImageView img_select;

        private Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final AnswersViewModel model, final int position) {

            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item.requestLayout();
            }


            tv_answer.setMyanmarText(model.getAnswerInUnicode());

            if(model.isSelected())
            {
                img_select.setImageResource(R.mipmap.selected);
            }
            else
            {
                img_select.setImageResource(R.mipmap.un_selected);
            }

            cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(model.isSelected())
                    {
                        model.setSelected(false);
                    }
                    else
                    {
                        model.setSelected(true);
                    }

                    answerSelectionCallback.select(position, model);

                }
            });

        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_question)
        MyanTextView tv_question;
        private Context context;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindHeader(final QuestionModel model) {

            tv_question.setMyanmarText(model.getQuestionInUnicode());


        }
    }

}
