package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Settings;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.NewsFeedByCategoryActivity;
import com.keoeoetech.pyopyomay.NewsFeedDetailActivity;
import com.keoeoetech.pyopyomay.PeriodTrackerActivity;
import com.keoeoetech.pyopyomay.QuizzActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.common.Pageable;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.keoeoetech.pyopyomay.model.CardModel;
import com.keoeoetech.pyopyomay.model.NewsFeedAdsModel;
import com.keoeoetech.pyopyomay.model.NewsFeedHeaderModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;
import com.squareup.picasso.Picasso;

import net.glxn.qrgen.android.QRCode;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class NewsFeedAdapter extends BaseAdapter {

    private static final int ITEM_CONTENTS = 5;
    private static final int ITEM_ADS = 6;
    private OnNewsFeedBtnClickedListener listener;

    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, final int viewType) {

        if (viewType == ITEM_CONTENTS) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_newsfeed, parent, false);
            return new NewsFeedAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ads, parent, false);
            return new NewsFeedAdapter.AdsViewHolder(view);
        }
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        if (viewType == ITEM_CONTENTS) {
            ((NewsFeedAdapter.ViewHolder) holder).bindPost((ArticleModel)
                    getItemsList().get(position), position);

        } else if (viewType == ITEM_ADS) {
            ((NewsFeedAdapter.AdsViewHolder) holder).bindPost((NewsFeedAdsModel)
                    getItemsList().get(position), position);
        }

    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_newsfeed, parent, false);

        return new NewsFeedAdapter.HeaderHolder(view);
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerHeader recyclerHeader = (RecyclerHeader) getItemsList().get(position);
        ((NewsFeedAdapter.HeaderHolder) holder).bindHeader((NewsFeedHeaderModel) recyclerHeader.getHeaderData());

    }


    @Override
    public int getItemViewType(int position) {
        Pageable item = getItemsList().get(position);
        if (item instanceof ArticleModel) {
            return ITEM_CONTENTS;
        } else if (item instanceof NewsFeedAdsModel) {
            return ITEM_ADS;
        } else {
            return super.getItemViewType(position);
        }
    }


    public void setListener(OnNewsFeedBtnClickedListener listener) {
        this.listener = listener;
    }

    private boolean isSaved(String UniqueKey) {

        ArticleModel model = Realm.getDefaultInstance().where(ArticleModel.class)
                .equalTo("UniqueKey", UniqueKey).findFirst();

        return model != null;

    }

    public interface OnNewsFeedBtnClickedListener {
        void onVideoClicked(String videoUrl, String videoTitle);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item_home)
        CardView cv_item_home;
        @BindView(R.id.img_newsfeed)
        ImageView img_newsfeed;
        @BindView(R.id.tv_newsfeed_category)
        MyanTextView tv_newsfeed_category;
        @BindView(R.id.tv_newsfeed_date)
        MyanTextView tv_newsfeed_date;
        @BindView(R.id.tv_newsfeed_title)
        MyanTextView tv_newsfeed_title;
        @BindView(R.id.tv_newsfeed_content)
        MyanTextView tv_newsfeed_content;
        @BindView(R.id.layout_news_feed_video_view)
        RelativeLayout layout_news_feed_video_view;
        @BindView(R.id.video_newsfeed)
        ImageView video_newsfeed;
        @BindView(R.id.img_newsfeed_favorite)
        ImageView img_newsfeed_favorite;
        MyDateFormat myDateFormat;
        Realm realm;
        private Context context;


        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);

        }

        void bindPost(final ArticleModel model, final int position) {


            if (isSaved(model.getUniqueKey())) {
                img_newsfeed_favorite.setImageResource(R.mipmap.like_pink_64);
            } else {
                img_newsfeed_favorite.setImageResource(R.mipmap.like_grey_64);
            }


            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_home.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_home.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_home.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_home.requestLayout();
            }

            myDateFormat = new MyDateFormat();


            tv_newsfeed_category.setMyanmarText(model.getCategoryName());
            tv_newsfeed_title.setMyanmarText(model.getTitle());
            try {
                tv_newsfeed_date.setMyanmarText(myDateFormat.DATE_FORMAT_DMY_TEXT.format(myDateFormat.DATE_FORMAT_YMD_HMS.parse(model.getAccesstime().replace("T", " "))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tv_newsfeed_content.setMyanmarText(model.getContent());


            switch (model.getArticleType()) {
                case PyoPyoMayConstant.PHOTO:
                    video_newsfeed.setVisibility(View.GONE);

                    int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getPhoto())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerInside()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_newsfeed);

                    break;
                case PyoPyoMayConstant.VIDEO:

                    video_newsfeed.setVisibility(View.VISIBLE);
                    size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getThumbnail())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerInside()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_newsfeed);

                    break;
                default:
                    break;
            }

            img_newsfeed_favorite.setOnClickListener(v -> {

                if (isSaved(model.getUniqueKey())) {

                    img_newsfeed_favorite.setImageResource(R.mipmap.like_grey_64);
                    realm = Realm.getDefaultInstance();
                    final ArticleModel results = realm.where(ArticleModel.class)
                            .equalTo("UniqueKey", model.getUniqueKey()).findFirst();

                    realm.executeTransaction(realm -> results.deleteFromRealm());

                } else {

                    img_newsfeed_favorite.setImageResource(R.mipmap.like_pink_64);
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();
                }

            });


            layout_news_feed_video_view.setOnClickListener(v -> {

                switch (model.getArticleType()) {
                    case PyoPyoMayConstant.PHOTO:
                        Intent intent = new Intent(context, NewsFeedDetailActivity.class);
                        intent.putExtra("obj", model);
                        context.startActivity(intent);
                        break;
                    case PyoPyoMayConstant.VIDEO:
                        listener.onVideoClicked(model.getVideoUrl(), model.getTitle());

                        break;
                    default:
                        break;
                }

            });


            cv_item_home.setOnClickListener(view -> {

                PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_CLICK,
                        "NewsFeedDetailClickId", model.getUniqueKey());

                Intent intent = new Intent(context, NewsFeedDetailActivity.class);
                intent.putExtra("obj", model);
                //intent.putExtra("newsFeedObj",model);
                context.startActivity(intent);
            });

        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_qrcode)
        ImageView img_qrcode;
        @BindView(R.id.tv_qrcode)
        MyanBoldTextView tv_qrcode;
        @BindView(R.id.img_qrcode_button)
        ImageView img_qrcode_button;
        @BindView(R.id.cv_qrcode)
        CardView cv_qrcode;
        @BindView(R.id.tv_answer_quizz)
        MyanTextView tv_answer_quizz;
        @BindView(R.id.tv_quizz)
        MyanTextView tv_quizz;
        @BindView(R.id.btn_quizz)
        MyanButton btn_quizz;
        @BindView(R.id.cv_social)
        CardView cv_social;
        @BindView(R.id.cv_health)
        CardView cv_health;
        @BindView(R.id.cv_motivation)
        CardView cv_motivation;
        @BindView(R.id.cv_education)
        CardView cv_education;
        @BindView(R.id.cv_handmade)
        CardView cv_handmade;
        @BindView(R.id.cv_fashion)
        CardView cv_fashion;
        @BindView(R.id.cv_item_woman_health)
        CardView cv_item_woman_health;
        @BindView(R.id.tv_woman_health)
        MyanTextView tv_woman_health;
        @BindView(R.id.tv_today)
        MyanTextView tv_today;
        @BindView(R.id.tv_best_tips)
        MyanTextView tv_best_tips;
        SharePreferenceHelper sharePreferenceHelper;
        private Context context;
        private String imei;
        private PyoPyoMayUserModel pyoPyoMayUserModel;
        private CardModel cardModel;

        public HeaderHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        private void bindHeader(final NewsFeedHeaderModel model) {

            referalShownNewsFeed();
            setMyanText();

            pyoPyoMayUserModel = Realm.getDefaultInstance().where(PyoPyoMayUserModel.class).findFirst();

            btn_quizz.setOnClickListener(v -> {

                PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_FRAGMENT,
                        PyoPyoMayConstant.USER_TYPE, pyoPyoMayUserModel.getUserType());

                Intent i = new Intent(context, QuizzActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(i);

            });

            cv_item_woman_health.setOnClickListener(v -> {

                PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_FRAGMENT,
                        PyoPyoMayConstant.PERIOD_BUTTON_CLICK, "Period Tracker Enter");


                Intent intent = new Intent(context, PeriodTrackerActivity.class);
                context.startActivity(intent);

            });


            cv_social.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.social_wellbeing_128, context.getString(R.string.str_social_wellbeing));
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });

            cv_health.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.health_care_128, "ကျန်းမာရေးအကြောင်းအရာ");
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });

            cv_motivation.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.motivation_128, "စိတ်ခွန်အားဖြည့်စာများ");
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });
            cv_education.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.learning_128, context.getString(R.string.str_education));
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });

            cv_handmade.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.diy_128, context.getString(R.string.str_diy));
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });

            cv_fashion.setOnClickListener(v -> {

                cardModel = new CardModel(R.mipmap.beauty_128, context.getString(R.string.str_beauty));
                Intent intent = new Intent(context, NewsFeedByCategoryActivity.class);
                intent.putExtra("obj", cardModel);
                context.startActivity(intent);
            });

        }

        private void referalShownNewsFeed() {

            sharePreferenceHelper = new SharePreferenceHelper(context);
            imei = Settings.Secure.getString(context
                            .getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Bitmap myBitmap = QRCode.from(sharePreferenceHelper.getPhoneNo()
                    + "," + imei).bitmap();
            img_qrcode.setImageBitmap(myBitmap);
            if (sharePreferenceHelper.isReferralQRCodeShown()) {
                cv_qrcode.setVisibility(View.GONE);
            } else {
                sharePreferenceHelper.setReferralQRCodeShown(true);
                cv_qrcode.setVisibility(View.VISIBLE);
            }

            img_qrcode_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cv_qrcode.setVisibility(View.GONE);

                }
            });

        }

        private void setMyanText() {

            tv_answer_quizz.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_LET_ANSWER));
            tv_quizz.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_ANSWER_QUIZZ));
            btn_quizz.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_BTN_QUIZZ));
            tv_woman_health.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_WOMAN_HEALTH));
            tv_today.setMyanmarText("ယနေ့အတွက်အကောင်းဆုံး");
            tv_best_tips.setMyanmarText("ဆောင်းပါးများ");

        }


    }


    class AdsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item_ads)
        CardView cv_item_ads;
        @BindView(R.id.img_ads)
        ImageView img_ads;
        @BindView(R.id.tv_ads_content)
        MyanTextView tv_ads_content;
        @BindView(R.id.btn_enter_fb_page)
        MyanButton btn_enter_fb_page;

        private Context context;

        public AdsViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }


        public void bindPost(NewsFeedAdsModel model, int position) {

            int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(model.getPicture())
                    .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .into(img_ads);

            tv_ads_content.setMyanmarText(model.getDescription());
            btn_enter_fb_page.setOnClickListener(v -> {
                String YourPageURL = "https://www.facebook.com/n/?"+model.getWeborfblink();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(YourPageURL));

                context.startActivity(browserIntent);
            });
        }

    }
}
