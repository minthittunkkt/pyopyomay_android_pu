package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.keoeoetech.pyopyomay.NotificationDetailActivity;
import com.keoeoetech.pyopyomay.PeriodTrackerActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.NotificationModel;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_TYPE_FCM;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_TYPE_PERIOD_TRACKER;

public class NotificationAdapter extends BaseAdapter {

    private NotificationDeleteListener listener;


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NotificationAdapter.ViewHolder) holder).bindPost((NotificationModel) getItemsList().get(position), position);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {

        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public void setListener(NotificationDeleteListener listener)
    {
        this.listener = listener;
    }


    public interface NotificationDeleteListener {

        void onRemoveButtonClicked(NotificationModel model);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;

        @BindView(R.id.cv_item_notification)
        CardView cv_item_notification;
        @BindView(R.id.img_notification)
        ImageView img_notification;
        @BindView(R.id.tv_noti_content)
        MyanTextView tv_noti_content;
        @BindView(R.id.tv_noti_datetime)
        MyanTextView tv_noti_datetime;
        @BindView(R.id.btn_remove)
        MyanButton btn_remove;


        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final NotificationModel model, final int position) {

            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_notification.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_notification.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_notification.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_notification.requestLayout();
            }

            btn_remove.setMyanmarText(TextDictionaryHelper.getText(context, TextDictionaryHelper.TEXT_BUTTON_REMOVE));

            tv_noti_content.setMyanmarText(model.getContent());
            tv_noti_datetime.setMyanmarText(model.getDatetime());
            btn_remove.setOnClickListener(v -> listener.onRemoveButtonClicked(model));


            cv_item_notification.setOnClickListener(view -> {

                if (model.getType().equals(NOTI_TYPE_PERIOD_TRACKER)) {
                    Intent intent = new Intent(context,
                            PeriodTrackerActivity.class);
                    intent.putExtra("activity", "noti");
                    intent.putExtra("id", model.getId());
                    context.startActivity(intent);

                } else  if (model.getType().equals(NOTI_TYPE_FCM))  {
                    Intent intent = new Intent(context, NotificationDetailActivity.class);
                    intent.putExtra("id", model.getId());
                    context.startActivity(intent);
                }

            });


        }
    }

}
