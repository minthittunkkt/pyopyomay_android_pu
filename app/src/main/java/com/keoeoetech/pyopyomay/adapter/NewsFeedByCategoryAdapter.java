package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.NewsFeedByCategoryDetailActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.common.Pageable;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.keoeoetech.pyopyomay.model.CardModel;
import com.keoeoetech.pyopyomay.model.NewsFeedAdsModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class NewsFeedByCategoryAdapter extends BaseAdapter {

    private static final int ITEM_CONTENTS = 5;
    private static final int ITEM_ADS = 6;
    private NewsFeedByCategoryAdapter.OnNewsFeedBtnClickedListener listener;


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, final int viewType) {


        if (viewType == ITEM_CONTENTS) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_newsfeed_by_category, parent, false);
            return new NewsFeedByCategoryAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_ads, parent, false);
            return new NewsFeedByCategoryAdapter.AdsViewHolder(view);
        }

    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);

        if (viewType == ITEM_CONTENTS) {
            ((NewsFeedByCategoryAdapter.ViewHolder) holder).bindPost((ArticleModel) getItemsList().get(position), position);

        } else if (viewType == ITEM_ADS) {
            ((NewsFeedByCategoryAdapter.AdsViewHolder) holder).bindPost((NewsFeedAdsModel) getItemsList().get(position), position);
        }

    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_category_newsfeed, parent, false);

        return new NewsFeedByCategoryAdapter.HeaderHolder(view);

    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerHeader recyclerHeader = (RecyclerHeader) getItemsList().get(position);
        ((NewsFeedByCategoryAdapter.HeaderHolder) holder).bindHeader((CardModel) recyclerHeader.getHeaderData());


    }

    @Override
    public int getItemViewType(int position) {
        Pageable item = getItemsList().get(position);
        if (item instanceof ArticleModel) {
            return ITEM_CONTENTS;
        } else if (item instanceof NewsFeedAdsModel) {
            return ITEM_ADS;
        } else {
            return super.getItemViewType(position);
        }
    }

    public void setListener(OnNewsFeedBtnClickedListener listener) {
        this.listener = listener;
    }

    private boolean isSaved(String uniqueKey) {
        if (Realm.getDefaultInstance().where(ArticleModel.class).equalTo("UniqueKey", uniqueKey).findFirst() != null) {
            return true;
        } else {
            return false;
        }
    }

    public interface OnNewsFeedBtnClickedListener {

        void onVideoClicked(String videoUrl, String videoTitle);

    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item_newsfeed_by_category)
        CardView cv_item_newsfeed_by_category;
        @BindView(R.id.img_newsfeed_by_category)
        ImageView img_newsfeed_by_category;
        @BindView(R.id.video_newsfeed_by_category)
        ImageView video_newsfeed_by_category;
        @BindView(R.id.tv_newsfeed_by_category_name)
        MyanTextView tv_newsfeed_by_category_name;
        @BindView(R.id.tv_newsfeed_by_category_title)
        MyanTextView tv_newsfeed_by_category_title;
        @BindView(R.id.tv_newsfeed_by_category_date)
        MyanTextView tv_newsfeed_by_category_date;
        @BindView(R.id.tv_newsfeed_by_category_content)
        MyanTextView tv_newsfeed_by_category_content;
        @BindView(R.id.img_newsfeedbyCategory_favorite)
        ImageView img_newsfeedbyCategory_favorite;
        @BindView(R.id.layout_news_feed_by_category_video_view)
        RelativeLayout layout_news_feed_by_category_video_view;
        MyDateFormat myDateFormat;
        Realm realm;
        private Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final ArticleModel model, final int position) {


            if (isSaved(model.getUniqueKey())) {
                img_newsfeedbyCategory_favorite.setImageResource(R.mipmap.like_pink_64);
            } else {
                img_newsfeedbyCategory_favorite.setImageResource(R.mipmap.like_grey_64);
            }


            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_newsfeed_by_category.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_newsfeed_by_category.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_newsfeed_by_category.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_newsfeed_by_category.requestLayout();
            }


            int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(model.getPhoto())
                    .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .into(img_newsfeed_by_category);

            myDateFormat = new MyDateFormat();

            tv_newsfeed_by_category_name.setMyanmarText(model.getCategoryName());
            tv_newsfeed_by_category_title.setMyanmarText(model.getTitle());
            try {
                tv_newsfeed_by_category_date.setMyanmarText
                        (myDateFormat.DATE_FORMAT_DMY_TEXT.format(myDateFormat.DATE_FORMAT_YMD_HMS
                                .parse(model.getAccesstime().replace("T", " "))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tv_newsfeed_by_category_content.setMyanmarText(model.getContent());


            System.out.println("Type:" + model.getArticleType());

            switch (model.getArticleType()) {
                case PyoPyoMayConstant.PHOTO:
                    video_newsfeed_by_category.setVisibility(View.GONE);

                    size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getPhoto())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerInside()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_newsfeed_by_category);

                    break;
                case PyoPyoMayConstant.VIDEO:

                    video_newsfeed_by_category.setVisibility(View.VISIBLE);
                    size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getThumbnail())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_newsfeed_by_category);

                    break;
                default:
                    break;
            }

            layout_news_feed_by_category_video_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (model.getArticleType()) {
                        case PyoPyoMayConstant.PHOTO:
                            Intent intent = new Intent(context, NewsFeedByCategoryDetailActivity.class);
                            intent.putExtra("obj", model);
                            context.startActivity(intent);
                            break;
                        case PyoPyoMayConstant.VIDEO:
                            listener.onVideoClicked(model.getVideoUrl(), model.getTitle());

                            break;
                        default:
                            break;
                    }

                }
            });

            img_newsfeedbyCategory_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isSaved(model.getUniqueKey())) {

                        img_newsfeedbyCategory_favorite.setImageResource(R.mipmap.like_grey_64);

                        realm = Realm.getDefaultInstance();
                        final ArticleModel results = realm.where(ArticleModel.class)
                                .equalTo("UniqueKey", model.getUniqueKey()).findFirst();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                results.deleteFromRealm();
                            }
                        });

                    } else {
                        img_newsfeedbyCategory_favorite.setImageResource(R.mipmap.like_pink_64);

                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(model);
                        realm.commitTransaction();
                    }

                }

            });


          cv_item_newsfeed_by_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_ACTIVITY,
                            PyoPyoMayConstant.NEWSFEED_BY_CATEGORY_CLICK, String.valueOf(model.getUniqueKey()));

                    Intent intent = new Intent(context, NewsFeedByCategoryDetailActivity.class);
                    intent.putExtra("obj", model);
                    context.startActivity(intent);
                }
            });

        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_header)
        ImageView img_header;
        @BindView(R.id.tv_header_title)
        MyanBoldTextView tv_header_title;
        private Context context;


        public HeaderHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bindHeader(CardModel model) {

            img_header.setImageResource(model.getImageID());
            tv_header_title.setMyanmarText(model.getCategoryNameForAPI());
        }
    }


    class AdsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_item_ads)
        CardView cv_item_ads;
        @BindView(R.id.img_ads)
        ImageView img_ads;
        @BindView(R.id.tv_ads_content)
        MyanTextView tv_ads_content;
        @BindView(R.id.btn_enter_fb_page)
        MyanButton btn_enter_fb_page;
        private Context context;

        public AdsViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }


        public void bindPost(NewsFeedAdsModel model, int position) {

            int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(model.getPicture())
                    .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .into(img_ads);

            tv_ads_content.setMyanmarText(model.getDescription());
            btn_enter_fb_page.setOnClickListener(v -> {
                String YourPageURL = "https://www.facebook.com/n/?"+model.getWeborfblink();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(YourPageURL));

                context.startActivity(browserIntent);
            });

        }
    }
}
