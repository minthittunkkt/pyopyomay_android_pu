package com.keoeoetech.pyopyomay.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.model.CountryDialCodeModel;

import butterknife.ButterKnife;

/**
 * Created by user on 6/19/2015.
 **/
public class CountryDialCodeAdapter extends ArrayAdapter<CountryDialCodeModel> {

    Context mContext;
    int mLayoutResourceId;

    public CountryDialCodeAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return myGetView(position, convertView, parent);
    }


    /**
     * Returns the view for a specific item on the list
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        return myGetView(position, convertView, parent);
    }

    public View myGetView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ButterKnife.bind(this, row);

        final CountryDialCodeModel currentItem = getItem(position);


        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);

        final MyanTextView tv_title = (MyanTextView) row.findViewById(R.id.tv_title);
        tv_title.setMyanmarText( currentItem.getCode() + "-" + currentItem.getCallingCode());

        return row;
    }

}
