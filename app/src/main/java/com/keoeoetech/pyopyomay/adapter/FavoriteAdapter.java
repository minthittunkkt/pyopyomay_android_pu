package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.keoeoetech.pyopyomay.NewsFeedDetailActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAdapter extends BaseAdapter {

    private MySavedArticleClickListener listener;


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_favorite, parent, false);
        return new FavoriteAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((FavoriteAdapter.ViewHolder) holder).bindPost((ArticleModel) getItemsList().get(position), position);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    public void setListener(MySavedArticleClickListener listener) {
        this.listener = listener;
    }

    public interface MySavedArticleClickListener {

        void onRemoveButtonClicked(ArticleModel model);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;

        @BindView(R.id.cv_item_favorite)
        CardView cv_item_favorite;
        @BindView(R.id.img_item_favorite)
        ImageView img_item_favorite;
        @BindView(R.id.img_play_btn)
        ImageView img_play_btn;
        @BindView(R.id.tv_item_favorite_type)
        MyanTextView tv_item_favorite_type;
        @BindView(R.id.tv_item_favorite_content)
        MyanTextView tv_item_favorite_content;
        @BindView(R.id.btn_remove)
        MyanButton btn_remove;
        @BindView(R.id.layout_favorite_image)
        RelativeLayout layout_favorite_image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        void bindPost(final ArticleModel model, final int position) {


            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_favorite.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_favorite.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_item_favorite.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_item_favorite.requestLayout();
            }


            switch (model.getArticleType()) {
                case PyoPyoMayConstant.PHOTO:
                    img_play_btn.setVisibility(View.GONE);

                    int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getPhoto())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_item_favorite);

                    break;
                case PyoPyoMayConstant.VIDEO:

                    img_play_btn.setVisibility(View.VISIBLE);
                    size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
                    Picasso.with(context)
                            .load(model.getThumbnail())
                            .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                            .resize(size, size)
                            .centerCrop()
                            .placeholder(R.mipmap.place_holder)
                            .error(R.mipmap.place_holder)
                            .into(img_item_favorite);

                    break;
                default:
                    break;
            }


            btn_remove.setMyanmarText(TextDictionaryHelper.getText(context,TextDictionaryHelper.TEXT_BUTTON_REMOVE));
            tv_item_favorite_type.setMyanmarText(model.getTitle());
            tv_item_favorite_content.setMyanmarText(model.getContent());

            cv_item_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, NewsFeedDetailActivity.class);
                    intent.putExtra("obj", model);
                    context.startActivity(intent);


                }
            });

            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRemoveButtonClicked(model);

                }
            });


        }
    }

}
