package com.keoeoetech.pyopyomay.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.keoeoetech.pyopyomay.CSODetailActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.common.BaseAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.DpToPxHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.model.OrganizationModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CSOAdapter extends BaseAdapter {


    @Override
    protected RecyclerView.ViewHolder onCreateCustomViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cso, parent, false);
        return new CSOAdapter.ViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CSOAdapter.ViewHolder) holder).bindPost((OrganizationModel) getItemsList().get(position), position);
    }

    @Override
    protected RecyclerView.ViewHolder onCreateCustomHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected void onBindCustomHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cv_organization)
        CardView cv_organization;
        @BindView(R.id.tv_organization_name)
        MyanTextView tv_organization_name;
        @BindView(R.id.org_pic)
        ImageView org_pic;
        @BindView(R.id.tv_service)
        MyanTextView tv_service;
        private Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();

            ButterKnife.bind(this, itemView);
        }

        void bindPost(final OrganizationModel model, final int position) {

            if (position == 0) {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_organization.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 20),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_organization.requestLayout();
            } else {
                ViewGroup.MarginLayoutParams layoutParams =
                        (ViewGroup.MarginLayoutParams) cv_organization.getLayoutParams();
                layoutParams.setMargins(DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10),
                        DpToPxHelper.dpToPx(context, 20), DpToPxHelper.dpToPx(context, 10));
                cv_organization.requestLayout();
            }

            int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));
            Picasso.with(context)
                    .load(model.getPhotoUrl())
                    .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                    .resize(size, size)
                    .centerInside()
                    .placeholder(R.mipmap.ppm_placeholder)
                    .error(R.mipmap.ppm_placeholder)
                    .into(org_pic);

            tv_organization_name.setMyanmarText(model.getCSOName());
            tv_service.setMyanmarText(model.getService());

            cv_organization.setOnClickListener(v -> {

                Intent intent = new Intent(context, CSODetailActivity.class);
                intent.putExtra("obj", model);
                context.startActivity(intent);

            });


        }


    }
}
