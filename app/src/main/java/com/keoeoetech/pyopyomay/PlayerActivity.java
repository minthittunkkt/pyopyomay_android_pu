package com.keoeoetech.pyopyomay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Formatter;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wai Phyo Aung on 5/3/2017.
 **/

public class PlayerActivity extends AppCompatActivity implements ExoPlayer.EventListener {

    public static final String VIDEO_URL = "video_url";
    public static final String VIDEO_TITLE = "video_title";
    private static final String TAG = "PlayerActivity";
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    @BindView(R.id.simple_exo_player_view)
    SimpleExoPlayerView simpleExoPlayerView;
    @BindView(R.id.player_main_layout)
    FrameLayout layoutPlayer;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.play_pause_control_layout)
    RelativeLayout layoutMediaControl;
    @BindView(R.id.tv_video_title)
    MyanTextView tvVideoTitle;
    @BindView(R.id.iv_play_pause)
    ImageView ivPlayPause;
    @BindView(R.id.tv_player_time_current)
    TextView tvCurrentPlayTime;
    @BindView(R.id.tv_player_end_time)
    TextView tvEndPlayTime;
    @BindView(R.id.seek_bar)
    SeekBar sbSeek;
    @BindView(R.id.iv_full_screen)
    ImageView ivFullScreen;
    private DataSource.Factory mediaDataSourceFactory;
    private SimpleExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private boolean needRetrySource;
    private TrackGroupArray lastSeenTrackGroupArray;

    private boolean isPlaying;
    private boolean isFullScreen;
    private int resumeWindow;
    private long resumePosition;

    private boolean isMediaControlShown = true;

    private String videoUrl = "";

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isPlaying = true;
        isFullScreen = false;
        clearResumePosition();
        mediaDataSourceFactory = buildDataSourceFactory(true);
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);

        hideStatusBar();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            videoUrl = b.getString(VIDEO_URL);
            String videoTitle = b.getString(VIDEO_TITLE);
            if (!TextUtils.isEmpty(videoTitle)) {
                tvVideoTitle.setMyanmarText(videoTitle);
            }
        }

        simpleExoPlayerView.setUseController(false);
        simpleExoPlayerView.requestFocus();
    }

    private void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent: isCalled...");
        releasePlayer();
        isPlaying = true;
        isFullScreen = false;
        clearResumePosition();
        setIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: isCalled...");
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: isCalled...");
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }

        Log.d(TAG, "onResume: (URL) " + videoUrl);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: isCalled...");
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: isCalled...");
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }


    private void initializePlayer() {
        boolean needNewPlayer = player == null;
        if (needNewPlayer) {
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            lastSeenTrackGroupArray = null;

            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, new DefaultLoadControl());
            player.addListener(this);

            simpleExoPlayerView.setPlayer(player);
            player.setPlayWhenReady(isPlaying);
        }
        if (needNewPlayer || needRetrySource) {
            Uri mp4Uri = Uri.parse(videoUrl);

            MediaSource mediaSource = new ExtractorMediaSource(mp4Uri, mediaDataSourceFactory,
                    new DefaultExtractorsFactory(), null, null);
            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
            if (haveResumePosition) {
                player.seekTo(resumeWindow, resumePosition);
            }
            player.prepare(mediaSource, !haveResumePosition, false);
            needRetrySource = false;

            togglePlayPause();
            setProgress();
        }

        triggerMainPlayerLayout();

        triggerPlayPause();

        triggerSeekBar();

        triggerFullScreen();
    }

    private void releasePlayer() {
        if (player != null) {
            isPlaying = player.getPlayWhenReady();
            updateResumePosition();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    private void updateResumePosition() {
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = player.isCurrentWindowSeekable() ? Math.max(0, player.getCurrentPosition())
                : C.TIME_UNSET;
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return ((PyoPyoMay) getApplication())
                .buildDataSourceFactory(useBandwidthMeter ? BANDWIDTH_METER : null, true);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        // Do nothing.
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        // TODO: 4/28/2017 Update Button Visibilities
        if (trackGroups != lastSeenTrackGroupArray) {
            MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
            if (mappedTrackInfo != null) {
                if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
                        == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                    showToast(R.string.error_unsupported_video);
                }
            }
            lastSeenTrackGroupArray = trackGroups;
        }
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        // Do nothing.
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case ExoPlayer.STATE_BUFFERING:
                if (isPlaying) {
                    pbLoading.setVisibility(View.VISIBLE);
                }
                Log.d(TAG, "onPlayerStateChanged: BUFFERING");
                break;
            case ExoPlayer.STATE_READY:
                pbLoading.setVisibility(View.GONE);
                Log.d(TAG, "onPlayerStateChanged: READY");
                break;
            case ExoPlayer.STATE_ENDED:
                player.seekTo(0);
                player.setPlayWhenReady(false);
                isPlaying = false;
                togglePlayPause();
                setProgress();
                tvCurrentPlayTime.setText(getString(R.string._00_00));
                Log.d(TAG, "onPlayerStateChanged: ENDED");
                break;
            case ExoPlayer.STATE_IDLE:
                Log.d(TAG, "onPlayerStateChanged: IDLE");
                break;
        }
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onPlayerError(ExoPlaybackException e) {
        String errorString = null;
        if (e.type == ExoPlaybackException.TYPE_RENDERER) {
            Exception cause = e.getRendererException();
            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString = getString(R.string.error_no_secure_decoder,
                                decoderInitializationException.mimeType);
                    } else {
                        errorString = getString(R.string.error_no_decoder,
                                decoderInitializationException.mimeType);
                    }
                } else {
                    errorString = getString(R.string.error_instantiating_decoder,
                            decoderInitializationException.decoderName);
                }
            }
        }
        if (errorString != null) {
            showToast(errorString);
            Log.e(TAG, "onPlayerError: " + errorString);
        }
        needRetrySource = true;
        if (isBehindLiveWindow(e)) {
            clearResumePosition();
            initializePlayer();
        } else {
            updateResumePosition();
            // TODO: 4/28/2017 Update Button Visibilities
            // TODO: 4/28/2017 Show Controls
        }
    }

    @Override
    public void onPositionDiscontinuity() {
        if (needRetrySource) {
            updateResumePosition();
        }
    }

    private void togglePlayPause() {
        if (isPlaying) {
            ivPlayPause.setImageResource(R.mipmap.ic_pause);
            layoutMediaControl.setVisibility(View.GONE);
            isMediaControlShown = false;
        } else {
            ivPlayPause.setImageResource(R.mipmap.ic_play);
            layoutMediaControl.setVisibility(View.VISIBLE);
            isMediaControlShown = true;
        }
    }

    private void setProgress() {
        sbSeek.setProgress(0);
        sbSeek.setSecondaryProgress(0);
        sbSeek.setMax(0);
        sbSeek.setMax((int) player.getDuration() / 1000);

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (player != null && isPlaying) {
                    sbSeek.setMax(0);
                    sbSeek.setMax((int) player.getDuration() / 1000);
                    int mCurrentPosition = (int) player.getCurrentPosition() / 1000;
                    sbSeek.setProgress(mCurrentPosition);
                    int mBufferedPosition = (int) player.getBufferedPosition() / 1000;
                    sbSeek.setSecondaryProgress(mBufferedPosition);
                    tvCurrentPlayTime.setText(stringForTime((int) player.getCurrentPosition()));
                    tvEndPlayTime.setText(stringForTime((int) player.getDuration()));

                    handler.postDelayed(this, 1000);
                }
            }
        });
    }

    private String stringForTime(int timeMs) {
        StringBuilder formatBuilder = new StringBuilder();
        Formatter formatter = new Formatter(formatBuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        formatBuilder.setLength(0);
        if (hours > 0) {
            return formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return formatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void triggerMainPlayerLayout() {
        final Handler handler = new Handler();
        final Runnable hideControlRunnable = new Runnable() {
            @Override
            public void run() {
                if (isPlaying) {
                    layoutMediaControl.setVisibility(View.GONE);
                    isMediaControlShown = false;
                }
            }
        };
        layoutPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isMediaControlShown) {
                    layoutMediaControl.setVisibility(View.VISIBLE);
                    isMediaControlShown = true;
                    handler.postDelayed(hideControlRunnable, 3000);
                } else {
                    if (isPlaying) {
                        layoutMediaControl.setVisibility(View.GONE);
                        isMediaControlShown = false;
                        handler.removeCallbacks(hideControlRunnable);
                    }
                }
            }
        });
    }

    private void triggerPlayPause() {
        ivPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying) {
                    player.setPlayWhenReady(false);
                    isPlaying = false;
                    togglePlayPause();
                } else {
                    player.setPlayWhenReady(true);
                    isPlaying = true;
                    togglePlayPause();
                    setProgress();
                }
            }
        });
    }

    private void triggerSeekBar() {
        sbSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    return;
                }

                player.seekTo(progress * 1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Do nothing.
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Do nothing.
            }
        });

        sbSeek.setMax(0);
        sbSeek.setMax((int) player.getDuration() / 1000);
    }

    private void triggerFullScreen() {
        ivFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFullScreen) {     // close full screen mode
                    ivFullScreen.setImageResource(R.mipmap.ic_full_screen_on);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    isFullScreen = false;
                } else {                // open full screen mode
                    ivFullScreen.setImageResource(R.mipmap.ic_full_screen_off);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    isFullScreen = true;
                }
            }
        });
    }
}
