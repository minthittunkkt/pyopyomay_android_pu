package com.keoeoetech.pyopyomay;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.keoeoetech.pyopyomay.adapter.QuestionAnswerAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.DialogHelperWithOkCancelButton;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.interfaces.AnswerSelectionCallback;
import com.keoeoetech.pyopyomay.model.AnswersModel;
import com.keoeoetech.pyopyomay.model.AnswersViewModel;
import com.keoeoetech.pyopyomay.model.LessonModel;
import com.keoeoetech.pyopyomay.model.QuestionModel;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;


public class QuestionAnswerActivity extends BaseActivity
        implements AnswerSelectionCallback {

    @BindView(R.id.relative_result)
    RelativeLayout relative_result;
    @BindView(R.id.tv_question)
    MyanTextView tv_question;
    @BindView(R.id.tv_total_marks)
    MyanTextView tv_total_marks;
    @BindView(R.id.tv_result_text)
    MyanTextView tv_result_text;
    @BindView(R.id.cv_result_message)
    CardView cv_result_message;
    @BindView(R.id.btn_finish)
    MyanButton btn_finish;
    @BindView(R.id.label_hint)
    MyanBoldTextView label_hint;
    @BindView(R.id.text_hint)
    MyanTextView text_hint;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.btn_answer)
    MyanButton btn_answer;
    @BindView(R.id.btn_next)
    MyanButton btn_next;
    @BindView(R.id.sb_break_point)
    SeekBar sb_break_point;
    @BindView(R.id.linear_seekbar)
    LinearLayout linear_seekbar;
    @BindView(R.id.img_quizz_answer)
    ImageView img_quizz_answer;
    DialogHelperWithOkCancelButton dialogHelperWithOkCancelButton;
    private QuestionAnswerAdapter questionAnswerAdapter;
    private LessonModel lesson;
    private ArrayList<QuestionModel> questionList;
    private int questionIndex = 0;
    private AnswersViewModel selectedAnswer = null;
    private Handler handler;
    private int totalMask = 0;
    private ProgressDialog progressDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_question_answer;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getQuestionList();



        sb_break_point.setMax(lesson.getBreakpoint());
        btn_answer.setOnClickListener(v -> {

            if (selectedAnswer != null)
                showResultMessage();
        });

    }

    private void init() {

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.QUESTION_ANSWER_ACTIVITY);
        handler = new Handler();
        lesson = Realm.getDefaultInstance().where(LessonModel.class)
                .equalTo("LessonId", getIntent().getIntExtra("id", 0))
                .findFirst();

        setupToolbar(true);
        setupToolbarText(lesson.getTitleInMyanmarUnicode());

        dialogHelperWithOkCancelButton = new DialogHelperWithOkCancelButton(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading....");

        if(lesson.getBreakpoint()==0)
        {
            linear_seekbar.setVisibility(View.GONE);
        }
        btn_answer.setMyanmarText(TextDictionaryHelper.getText(this.getApplicationContext(), TextDictionaryHelper.TEXT_BUTTON_ANSWER));

        btn_finish.setMyanmarText(TextDictionaryHelper.getText(this.getApplicationContext(), TextDictionaryHelper.TEXT_BUTTON_FINISH));
        label_hint.setMyanmarText(TextDictionaryHelper.getText(this.getApplicationContext(), TextDictionaryHelper.TEXT_LABEL_HINT));

        questionList = new ArrayList<>();
        questionAnswerAdapter = new QuestionAnswerAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setAdapter(questionAnswerAdapter);
        recyclerview.setHasFixedSize(true);
    }

    private void getQuestionList() {
        RealmResults<QuestionModel> results = Realm.getDefaultInstance()
                .where(QuestionModel.class).
                        equalTo("LessonId",
                                lesson.getLessonId()).findAll();

        questionList.addAll(results);

        bindQuestionAnswer();
    }

    private void bindQuestionAnswer() {

        QuestionModel question = questionList.get(questionIndex);

        questionAnswerAdapter.clear();
        tv_question.setMyanmarText(question.getQuestionInUnicode());

        RealmResults<AnswersModel> answerList = Realm.getDefaultInstance()
                .where(AnswersModel.class)
                .equalTo("QuestionId", question.getQuestionId()).findAll();


        for (int i = 0; i < answerList.size(); i++) {
            AnswersModel answersModel = answerList.get(i);

            AnswersViewModel model = new AnswersViewModel();

            model.setAnswerId(answersModel.getAnswerId());
            model.setQuestionId(answersModel.getQuestionId());
            model.setLessonId(answersModel.getLessonId());
            model.setAnswerInUnicode(answersModel.getAnswerInUnicode());
            model.setAnswerInEnglish(answersModel.getAnswerInEnglish());
            model.setAnswerInZawgyi(answersModel.getAnswerInZawgyi());
            model.setAnswerInMon(answersModel.getAnswerInMon());
            model.setAnswerInSagawKaren(answersModel.getAnswerInSagawKaren());
            model.setAnswerInShan(answersModel.getAnswerInShan());
            model.setAnswerInPoeKaren(answersModel.getAnswerInPoeKaren());
            model.setAnswerInKaChin(answersModel.getAnswerInKaChin());
            model.setAnswerInRaKhine(answersModel.getAnswerInRaKhine());
            model.setTipsInUnicode(answersModel.getTipsInUnicode());
            model.setTipsInEnglish(answersModel.getTipsInEnglish());
            model.setTipsInZawgyi(answersModel.getTipsInZawgyi());
            model.setTipsInMon(answersModel.getTipsInMon());
            model.setTipsInSagawKaren(answersModel.getTipsInSagawKaren());
            model.setTipsInShan(answersModel.getTipsInShan());
            model.setTipsInPoeKaren(answersModel.getTipsInPoeKaren());
            model.setTipsInKaChin(answersModel.getTipsInKaChin());
            model.setTipsInRaKhine(answersModel.getTipsInRaKhine());
            model.setPhoto(answersModel.getPhoto());
            model.setDeleted(answersModel.isDeleted());
            model.setAccesstime(answersModel.getAccesstime());
            model.setActive(answersModel.isActive());
            model.setPostedUser(answersModel.getPostedUser());
            model.setPoint(answersModel.getPoint());
            model.setRight(answersModel.isRight());
            model.setSelected(false);

            questionAnswerAdapter.add(model);

        }


        progressDialog.show();
        handler.postDelayed(() -> {
            questionAnswerAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }, 500);

    }


    private void nextQuestion() {
        if (questionIndex != (questionList.size() - 1)) {

            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.QUESTION_ANSWER_ACTIVITY,
                    PyoPyoMayConstant.QUESTION_ID,
                    String.valueOf(questionList.get(questionIndex).QuestionId));

            questionIndex++;
            bindQuestionAnswer();

        } else {
            questionAnswerAdapter.clear();

            //End question
            showTotalMark();
        }

    }

    @Override
    public void select(int position, AnswersViewModel model) {

        btn_answer.setEnabled(true);
        selectedAnswer = model;

        questionAnswerAdapter.replace(position, model);
        questionAnswerAdapter.notifyDataSetChanged();

        for (int i = 0; i < questionAnswerAdapter.getItemCount(); i++) {
            if (i != position) {
                AnswersViewModel answersViewModel =
                        (AnswersViewModel) questionAnswerAdapter
                                .getItemsList().get(i);
                answersViewModel.setSelected(false);
                questionAnswerAdapter.replace(i, answersViewModel);
            }
        }
        questionAnswerAdapter.notifyDataSetChanged();

    }

    private void showResultMessage() {

        Log.d("THN", "Selected Answer " + selectedAnswer.isSelected);

        if (selectedAnswer != null) {

            if (selectedAnswer.isSelected()) {
                PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.QUESTION_ANSWER_ACTIVITY,
                        PyoPyoMayConstant.ANSWER_ID, String.valueOf(selectedAnswer.getAnswerId()));
                Log.d("THN", "showResultMessage: " + selectedAnswer.toString());

                progressDialog.show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        questionAnswerAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }
                }, 400);


                recyclerview.setVisibility(View.INVISIBLE);
                cv_result_message.setVisibility(View.VISIBLE);
                text_hint.setVisibility(View.VISIBLE);
                text_hint.setMyanmarText(selectedAnswer.getTipsInUnicode());
                totalMask += selectedAnswer.getPoint();

                sb_break_point.setProgress(totalMask);

                btn_answer.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);

                btn_next.setOnClickListener(v -> {

                    recyclerview.setVisibility(View.VISIBLE);
                    cv_result_message.setVisibility(View.INVISIBLE);
                    btn_next.setVisibility(View.GONE);
                    btn_answer.setVisibility(View.VISIBLE);
                    btn_answer.setEnabled(false);
                    nextQuestion();
                });

                selectedAnswer = null;
            }

        }


    }


    private void showTotalMark() {
        relative_result.setVisibility(View.VISIBLE);

        if (lesson.getBreakpoint() == 0) {
            img_quizz_answer.setImageResource(R.mipmap.suggestion_512);
            tv_total_marks.setMyanmarText("ဖော်ပြပါသင်ခန်းစာများရှိမေးခွန်းများသည် ");
            tv_result_text.setVisibility(View.VISIBLE);
            tv_result_text.setMyanmarText("အကြံပြုမေးခွန်းများသာဖြစ်သည်။");
        } else {
            img_quizz_answer.setImageResource(R.mipmap.marks_512);
            tv_total_marks.setMyanmarText(TextDictionaryHelper.getText(this.getApplicationContext(),
                    TextDictionaryHelper.TEXT_TOTAL_MARKS) + totalMask + "/" + lesson.getBreakpoint() +
                    TextDictionaryHelper.getText(this.getApplicationContext(), TextDictionaryHelper.TEXT_POINTS));

        }


        btn_finish.setOnClickListener(v -> finish());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {

        dialogHelperWithOkCancelButton.showDialog(R.mipmap.profile_data_fill,
                "သေချာပါသလား", "ထွက်မည်",
                "မထွက်ပါ");

        dialogHelperWithOkCancelButton.getBtnOk().
                setOnClickListener(v -> QuestionAnswerActivity.super.onBackPressed());

        dialogHelperWithOkCancelButton.getBtnCancel()
                .setOnClickListener(v -> dialogHelperWithOkCancelButton.hideDialog());

    }

}

