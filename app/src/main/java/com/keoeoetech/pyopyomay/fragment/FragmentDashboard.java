package com.keoeoetech.pyopyomay.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.adapter.DashboardAdapter;
import com.keoeoetech.pyopyomay.helper.RVSpanCountHelper;
import com.keoeoetech.pyopyomay.model.DashboardModel;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentDashboard extends Fragment {

    @BindView(R.id.rv_dasboard)
    RecyclerView rv_dashboard;
    private DashboardAdapter dashboardAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard,
                container, false);
        ButterKnife.bind(this, view);

        init();
        setupData();

        return view;
    }


    private void init() {
        dashboardAdapter=new DashboardAdapter();

        int spanCount = RVSpanCountHelper.getSpanCount(getActivity().getWindowManager());
        rv_dashboard.setLayoutManager(new GridLayoutManager(getActivity(), spanCount));
        rv_dashboard.setAdapter(dashboardAdapter);
    }

    private void setupData() {

        DashboardModel model1=new DashboardModel();
        model1.setId(1);
        model1.setTitle(getString(R.string.str_favorite_data));
        model1.setIcon(R.mipmap.favorite_dashboard_icon);
        dashboardAdapter.add(model1);

        DashboardModel model2=new DashboardModel();
        model2.setId(2);
        model2.setTitle(getString(R.string.str_cso_directory));
        model2.setIcon(R.mipmap.cso_dashboard_icon);
        dashboardAdapter.add(model2);

    }


}
