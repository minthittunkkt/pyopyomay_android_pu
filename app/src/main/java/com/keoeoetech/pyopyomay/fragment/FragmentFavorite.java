package com.keoeoetech.pyopyomay.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.adapter.FavoriteAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.dialog.MySavedArticlesDeleteDialog;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.ArticleModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class FragmentFavorite extends Fragment implements FavoriteAdapter.MySavedArticleClickListener {
    @BindView(R.id.rv_favorite)
    RecyclerView rv_favorite;
    FavoriteAdapter favoriteAdapter;

    @BindView(R.id.savePostEmptyView)
    RelativeLayout savePostEmptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    private Realm realm;

    private RealmResults<ArticleModel> results;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite,
                container, false);
        ButterKnife.bind(this, view);

        init();

        getAllSavedNewsFeed();

        return view;
    }


    private void getAllSavedNewsFeed() {

        results = realm.where(ArticleModel.class).findAll();

        if (!results.isEmpty()) {
            setupFavoriteData(results);
        }

        results.addChangeListener(new RealmChangeListener<RealmResults<ArticleModel>>() {
            @Override
            public void onChange(@NonNull RealmResults<ArticleModel> articleModels) {
                Log.d("RM", "onChange: " + articleModels.size());

                setupFavoriteData(articleModels);
            }
        });
    }

    private void setupFavoriteData(List<ArticleModel> articleModels) {
        favoriteAdapter.clear();

        for (ArticleModel model : articleModels) {

            favoriteAdapter.add(model);

            Log.d("CMA", "getAllSavedNewsFeed: " + model.getTitle());
        }

        if (articleModels.size() == 0) {
            //favoriteAdapter.showEmptyView(R.layout.empty_layout);

            tv_empty.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_EMPTY_DATA));
            savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }

    private void init() {

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.FAVORITE_FRAGMENT);

        realm = Realm.getDefaultInstance();

        favoriteAdapter = new FavoriteAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_favorite.setLayoutManager(linearLayoutManager);
        rv_favorite.setAdapter(favoriteAdapter);
        favoriteAdapter.setListener(this);

    }

    @Override
    public void onRemoveButtonClicked(final ArticleModel model) {


        PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.FAVORITE_FRAGMENT,
                PyoPyoMayConstant.FAVORITE_DELETE_CLICK,
                String.valueOf(model.getUniqueKey()));

        Log.i("CMA", "onRemoveButtonClicked: " + model.getTitle());
        MySavedArticlesDeleteDialog savedArticlesDeleteDialog = new MySavedArticlesDeleteDialog();
        savedArticlesDeleteDialog.setListener(new MySavedArticlesDeleteDialog.OnMySavedArticlesDeleteDialogListener() {
            @Override
            public void onDeleteClicked() {
                final ArticleModel results = realm.where(ArticleModel.class).equalTo("UniqueKey",
                        model.getUniqueKey()).findFirst();

                if (results != null) {


                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            results.deleteFromRealm();
                            favoriteAdapter.clear();
                            favoriteAdapter.notifyDataSetChanged();

                        }
                    });
                } else {
                    Log.d("THN", "onRemoveButtonClicked: deleted ID not found");
                }
            }
        });

        savedArticlesDeleteDialog.show(getFragmentManager(),
                savedArticlesDeleteDialog.getClass().getName());

    }


    @Override
    public void onResume() {
        super.onResume();

        if (results.size() == 0) {
            //favoriteAdapter.showEmptyView(R.layout.empty_layout);

            tv_empty.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_EMPTY_DATA));
            savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }
}
