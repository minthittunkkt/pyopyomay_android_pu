package com.keoeoetech.pyopyomay.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.adapter.NotificationAdapter;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.dialog.MySavedArticlesDeleteDialog;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.model.NotificationModel;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class FragmentNotification extends Fragment implements NotificationAdapter.NotificationDeleteListener {

    NotificationAdapter notificationAdapter;
    Realm realm;
    private MyDateFormat myDateFormat;
    Calendar cal;


    @BindView(R.id.rv_notification)
    RecyclerView rv_notification;
    @BindView(R.id.savePostEmptyView)
    RelativeLayout savePostEmptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    RealmResults<NotificationModel> result;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification,
                container, false);
        ButterKnife.bind(this, view);

        init();
        getAllNotificationData();
        return view;
    }


    private void init() {

        realm = Realm.getDefaultInstance();
        myDateFormat = new MyDateFormat();
        cal = Calendar.getInstance();

        notificationAdapter = new NotificationAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        rv_notification.setLayoutManager(linearLayoutManager);
        rv_notification.setAdapter(notificationAdapter);
        notificationAdapter.setListener(this);

    }

    private void getAllNotificationData() {

        //notificationAdapter.clear();

        result = Realm.getDefaultInstance()
                .where(NotificationModel.class).findAll();

        if (!result.isEmpty()) {

            setupNotiData(result);
        }

        result.addChangeListener(new RealmChangeListener<RealmResults
                <NotificationModel>>() {
            @Override
            public void onChange(@NonNull RealmResults<NotificationModel> notificationModels) {
                Log.d("RM", "onChange: " + notificationModels.size());

                setupNotiData(notificationModels);
            }
        });

    }

    private void setupNotiData(List<NotificationModel> notificationModels) {
        notificationAdapter.clear();

        for (NotificationModel model :
                notificationModels) {

            notificationAdapter.add(model);
        }
        if (notificationModels.size() == 0) {
            tv_empty.setMyanmarText(getString(R.string.empty_noti_data));
        savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onRemoveButtonClicked(final NotificationModel model) {
        MySavedArticlesDeleteDialog savedArticlesDeleteDialog = new MySavedArticlesDeleteDialog();
        savedArticlesDeleteDialog.setListener(new MySavedArticlesDeleteDialog.OnMySavedArticlesDeleteDialogListener() {
            @Override
            public void onDeleteClicked() {
                final NotificationModel results = realm.where
                        (NotificationModel.class).equalTo("id",
                        model.getId()).findFirst();

                if (results != null) {


                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(@NonNull Realm realm) {
                            results.deleteFromRealm();
                            notificationAdapter.clear();
                            notificationAdapter.notifyDataSetChanged();

                        }
                    });
                } else {
                    Log.d("THN", "onRemoveButtonClicked: deleted ID not found");
                }
            }
        });

        savedArticlesDeleteDialog.show(getFragmentManager(),
                savedArticlesDeleteDialog.getClass().getName());

    }

    @Override
    public void onResume() {
        super.onResume();
        if (result.size() == 0) {
            tv_empty.setMyanmarText(getString(R.string.empty_noti_data));
            savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }
}