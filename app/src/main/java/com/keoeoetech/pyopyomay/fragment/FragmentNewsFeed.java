package com.keoeoetech.pyopyomay.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.keoeoetech.pyopyomay.PlayerActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.adapter.NewsFeedAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.EndlessRecyclerViewScrollListener;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.CheckScreenSizeHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.NewsFeedCustomModel;
import com.keoeoetech.pyopyomay.model.NewsFeedHeaderModel;
import com.keoeoetech.pyopyomay.model.NewsFeedModel;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNewsFeed extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener,
        NewsFeedAdapter.OnNewsFeedBtnClickedListener{


    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.rv_newsfeed)
    ShimmerRecyclerView rv_newsfeed;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    SharePreferenceHelper sharePreferenceHelper;
    private ServiceHelper.ApiService service;
    private Call<NewsFeedCustomModel> newsFeedCustomModelCall;
    private NewsFeedAdapter newsFeedAdapter;
    private boolean isLoading;
    private boolean isEnd;
    private int page = 1;
    private int pageSize = 10;
    private String app = "pyopyomay";
    private String userphoneno = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_feed, container,
                false);
        ButterKnife.bind(this, view);

        init();
        getHeader();
        getNewFeedsData();

        return view;
    }

    private void init() {

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.NEWSFEED_FRAGMENT);

        sharePreferenceHelper = new SharePreferenceHelper(getContext());
        userphoneno = sharePreferenceHelper.getPhoneNo();

        service = ServiceHelper.getClient(getContext());
        newsFeedAdapter = new NewsFeedAdapter();
        swipe_refresh_layout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        swipe_refresh_layout.setOnRefreshListener(this);
        rv_newsfeed.setHasFixedSize(true);

        tv_empty.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_NO_DATA_AVAILABLE));

        if (CheckScreenSizeHelper.isTablet(getContext())) {
            rv_newsfeed.setLayoutManager(new GridLayoutManager(getContext(), 2));
        } else {
            rv_newsfeed.setLayoutManager(new GridLayoutManager(getContext(), 1));
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_newsfeed.setLayoutManager(linearLayoutManager);
        rv_newsfeed.setAdapter(newsFeedAdapter);

        rv_newsfeed.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager)
                rv_newsfeed.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isLoading && !isEnd) {
                    getNewFeedsData();
                } else if (isEnd) {
                    //Toast.makeText(getContext(), R.string.string_no_more_data, Toast.LENGTH_SHORT).show();
                }
            }
        });

        newsFeedAdapter.setListener(this);

    }

    private void getHeader() {
        NewsFeedHeaderModel model = new NewsFeedHeaderModel();
        model.setTitle("Title");
        newsFeedAdapter.addHeader(model);
    }


    private void getNewFeedsData() {

        if (page == 1) {
            rv_newsfeed.showShimmerAdapter();
        }
        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);

        newsFeedAdapter.clearFooter();
        rv_newsfeed.post(() -> {
            newsFeedAdapter.showLoading();
            emptyView.setVisibility(View.GONE);
        });

        newsFeedCustomModelCall = service.getAllNewFeedData(userphoneno, page, pageSize, app);
        newsFeedCustomModelCall.enqueue(new Callback<NewsFeedCustomModel>() {
            @Override
            public void onResponse(Call<NewsFeedCustomModel> call, Response<NewsFeedCustomModel> response) {

                if (page == 1) {
                    rv_newsfeed.hideShimmerAdapter();
                }
                isLoading = false;
                newsFeedAdapter.clearFooter();

                if (response.isSuccessful()) {
                    NewsFeedCustomModel result = response.body();
                    ArrayList<NewsFeedModel> resultList = result.getResults();

                    if (page == 1 && resultList.isEmpty()) {
                        //newsFeedAdapter.showEmptyView(R.layout.empty_layout);
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        emptyView.setVisibility(View.GONE);

                        isEnd = result.getResults().isEmpty();
                        for (int i = 0; i < resultList.size(); i++) {
                            if (resultList.get(i).getAds() != null) {
                                newsFeedAdapter.add(resultList.get(i).getAds());
                            }
                            newsFeedAdapter.add(resultList.get(i).getArticle());

                        }

                        page++;
                    }


                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<NewsFeedCustomModel> call, Throwable t) {
                handleFailure();
            }


            public void handleFailure() {
                if (page == 1) {
                    rv_newsfeed.hideShimmerAdapter();
                }
                newsFeedAdapter.clearFooter();
                newsFeedAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, () -> getNewFeedsData());
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (newsFeedCustomModelCall != null) {
            newsFeedCustomModelCall.cancel();
        }
    }

    @Override
    public void onRefresh() {
        page = 1;
        newsFeedAdapter.clear();
        getHeader();
        getNewFeedsData();


        newsFeedAdapter.notifyDataSetChanged();

    }


    public void onVideoClicked(String videoUrl, String videoTitle) {

        Intent intentPlayer = new Intent(getActivity(), PlayerActivity.class);
        intentPlayer.putExtra(PlayerActivity.VIDEO_URL, videoUrl);
        intentPlayer.putExtra(PlayerActivity.VIDEO_TITLE, videoTitle);
        startActivity(intentPlayer);

    }

    @Override
    public void onResume() {
        super.onResume();
        newsFeedAdapter.notifyDataSetChanged();

    }


}