package com.keoeoetech.pyopyomay.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.keoeoetech.pyopyomay.BuildConfig;
import com.keoeoetech.pyopyomay.ProfileUpdateActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.ReferralPinCodeActivity;
import com.keoeoetech.pyopyomay.SplashActivity;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.BitmapTransform;
import com.keoeoetech.pyopyomay.helper.CameraHelper;
import com.keoeoetech.pyopyomay.helper.DialogHelperWithOkCancelButton;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.interfaces.CameraCallback;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.BASE_URL;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.PHOTO_URL;

public class FragmentSetting extends Fragment implements UploadStatusDelegate, CameraCallback {


    @BindView(R.id.img_profile)
    ImageView img_profile;
    @BindView(R.id.tv_profile_name)
    MyanTextView tv_profile_name;
    @BindView(R.id.tv_edit_profile)
    MyanTextView tv_edit_profile;
    @BindView(R.id.tv_help_and_support)
    MyanTextView tv_help_and_support;
    @BindView(R.id.tv_sign_out)
    MyanTextView tv_sign_out;

    @BindView(R.id.linear_profile)
    LinearLayout linear_profile;
    @BindView(R.id.linear_referal)
    LinearLayout linear_referal;
    @BindView(R.id.linear_help_and_support)
    LinearLayout linear_help_and_support;
    @BindView(R.id.linear_sign_out)
    LinearLayout linear_sign_out;

    private SharePreferenceHelper sharePreferenceHelper;

    private String messengerID = "326875798071433";
    PyoPyoMayUserModel pyoPyoMayUserModel;
    CameraHelper cameraHelper;
    private ProgressDialog progressDialog;
    private String picturePath;
    private String uploadPath = "";
    private Handler handler;
    private boolean isPhotoSelected = false;
    int size = (int) Math.ceil(Math.sqrt(PyoPyoMayConstant.MAX_WIDTH * PyoPyoMayConstant.MAX_HEIGHT));


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);

        init();

        linear_referal.setOnClickListener(v -> {

            Intent intent = new Intent(getContext(), ReferralPinCodeActivity.class);
            startActivity(intent);

        });

        setMyanmarText();
        getMemberInfo();
        photoUpload();
        helpAndSupportClick();
        profileClick();
        signOut();

        return view;

    }

    private void init() {
        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.SETTING_FRAGMENT);
        sharePreferenceHelper = new SharePreferenceHelper(getContext());
        pyoPyoMayUserModel = Realm.getDefaultInstance().where(PyoPyoMayUserModel.class).findFirst();
        handler = new Handler();
        progressDialog = new ProgressDialog(getActivity());
        cameraHelper = new CameraHelper(getApplicationContext());
        cameraHelper.registerCallback(this);
    }

    private void setMyanmarText() {
        tv_edit_profile.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_PROFILE));
        tv_help_and_support.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_HELP_AND_SUPPORT));
        tv_sign_out.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_SIGN_OUT));
    }

    private void profileClick() {
        linear_profile.setOnClickListener(v -> {
            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.SETTING_FRAGMENT, PyoPyoMayConstant.PROFILE_CLICK, "Profile Click");

            Intent intent = new Intent(getContext(), ProfileUpdateActivity.class);
            startActivity(intent);
        });
    }


    private void getMemberInfo() {
        PyoPyoMayUserModel model = Realm.getDefaultInstance().where(PyoPyoMayUserModel.class)
                .equalTo("MemberId", sharePreferenceHelper.getLoginMemberId()).findFirst();
        if (model.getMemberName() != null) {
            if (!model.getMemberName().isEmpty()) {

                Picasso.with(getContext())
                        .load(PHOTO_URL + model.getMemberProfilePhoto())
                        .transform(new BitmapTransform(PyoPyoMayConstant.MAX_WIDTH, PyoPyoMayConstant.MAX_HEIGHT))
                        .resize(size, size)
                        .centerInside()
                        .placeholder(R.mipmap.place_holder)
                        .error(R.mipmap.place_holder)
                        .into(img_profile);

                tv_profile_name.setMyanmarText(model.getMemberName());
            }
        }
    }

    private void helpAndSupportClick() {
        linear_help_and_support.setOnClickListener(v -> {
            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.SETTING_FRAGMENT,
                    PyoPyoMayConstant.MESSENGER_CLICK, "Messenger Click");
            messengerClick(messengerID);

        });
    }

    private void messengerClick(String messengerID) {
        System.out.println("Messenger Clicked");
        if (!TextUtils.isEmpty(messengerID)) {
            callFacebookChat(messengerID);
            Log.d("Messenger ID", messengerID);
        }
    }

    private void callFacebookChat(String messengerID) {
        String uri = "fb://messaging/" + messengerID.trim();
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));

        try {
            startActivity(sendIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            callFacebookMessenger(messengerID);
        }

        System.out.println("Facebook Messenger ID: " + messengerID);
    }

    private void callFacebookMessenger(String messengerID) {
        String uri = "fb-messenger://user/" + messengerID.trim();
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));

        try {
            startActivity(sendIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "Messenger ရွိမွသုံးလို႔ရပါမည။္", Toast.LENGTH_SHORT).show();
        }

        System.out.println("Messenger ID: " + messengerID);
    }


    private void photoUpload() {
        img_profile.setOnClickListener(v -> showPictureDialog());
        }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            callGallery();
                            break;
                        case 1:
                            callCamera();
                            break;
                    }
                });
        pictureDialog.show();
    }


    private void callGallery() {

        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 111);

    }

    private void galleryProcess(Intent data) {

        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = null;
        if (selectedImage != null) {
            cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
        }
        if (cursor != null) {

            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        Bitmap bmImg = BitmapFactory.decodeFile(picturePath, options);
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, picturePath);

        cameraHelper.saveImage(temp);

    }

    private void callCamera() {

        sharePreferenceHelper.setTempPhotoName(CameraHelper.getDateTImeStampForFileName() + ".jpg");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File pictureDirectory = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    CameraHelper.TEMP_FOLDER_NAME);
            if (!pictureDirectory.exists()) {
                pictureDirectory.mkdirs();
            }
            File file = new File(pictureDirectory.getAbsolutePath() + File.separator
                    + sharePreferenceHelper.getTempPhotoName());
            sharePreferenceHelper.setTempPhotoPath(file.getAbsolutePath());
            // Continue only if the File was successfully created
            Uri photoURI = FileProvider.getUriForFile(getContext(),
                    BuildConfig.APPLICATION_ID + ".provider", file);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, 222);
        }
    }

    private void cameraProcess() {
        File pictureDirectory = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                CameraHelper.TEMP_FOLDER_NAME);
        File file = new File(pictureDirectory.getAbsolutePath()
                + File.separator + sharePreferenceHelper.getTempPhotoName());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;// reduce images resolution
        Bitmap bmImg = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        Bitmap temp = cameraHelper.rotateBitmap(bmImg, file.getAbsolutePath());
        cameraHelper.saveImage(temp);

        progressDialog.dismiss();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 111) {
                galleryProcess(data);
            }else if (requestCode == 222) {
                progressDialog.show();
                handler.postDelayed(() -> cameraProcess(), 2000);
            }
        }

    }

    @Override
    public void getFilePath(String path) {

        uploadPath = path;
        isPhotoSelected = true;
        img_profile.setImageURI(Uri.parse(path));
        uploadMultipart(uploadPath, sharePreferenceHelper.getLoginMemberId());

    }



    @Override
    public void onResume() {
        super.onResume();

        if (!checkWriteExternalStoragePermission() && !checkReadExternalStoragePermission() && !checkCameraPermission()) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA}, 1);
        }
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }



    private void signOut() {

        linear_sign_out.setOnClickListener(v -> {

            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.SETTING_FRAGMENT, PyoPyoMayConstant.LOGOUT_CLICK, "Logout Click");

            final DialogHelperWithOkCancelButton dialogHelperWithOkCancelButton=new DialogHelperWithOkCancelButton(getContext());
            dialogHelperWithOkCancelButton.showDialog(R.mipmap.logout_512,
                    TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_LOGOUT_MESSAGE),
                    TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_BTN_LOGOUT_OK),
                    TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_BTN_LOGOUT_CANCEL));

            dialogHelperWithOkCancelButton.getBtnOk().setOnClickListener(v12 -> {
                sharePreferenceHelper.logout();
                Intent intent = new Intent(getContext(), SplashActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                getContext().startActivity(intent);

            });

            dialogHelperWithOkCancelButton.getBtnCancel().setOnClickListener(v1 -> dialogHelperWithOkCancelButton.hideDialog());


        });


    }

    private void uploadMultipart(String path, int memberid) {

        Log.d("UploadMultipart", path + "\n" + memberid);

        try {
            MultipartUploadRequest req =
                    new MultipartUploadRequest(getApplicationContext(), BASE_URL +
                            "member/uploadPhoto")
                            .addFileToUpload(path, "file")
                            .addParameter("MemberId", String.valueOf(memberid))
                            .setUtf8Charset()
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2);
            req.setDelegate(this).startUpload();
        } catch (Exception exc) {
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {
        progressDialog.show();
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        progressDialog.dismiss();
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {

        Log.d("ServerRes", serverResponse.getBodyAsString());

        Gson gson = new Gson();
        PyoPyoMayUserModel userModel = gson.fromJson(serverResponse.getBodyAsString(),
                new TypeToken<PyoPyoMayUserModel>(){}.getType());


        if(userModel!=null) {

            Realm.getDefaultInstance().beginTransaction();
            Realm.getDefaultInstance().copyToRealmOrUpdate(userModel);
            Realm.getDefaultInstance().commitTransaction();

        }
        progressDialog.dismiss();
        //Finish

    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {

        progressDialog.dismiss();

    }


}
