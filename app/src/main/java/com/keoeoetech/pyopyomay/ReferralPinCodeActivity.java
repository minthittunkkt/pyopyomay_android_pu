package com.keoeoetech.pyopyomay;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;

import butterknife.BindView;

public class ReferralPinCodeActivity extends BaseActivity {

    public static final String TAG = "PinLockView";
    @BindView(R.id.indicator_dots)
    IndicatorDots indicator_dots;
    @BindView(R.id.pin_lock_view)
    PinLockView pin_lock_view;

    @Override
    protected int getLayoutResource() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_refferal_pin_code;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {
        init();

    }

    private void init() {

        setupToolbar(false);
        getSupportActionBar().hide();

        pin_lock_view.attachIndicatorDots(indicator_dots);
        pin_lock_view.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                Log.d(TAG, "Pin complete: " + pin);
                if (PyoPyoMayConstant.REFERRAL_PINCODE.equals(pin)) {
                    Intent intent = new Intent(getApplicationContext(), ReferralActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onEmpty() {
                Log.d(TAG, "Pin empty");
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                Log.d(TAG, "Pin changed, new length " + pinLength +
                        " with intermediate pin " + intermediatePin);
            }
        });

        pin_lock_view.setPinLength(4);
        pin_lock_view.setTextColor(ContextCompat.getColor(this, R.color.colorBlack));

        indicator_dots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
    }
}