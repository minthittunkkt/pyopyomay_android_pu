package com.keoeoetech.pyopyomay.custom_control;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import org.sufficientlysecure.htmltextview.HtmlTextView;

/*Created by hello on 1/25/17.*/

public class MyanHtmlTextView extends HtmlTextView {

    public MyanHtmlTextView(Context context) {
        super(context);
        setText(MyanTextProcessor.processText(getContext(), getText().toString()));
    }

    public MyanHtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText(MyanTextProcessor.processText(getContext(), getText().toString()));
    }

    public MyanHtmlTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setText(MyanTextProcessor.processText(getContext(), getText().toString()));
    }

    public void setMyanmarText(String text) {
        setText(MyanTextProcessor.processText(getContext(), text));
    }

    public void setMyanmarHtmlText(String text){
        setHtml(MyanTextProcessor.processText(getContext(), text));
    }

    public String setNullOrEmpty(String text)
    {
        if(text != null)
        {
            if(!text.equals(""))
            {
                return text;
            }
            else
            {
                return "-";
            }
        }
        else
        {
            return "-";
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}