package com.keoeoetech.pyopyomay.custom_control;

import android.content.Context;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.helper.Rabbit;


/* Created by hello on 3/12/18.*/

public class MyanTextProcessor {

    public static String processText(Context context, String original_text) {

        switch (((PyoPyoMay) context.getApplicationContext()).getSelectedFont()) {
            case "zg":
                original_text = Rabbit.uni2zg(original_text);
                break;
            default:
                break;

        }

        return original_text;
    }

}
