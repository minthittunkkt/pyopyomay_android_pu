package com.keoeoetech.pyopyomay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.keoeoetech.pyopyomay.adapter.ViewPagerAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.fragment.FragmentDashboard;
import com.keoeoetech.pyopyomay.fragment.FragmentNewsFeed;
import com.keoeoetech.pyopyomay.fragment.FragmentNotification;
import com.keoeoetech.pyopyomay.fragment.FragmentSetting;
import com.keoeoetech.pyopyomay.helper.CustomDialogHelper;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PeriodAlarmHelper;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.NotificationModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import butterknife.BindView;
import io.realm.Realm;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_BADGE_BROADCAST_INTENT;

public class MainActivity extends BaseActivity {


    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottom_navigation;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    MenuItem prevMenuItem;
    FragmentNewsFeed fragmentNewsFeed;
    FragmentNotification fragmentDiscover;
    FragmentSetting fragmentSetting;
    FragmentDashboard fragmentDashboard;
    Realm realm;
    long count;
    private SharePreferenceHelper sharePreferenceHelper;
    private CustomDialogHelper customDialogHelper;
    private View notificationBadge;
    MyDateFormat myDateFormat;
    PeriodAlarmHelper periodAlarmHelper;


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            /*String msg_from_intent = intent.getStringExtra(NOTI_BADGE_BROADCAST_MESSAGE);
            Toast.makeText(getApplicationContext(), msg_from_intent, Toast.LENGTH_SHORT).show();*/
            refreshBadgeView(); }
    };

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        if (getIntent().getStringExtra("noti") != null) {

            Intent periodIntent = new Intent(this,
                    PeriodTrackerActivity.class);
            periodIntent.putExtra("activity", "main");
            startActivity(periodIntent);
        }

        init();
        addBadgeView();
        refreshBadgeView();

    }

    private void init() {

        setupToolbar(false);
        getSupportActionBar().hide();

        PyoPyoMay.getInstance().trackScreenView
                (PyoPyoMayConstant.MAIN_ACTIVITY);


        sharePreferenceHelper = new SharePreferenceHelper(this);
        customDialogHelper = new CustomDialogHelper(this);

        realm = Realm.getDefaultInstance();
        myDateFormat = new MyDateFormat();
        periodAlarmHelper = new PeriodAlarmHelper(this);


        bottomNavigationIconSize();

        bottom_navigation.setItemIconTintList(null);
        bottom_navigation.setOnNavigationItemSelectedListener
                (item -> {
                    switch (item.getItemId()) {

                        case R.id.action_newsfeed:
                            viewpager.setCurrentItem(0);
                            return true;
                        case R.id.action_favorite:
                            viewpager.setCurrentItem(1);
                            return true;
                        case R.id.action_discover:
                            viewpager.setCurrentItem(2);
                            return true;
                        case R.id.action_setting:
                            viewpager.setCurrentItem(3);
                            return true;
                    }

                    return false;
                });


        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageSelected(int position) {

                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottom_navigation.getMenu().getItem(0).setChecked(false);
                }
                bottom_navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottom_navigation.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

        setupViewPager(viewpager);

    }

    private void refreshBadgeView() {

        count = realm.where(NotificationModel.class)
                .equalTo("isSeen", false)
                .count();

        if (count != 0) {
            notificationBadge.setVisibility(View.VISIBLE);
        } else {
            notificationBadge.setVisibility(View.GONE);
        }
    }

    private void addBadgeView() {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView)
                bottom_navigation.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView)
                menuView.getChildAt(2);

        notificationBadge = LayoutInflater.from(this)
                .inflate(R.layout.layout_notification_badge, menuView,
                        false);

        itemView.addView(notificationBadge);
    }

    public void bottomNavigationIconSize() {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottom_navigation.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }
    }

    private boolean isProfileSetupNeed() {
        PyoPyoMayUserModel model = Realm.getDefaultInstance().
                where(PyoPyoMayUserModel.class).equalTo("MemberId",
                sharePreferenceHelper.getLoginMemberId()).findFirst();


        if (model.getMemberName() != null) {
            return model.getMemberName().isEmpty() || model.getDOB().isEmpty()
                    || model.getUserType().isEmpty();
        } else {
            return true;
        }

    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragmentNewsFeed = new FragmentNewsFeed();
        fragmentDashboard = new FragmentDashboard();
        fragmentDiscover = new FragmentNotification();
        fragmentSetting = new FragmentSetting();
        adapter.addFragment(fragmentNewsFeed);
        adapter.addFragment(fragmentDashboard);
        adapter.addFragment(fragmentDiscover);
        adapter.addFragment(fragmentSetting);
        viewPager.setAdapter(adapter);

        viewPager.setOffscreenPageLimit(3);
    }

    public void onBackPressed() {
        int currentId = viewpager.getCurrentItem();

        switch (currentId) {
            case 0:
                finish();
                break;
            case 1:
                viewpager.setCurrentItem(0);
                break;
            case 2:
                viewpager.setCurrentItem(0);
                break;
            case 3:
                viewpager.setCurrentItem(0);
                break;

        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        IntentFilter intentFilter = new IntentFilter(NOTI_BADGE_BROADCAST_INTENT);
        registerReceiver(mReceiver, intentFilter);

        refreshBadgeView();

        if (isProfileSetupNeed()) {
            customDialogHelper.showDialog(R.mipmap.profile_data_fill,
                    TextDictionaryHelper.getText(this,
                            TextDictionaryHelper.TEXT_PERSONAL_INFO_UPDATE), "အသေးစိတ်ဖြည့်ပါ");

            customDialogHelper.getBtnOk().setOnClickListener(v -> {

                Intent intent = new Intent(MainActivity.this, ProfileUpdateActivity.class);
                startActivity(intent);

            });

        } else {
            customDialogHelper.hideDialog();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mReceiver);

    }



}