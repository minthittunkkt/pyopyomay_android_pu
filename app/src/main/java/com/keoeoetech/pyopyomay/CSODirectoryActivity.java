package com.keoeoetech.pyopyomay;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.keoeoetech.pyopyomay.adapter.CSOAdapter;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.common.EndlessRecyclerViewScrollListener;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.CheckScreenSizeHelper;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.OrganizationCustomModel;
import com.keoeoetech.pyopyomay.model.OrganizationModel;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CSODirectoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;
    @BindView(R.id.organization_recycler)
    ShimmerRecyclerView organization_recycler;
    int page = 1;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    private ServiceHelper.ApiService service;
    private Call<OrganizationCustomModel> organizationCustomModelCall;
    private CSOAdapter csoAdapter;
    private boolean isLoading;
    private boolean isEnd;
    private int pageSize=10;
    private String category = "";


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_csodirectory;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        setupToolbar(true);

        category =getIntent().getStringExtra("category");
        Log.d("Category Name", category);

        init();
        getOrganizationData();


    }

    private void init() {

        swipe_refresh_layout.setOnRefreshListener(this);
        service = ServiceHelper.getClient(this);
        swipe_refresh_layout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        organization_recycler.setHasFixedSize(true);

        tv_empty.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_NO_DATA_AVAILABLE));

        if (CheckScreenSizeHelper.isTablet(this)) {
            organization_recycler.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            organization_recycler.setLayoutManager(new GridLayoutManager(this, 1));
        }

        csoAdapter = new CSOAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        organization_recycler.setLayoutManager(linearLayoutManager);
        organization_recycler.setAdapter(csoAdapter);
        organization_recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager) organization_recycler.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!isLoading && !isEnd) {
                    getOrganizationData();
                } else if (isEnd) {
                    //showToastMsg(getString(R.string.string_no_more_data));
                }

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (organizationCustomModelCall != null) {
            organizationCustomModelCall.cancel();
        }
    }


    private void getOrganizationData() {
        if (page == 1) {
            organization_recycler.showShimmerAdapter();
        }

        isLoading = true;
        swipe_refresh_layout.setRefreshing(false);
        csoAdapter.clearFooter();
        organization_recycler.post(() -> {
            csoAdapter.showLoading();
            emptyView.setVisibility(View.GONE);
        });

        organizationCustomModelCall = service.getOrganizationData(page, pageSize,category);
        organizationCustomModelCall.enqueue(new Callback<OrganizationCustomModel>() {
            @Override
            public void onResponse(Call<OrganizationCustomModel> call, Response<OrganizationCustomModel> response) {
                if (page == 1) {
                    organization_recycler.hideShimmerAdapter();
                }

                isLoading = false;
                csoAdapter.clearFooter();
                if (response.isSuccessful()) {
                    OrganizationCustomModel result = response.body();
                    ArrayList<OrganizationModel> resultList = result.getOrganizationModelArrayList();

                    if (page == 1 && resultList.isEmpty()) {
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        emptyView.setVisibility(View.GONE);
                    }

                    isEnd = result.getOrganizationModelArrayList().isEmpty();
                    for (int i = 0; i < resultList.size(); i++) {
                        csoAdapter.add(resultList.get(i));
                    }
                    page++;
                } else {
                    handleFailure();
                }

            }

            @Override
            public void onFailure(Call<OrganizationCustomModel> call, Throwable t) {

                handleFailure();
            }
        });

    }

    private void handleFailure() {

        csoAdapter.clearFooter();
        csoAdapter.showRetry(R.layout.recycler_footer_retry, R.id.btn_retry, () -> getOrganizationData());
    }

    @Override
    public void onRefresh() {
        page = 1;
        category = getIntent().getStringExtra("category");
        csoAdapter.clear();
        getOrganizationData();
        csoAdapter.notifyDataSetChanged();
    }

}