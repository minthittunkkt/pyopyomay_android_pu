package com.keoeoetech.pyopyomay.helper;

import android.content.Context;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.model.TextModel;


import java.util.ArrayList;

public class TextDictionaryHelper {

    public static final String TEXT_PROFILE = "TEXT_PROFILE";
    public static final String TEXT_HEALTH_PROFILE = "TEXT_HEALTH_PROFILE";
    public static final String TEXT_LANGUAGE = "TEXT_LANGUAGE";
    public static final String TEXT_TERMS_OF_SERVICES = "TEXT_TERMS_OF_SERVICES";
    public static final String TEXT_PRIVACY_POLICY = "TEXT_PRIVACY_POLICY";
    public static final String TEXT_HELP_AND_SUPPORT = "TEXT_HELP_AND_SUPPORT";
    public static final String TEXT_SIGN_OUT = "TEXT_SIGN_OUT";
    public static final String TEXT_START_LESSON="TEXT_START_LESSON";
    public static final String TEXT_VERSION="TEXT_VERSION";
    public static final String TEXT_HEALTH_CARE="TEXT_HEALTH_CARE";
    public static final String TEXT_DIY="TEXT_DIY";
    public static final String TEXT_SKILL="TEXT_SKILL";
    public static final String TEXT_FASHION_AND_BEAUTY="TEXT_FASHION_AND_BEAUTY";
    public static final String TEXT_MOTIVATION_AND_STORY = "TEXT_MOTIVATION_AND_STORY";
    public static final String TEXT_SOCIAL_WELL_BEING="TEXT_SOCIAL_WELL_BEING";
    public static final String TEXT_BTN_QUIZZ="TEXT_BTN_QUIZZ";
    public static final String TEXT_ANSWER_QUIZZ="TEXT_ANSWER_QUIZZ";
    public static final String TEXT_BUTTON_ANSWER="TEXT_BUTTON_ANSWER";
    public static final String TEXT_RESULT_WRONG="TEXT_RESULT_WRONG";
    public static final String TEXT_RESULT_CORRECT="TEXT_RESULT_CORRECT";
    public static final String TEXT_BUTTON_FINISH="TEXT_BUTTON_FINISH";
    public static final String TEXT_TOTAL_MARKS="TEXT_TOTAL_MARKS";
    public static final String TEXT_POINTS="TEXT_POINTS";
    public static final String TEXT_LABEL_HINT="TEXT_LABEL_HINT";
    public static final String TEXT_NO_DATA_AVAILABLE = "TEXT_NO_DATA_AVAILABLE";
    public static final String TEXT_BUTTON_REMOVE="TEXT_BUTTON_REMOVE";
    public static final String TEXT_DIALOG_TITLE="TEXT_DIALOG_TITLE";
    public static final String TEXT_BTN_OK="TEXT_BTN_OK";
    public static final String TEXT_BTN_CANCEL="TEXT_BTN_CANCEL";
    public static final String TEXT_BUTTON_DELETE="TEXT_BUTTON_DELETE";
    public static final String TEXT_SAVE_POST="TEXT_SAVE_POST_DATA";
    public static final String TEXT_EMPTY_DATA="TEXT_EMPTY_DATA";
    public static final String TEXT_USER_NAME="TEXT_USER_NAME";
    public static final String TEXT_USER_BIRTHDATE="TEXT_USER_BIRTHDATE";
    public static final String TEXT_BTN_UPDATE="TEXT_BTN_UPDATE";
    public static final String TEXT_WOMAN_HEALTH="TEXT_WOMAN_HEALTH";
    public static final String TEXT_BEST_TIPS_OF_TODAY="TEXT_BEST_TIPS_OF_TODAY";
    public static final String TEXT_LAST_DAY_OF_PERIOD="TEXT_LAST_DAY_OF_PERIOD";
    public static final String TEXT_PERIOD_DURATION="TEXT_PERIOD_DURATION";
    public static final String TEXT_PERIOD_SUGGESTION_DATE="TEXT_PERIOD_SUGGESTION_DATE";
    public static final String TEXT_OVULATION_DATE="TEXT_OVULATION_DATE";
    public static final String TEXT_CALENDAR="TEXT_CALENDAR";
    public static final String TEXT_MESSENGER="TEXT_MESSENGER";
    public static final String TEXT_PERIOD_CYCLE="TEXT_PERIOD_CYCLE";
    public static final String TEXT_BTN_WOMAN_HEALTH_UPDATE="TEXT_BTN_WOMAN_HEALTH_UPDATE";
    public static final String TEXT_BTN_SHOW_WOMAN_DATA="TEXT_BTN_SHOW_WOMAN_DATA";
    public static final String TEXT_PERIOD_DATA_FILL="TEXT_PERIOD_DATA_FILL";
   public static final String TEXT_PROFILE_SETUP="TEXT_PROFILE_SETUP";
    public static final String TEXT_AGE="TEXT_AGE";
    public static final String TEXT_ENTER_NAME="TEXT_ENTER_NAME";
    public static final String TEXT_ENTER_BIRTHDAY="TEXT_ENTER_BIRTHDAY";
    public static final String TEXT_BTN_ANSWER_QUIZZ="TEXT_BTN_ANSWER_QUIZZ";
    public static final String TEXT_PERIOD_CYCLE_TIP="TEXT_PERIOD_CYCLE_TIP";
    public static final String TEXT_PERIOD_DURATION_TIP="TEXT_PERIOD_DURATION_TIP";
    public static final String TEXT_PERSONAL_INFO_UPDATE="TEXT_PERSONAL_INFO_UPDATE";
    public static final String TEXT_BTN_PERIOD_START="TEXT_BTN_PERIOD_START";
    public static final String TEXT_LOGOUT_MESSAGE="TEXT_LOGOUT_MESSAGE";
    public static final String TEXT_BTN_LOGOUT_OK="TEXT_BTN_LOGOUT_OK";
    public static final String TEXT_BTN_LOGOUT_CANCEL="TEXT_BTN_LOGOUT_CANCEL";
    public static final String TEXT_SAVE_MESSAGE="TEXT_SAVE_MESSAGE";
    public static final String TEXT_DELETE_MESSAGE="TEXT_DELETE_MESSAGE";
    public static final String TEXT_LET_ANSWER="TEXT_LET_ANSWER";
    public static final String TEXT_PYOPYOMAY_ANSWER_QUIZZ="TEXT_PYOPYOMAY_ANSWER_QUIZZ";




    public static ArrayList<TextModel> list = new ArrayList<TextModel>() {{

        add(new TextModel(TEXT_PROFILE,"Profile","ကိုယ်ရေးအချက်အလက်"));
        add(new TextModel(TEXT_HEALTH_PROFILE,"Health Profile","ကျန်းမာရေးအချက်အလက်"));
        add(new TextModel(TEXT_LANGUAGE,"Language","ဘာသာစကားရွေးချယ်ရန်"));
        add(new TextModel(TEXT_PRIVACY_POLICY,"Privacy Policy","ကိုယ်ရေးအချက်အလက်မူဝါဒ"));
        add(new TextModel(TEXT_TERMS_OF_SERVICES,"Terms of Services","စည်းမျဉ်းစည်းကမ်းများ"));
        add(new TextModel(TEXT_HELP_AND_SUPPORT,"Help and Support","ပျိုပျိုမေ facebook page messenger"));
        add(new TextModel(TEXT_SIGN_OUT,"Sign Out", "ထွက်ခွာရန်"));
        add(new TextModel(TEXT_START_LESSON,"Start Lesson","သင်ခန်းစာများကိုစတင်ပါမည်"));
        add(new TextModel(TEXT_VERSION, "Version", "ဗားရှင်း"));
        add(new TextModel(TEXT_HEALTH_CARE,"Health Care", "ကျန်းမာရေးအကြောင်းအရာ"));
        add(new TextModel(TEXT_DIY,"DIY","ဖန်တီးမှုပညာ"));
        add(new TextModel(TEXT_SKILL,"Skill","လေ့လာပါ၊ မှတ်သားပါ"));
        add(new TextModel(TEXT_FASHION_AND_BEAUTY,"Fashion and Beauty","ဖက်ရှင်နှင့် အလှအပ"));
        add(new TextModel(TEXT_MOTIVATION_AND_STORY,"Motivation and Story","စိတ်ခွန်အားဖြည့်စာများ"));
        add(new TextModel(TEXT_SOCIAL_WELL_BEING,"Social Well Being","လူနေမှုဘဝ"));
        add(new TextModel(TEXT_ANSWER_QUIZZ,"Let Answer Quizz","ဉာဏ်စမ်းမေးခွန်းများ"));
        add(new TextModel(TEXT_BTN_QUIZZ,"Answer Quizz","၀င်မည်"));
        add(new TextModel(TEXT_BUTTON_ANSWER,"Answer","ဖြေဆိုပါမည်"));
        add(new TextModel(TEXT_RESULT_WRONG,"Wrong answer!","အဖြေမှားပါသည်။ ပြန်လည်ဖြေဆိုပါ။"));
        add(new TextModel(TEXT_RESULT_CORRECT,"Correct answer!","အဖြေမှန်ပါသည်။"));
        add(new TextModel(TEXT_BUTTON_FINISH,"Finish","ပြီးပါပြီ"));
        add(new TextModel(TEXT_TOTAL_MARKS,"Total Mask : ","စုစုပေါင်းရမှတ် "));
        add(new TextModel(TEXT_POINTS," Points"," မှတ်"));
        add(new TextModel(TEXT_LABEL_HINT,"hint:","ရှင်းလင်းချက်"));
        add(new TextModel(TEXT_NO_DATA_AVAILABLE,"No Data Available!","ဒေတာမရှိသေးပါ"));
        add(new TextModel(TEXT_BUTTON_REMOVE,"Remove","ဖျက်မည်"));
        add(new TextModel(TEXT_DIALOG_TITLE,"My Saved Articles","သေချာပါသလား"));
        add(new TextModel(TEXT_SAVE_POST,"Save Post","သိမ်းဆည်းထားသည်များ"));
        add(new TextModel(TEXT_BUTTON_DELETE,"Delete","ဖျက်မည်"));
        add(new TextModel(TEXT_EMPTY_DATA,"Empty Saved Data","သိမ်းဆည်းထားသည်များမရှိပါ"));
        add(new TextModel(TEXT_USER_NAME,"Name",""));
        add(new TextModel(TEXT_USER_BIRTHDATE, "Birthday",""));
        add(new TextModel(TEXT_BTN_UPDATE,"Update","ပရိုဖိုင်း ပြုလုပ်မည်"));
        add(new TextModel(TEXT_WOMAN_HEALTH,"Woman Health","အမျိုးသမီးကျန်းမာရေး"));
        add(new TextModel(TEXT_BEST_TIPS_OF_TODAY,"Best Tips of Today","ယနေ့အတွက်အကောင်းဆုံးဆောင်းပါးများ"));
        add(new TextModel(TEXT_LAST_DAY_OF_PERIOD,"Last day of Period","နောက်ဆုံးရာသီ၏ ပထမဆုံးရက်"));
        add(new TextModel(TEXT_OVULATION_DATE,"Ovulation Day","မမျိုးဥ ကြွေမည့်ရက်"));
        add(new TextModel(TEXT_PERIOD_SUGGESTION_DATE,"Period Suggestion Day","နောက်တစ်ကြိမ်ရာသီလာမည့်ရက်"));
        add(new TextModel(TEXT_PERIOD_DURATION,"Period Duration","ရာသီရက်ကြာချိန်"));
        add(new TextModel(TEXT_CALENDAR,"Calendar","ပြက္ခဒိန်"));
        add(new TextModel(TEXT_MESSENGER,"Please install Messenger App to use this feature.","Messenger ရွိမွသုံးလို႔ရပါမည။္"));
        add(new TextModel(TEXT_PERIOD_CYCLE,"Period Cycle", "ရာသီစက်ဝန်းကြာချိန်"));
        add(new TextModel(TEXT_BTN_WOMAN_HEALTH_UPDATE,"Set up your data","အသေးစိတ်ဖြည့်မည်"));
        add(new TextModel(TEXT_BTN_SHOW_WOMAN_DATA,"Show your data","အချက်အလက်ပြမည်"));
        add(new TextModel(TEXT_PERIOD_DATA_FILL,"Please fill","အသေးစိတ်ဖြည့်ပါ"));
        add(new TextModel(TEXT_PROFILE_SETUP,"Please fill your information.","ကိုယ်ရေးအချက်အလက် ဖြည့်ပါ"));
        add(new TextModel(TEXT_AGE,"Age must be 12 years old.","အသက် ၁၂ နှစ်အထက်များသာအသုံးပြုနိုင်သည်"));
        add(new TextModel(TEXT_ENTER_NAME,"Enter name","သင်၏နာမည်ထည့်ပါ"));
        add(new TextModel(TEXT_ENTER_BIRTHDAY,"Enter Birthday","သင်၏မွေးနေ့ထည့်ပါ"));
        add(new TextModel(TEXT_BTN_ANSWER_QUIZZ,"Answer Quizz","၀င်မည်"));
        add(new TextModel(TEXT_PERIOD_CYCLE_TIP,"Period Cycle Tip",
                "ယေဘုယျအားဖြင့် ရာသီစက်ဝန်းကြာချိန်မှာ ၂၁ ရက် မှ ၃၅ ရက်အတွင်းဖြစ်နိုင်ပါသည်။"));
        add(new TextModel(TEXT_PERIOD_DURATION_TIP,"Period Duration Tip",
                "ယေဘုယျအားဖြင့် ရာသီရက်ကြာချိန်မှာ ၂ ရက် မှ ၇ ရက်အထိဖြစ်နိုင်ပါသည်။"));
        add(new TextModel(TEXT_PERSONAL_INFO_UPDATE,"Please fill personal information","ကိုယ်ရေးအချက်အလက်ပြန်လည်ပြင်ဆင်ပါ။"));
        add(new TextModel(TEXT_BTN_PERIOD_START,"Period Start","ရာသီစက်ဝန်းစတင်မည်"));
        add(new TextModel(TEXT_LOGOUT_MESSAGE,"Logout","သေချာပါသလား"));
        add(new TextModel(TEXT_BTN_LOGOUT_OK,"OK","ထွက်မည်"));
        add(new TextModel(TEXT_BTN_LOGOUT_CANCEL,"CANCEL","မထွက်ပါ"));
        add(new TextModel(TEXT_BTN_OK,"OK","လုပ်ဆောင်မည်"));
        add(new TextModel(TEXT_BTN_CANCEL,"CANCEL","မလုပ်ဆောင်ပါ"));
        add(new TextModel(TEXT_SAVE_MESSAGE,"Saved Article","ဖတ်ရန်သိမ်းဆည်းမည်"));
        add(new TextModel(TEXT_DELETE_MESSAGE,"Delete Article","သိမ်းဆည်းထားသည်ကိုဖျက်ပါမည်"));
        add(new TextModel(TEXT_LET_ANSWER,"","ဖြေကြည့်ရအောင်"));
        add(new TextModel(TEXT_PYOPYOMAY_ANSWER_QUIZZ,"Quizz for PyoPyoMay","ပျိုပျိုမေများအတွက် သင်ခန်းစာများကို ဖြေဆိုကြရအောင်"));

    }};

    public static String getText(Context context, String key) {
        String result = "";
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getKey().equals(key)) {
                TextModel model = list.get(i);

                switch (((PyoPyoMay) context.getApplicationContext()).getSelectedLanguage()) {
                    case PyoPyoMayConstant.ENGLISH_LANGUAGE:
                        result = model.getEnglish();
                        break;
                    default:
                        result = model.getMyanmar();
                        break;
                }

            }
        }

        return result;
    }

}
