package com.keoeoetech.pyopyomay.helper;

import android.content.Context;
import android.content.res.Configuration;

/*
 Created by hello on 9/8/16.
 */
public class CheckScreenSizeHelper {

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

}
