package com.keoeoetech.pyopyomay.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.keoeoetech.pyopyomay.model.AnswersModel;
import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.keoeoetech.pyopyomay.model.LessonModel;
import com.keoeoetech.pyopyomay.model.NotificationModel;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;
import com.keoeoetech.pyopyomay.model.QuestionModel;

import io.realm.Realm;

import static com.facebook.FacebookSdk.getApplicationContext;


public class SharePreferenceHelper {

    private static final String PHOTO_NAME_TEMP_KEY = "photo_name";
    private static final String PHOTO_PATH_TEMP_KEY = "photo_path";
    private static String MEMBER_PHONE_NO_KEY = "member_phone_no";
    private static String MEMBER_ID_KEY = "member_id";
    private static String REFERAL_QRCODE_SHOWN="isReferralQRCodeShown";

    private SharedPreferences sharedPreference;


    public SharePreferenceHelper(Context context) {
        String SHARE_PREFRENCE = "pyopyomayPref";
        sharedPreference = context.getSharedPreferences(SHARE_PREFRENCE,
                Context.MODE_PRIVATE);

    }


    //for Login

    public void setLogin(String phone, int memberid) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(MEMBER_PHONE_NO_KEY, phone);
        editor.putInt(MEMBER_ID_KEY, memberid);
        editor.apply();
    }

    public boolean isLogin()
    {
        if(sharedPreference.contains(MEMBER_ID_KEY))
            return true;
        else
        {
            return false;
        }

    }

    public int getLoginMemberId() {
        return sharedPreference.getInt(MEMBER_ID_KEY, 0);
    }

    public String getPhoneNo()
    {
        return sharedPreference.getString(MEMBER_PHONE_NO_KEY,"");
    }

    public boolean isPhoneNoSetUp() {
        if (sharedPreference.contains(MEMBER_PHONE_NO_KEY) && sharedPreference.contains(MEMBER_ID_KEY)) {
            return true;
        } else {
            return false;
        }
    }

    public void logout()
    {
        SharedPreferences.Editor editor=sharedPreference.edit();
        editor.clear();
        editor.apply();

        Realm.getDefaultInstance().executeTransaction(realm -> {

            realm.delete(PyoPyoMayUserModel.class);
            realm.delete(PeriodTrackerModel.class);
            realm.delete(ArticleModel.class);
            realm.delete(NotificationModel.class);
            realm.delete(LessonModel.class);
            realm.delete(QuestionModel.class);
            realm.delete(AnswersModel.class);

        });

        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();

    }
    public void setTempPhotoName(String photoName) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(PHOTO_NAME_TEMP_KEY, photoName);
        editor.apply();
    }

    public void setTempPhotoPath(String photoPath) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(PHOTO_PATH_TEMP_KEY, photoPath);
        editor.apply();
    }



    public String getTempPhotoName()
    {
        return sharedPreference.getString(PHOTO_NAME_TEMP_KEY, "");
    }
    public String getTempPhotoPath() {
        return sharedPreference.getString(PHOTO_PATH_TEMP_KEY, "");
    }

    public boolean isReferralQRCodeShown() {
        return sharedPreference.getBoolean(REFERAL_QRCODE_SHOWN, false);
    }

    public void setReferralQRCodeShown(boolean flag) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putBoolean(REFERAL_QRCODE_SHOWN, flag);
        editor.apply();
    }


}