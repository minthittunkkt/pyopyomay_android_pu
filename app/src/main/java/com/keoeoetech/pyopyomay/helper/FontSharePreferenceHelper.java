package com.keoeoetech.pyopyomay.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.keoeoetech.pyopyomay.model.ArticleModel;
import com.keoeoetech.pyopyomay.model.PeriodTrackerModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import io.realm.Realm;

public class FontSharePreferenceHelper {

    private static String FONT_KEY = "font_name";
    private static String LANGUAGE_KEY = "language_name";

    private SharedPreferences sharedPreference;


    public FontSharePreferenceHelper(Context context) {
        String SHARE_PREFRENCE = "pyopyomayfontPref";
        sharedPreference = context.getSharedPreferences(SHARE_PREFRENCE, Context.MODE_PRIVATE);

    }

    //for Language
    public boolean isLanguageFontSetup() {
        if (sharedPreference.contains(FONT_KEY) && sharedPreference.contains(LANGUAGE_KEY)) {
            return true;
        } else {
            return false;
        }
    }


    public String getFont() {
        return sharedPreference.getString(FONT_KEY, "");
    }

    public void setFont(String language) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(FONT_KEY, language);
        editor.apply();
    }

    public String getLanguage() {

        return sharedPreference.getString(LANGUAGE_KEY, "");
    }

    public void setLanguage(String language) {
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(LANGUAGE_KEY, language);
        editor.apply();
    }



}
