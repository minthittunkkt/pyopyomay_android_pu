package com.keoeoetech.pyopyomay.helper;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.keoeoetech.pyopyomay.receiver.AlarmReceiver;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class PeriodAlarmHelper {

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private MyDateFormat myDateFormat;

    public PeriodAlarmHelper(Context context) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(context, 1111, intent, 0);
        myDateFormat = new MyDateFormat();
    }

    @SuppressLint("NewApi")
    public void startAlarm(Date periodDate) {
        Calendar periodCalendar = Calendar.getInstance();
        periodCalendar.setTime(periodDate);

        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.set(Calendar.DAY_OF_MONTH, periodCalendar.get(Calendar.DAY_OF_MONTH));
        alarmCalendar.set(Calendar.MONTH, periodCalendar.get(Calendar.MONTH));
        alarmCalendar.set(Calendar.YEAR, periodCalendar.get(Calendar.YEAR));
        alarmCalendar.add(Calendar.DAY_OF_MONTH, -1);

        Log.d("PeriodDateinAlarmHelper", myDateFormat.DATE_FORMAT_YMD_HMS.format(alarmCalendar.getTime()));

        alarmMgr.setExact(AlarmManager.RTC_WAKEUP,
                alarmCalendar.getTimeInMillis(), alarmIntent);
    }

    public void stopAlarm() {
        alarmMgr.cancel(alarmIntent);
    }

}
