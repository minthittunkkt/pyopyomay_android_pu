package com.keoeoetech.pyopyomay.helper;

import android.content.Context;


import com.keoeoetech.pyopyomay.model.NewsFeedCustomModel;
import com.keoeoetech.pyopyomay.model.OrganizationCustomModel;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;
import com.keoeoetech.pyopyomay.model.ReferalResponseModel;
import com.keoeoetech.pyopyomay.model.ReferralModel;

import java.io.File;
import java.io.IOException;

import java.util.Iterator;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.BASE_URL;


/**
 * Created by minthittun on 12/30/2015.
 */
public class ServiceHelper {


    private static ApiService apiService;
    private static Cache cache;

    public static ApiService getClient(final Context context) {
        if (apiService == null) {

            Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    int maxAge = 300; // read from cache for 5 minute
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, max-age="
                                    + maxAge)
                            .build();
                }
            };

            //setup cache
            File httpCacheDirectory = new File(context.getCacheDir(), "responses");
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            cache = new Cache(httpCacheDirectory, cacheSize);

            final OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
            okClientBuilder.addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR);
            okClientBuilder.cache(cache);

            OkHttpClient okClient = okClientBuilder.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService = client.create(ApiService.class);
        }
        return apiService;
    }

    public static void removeFromCache(String url) {
        try {
            Iterator<String> it = cache.urls();
            while (it.hasNext()) {
                String next = it.next();
                if (next.contains(BASE_URL + url)) {
                    it.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public interface ApiService {

        @GET("newsfeed/pyopyomaysocialfeed")
        Call<NewsFeedCustomModel> getAllNewFeedData(@Query("userphoneno") String userphoneno,
                                                    @Query("page") int page,
                                                    @Query("pageSize") int pageSize,
                                                    @Query("app") String app);

        @GET("newsfeed/getByCategoryName")
        Call<NewsFeedCustomModel> getNewsFeedByCategory(@Query("categoryName") String categoryName,
                                                    @Query("page") int page,
                                                    @Query("pageSize") int pageSize);
        @GET("member/login")
        Call<PyoPyoMayUserModel> getByPhone(@Query("phoneNumber") String phoneNumber);

        @GET("member/getbymemberId")
        Call<PyoPyoMayUserModel> getByMemberId(@Query("memberid") int memberid);

        @POST("member/update")
        Call<PyoPyoMayUserModel> updateMemberProfile(@Body PyoPyoMayUserModel model);

        @POST("ref/RefQRScancodePyoPyoMay")
        Call<ReferalResponseModel> postReferral(@Body ReferralModel model);

        @GET("member/loginwithfacebook")
        Call<PyoPyoMayUserModel> getByFacebookId(@Query("phoneNumber") String phoneNumber,
                                                 @Query("facebookid") String facebookid);


        @GET("CSONews/getallCSOdata")
        Call<OrganizationCustomModel> getOrganizationData(@Query("page") int page,
                                                          @Query("pageSize") int pageSize,
                                                          @Query("category") String category);

    }
}
