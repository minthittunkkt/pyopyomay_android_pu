package com.keoeoetech.pyopyomay.helper;

public class PyoPyoMayConstant {

    public static final String BASE_URL = "https://newmaymay.azurewebsites.net/api/";
    public static final String OTP_BASE_URL="https://kktsmsverification.azurewebsites.net/api/";
    public static final String PHOTO_URL = "https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/maymay/pyopyomay/";
    public static final String REFERRAL_URL = "https://maymayadmin.azurewebsites.net/Referral/PyoPyoMayReferralDetail?referralpno=";
    public static final String REFERRAL_PINCODE="2457";

    public static final int MAX_WIDTH = 1024;
    public static final int MAX_HEIGHT = 768;

    public static final String ZG_FONT = "zg";
    public static final String MM3_FONT = "mm3";

    public static final String NOTI_TYPE_PERIOD_TRACKER = "period_tracker";
    public static final String NOTI_TYPE_FCM="FCM_Messaging";

    public static final String ENGLISH_LANGUAGE = "english";

    public static final String PHOTO = "Content";
    public static final String VIDEO = "Video";

    public static final String NEWSFEED_FRAGMENT="NewsFeedScreen";
    public static final String FAVORITE_FRAGMENT="FavoriteScreen";
    public static final String SETTING_FRAGMENT="SettingScreen";
    public static final String QUIZZ_ACTIVITY="QuizzScreen";
    public static final String QUESTION_ANSWER_ACTIVITY="QuestionAnswerScreen";
    public static final String PERIOD_ACTIVITY="PeriodDataFillScreen";
    public static final String PERIOD_TRACKER_ACTIVITY="PeriodTrackerScreen";
    public static final String NEWSFEED_DETAIL_ACTIVITY="NewsFeedDetailScreen";
    public static final String LESSON_CLICK="LessonId";
    public static final String NEWSFEED_BY_CATEGORY_ACTIVITY="NewsFeedByCategoryActivity";
    public static final String NEWSFEED_CLICK="NewsFeedClick";
    public static final String FAVORITE_CLICK="FavoriteClick";
    public static final String PROFILE_CLICK="ProfileClick";
    public static final String MESSENGER_CLICK="MessengerClick";
    public static final String LOGOUT_CLICK="LogoutClick";
    public static final String USER_TYPE="UserType";
    public static final String QUESTION_ID="QuestionId";
    public static final String ANSWER_ID="AnswerId";
    public static final String PERIOD_BUTTON_CLICK="PeriodClick";
    public static final String LOGIN_ACTIVITY="LoginScreen";
    public static final String LOGIN_BUTTON_CLICK="LoginButtonClick";
    public static final String NEWSFEED_BY_CATEGORY_CLICK="NewsFeedByCategoryClick";
    public static final String NEWSFEED_BY_CATEGORY_DETAIL_CLICK="NewsFeedByDetailCategoryClick";
    public static final String NEWSFEED_BY_CATEGORY_DETAIL_ACTIVITY="NewsFeedByDetailCategoryActivity";
    public static final String FAVORITE_DELETE_CLICK="FavoriteContentDelete";
    public static final String MAIN_ACTIVITY="Main Screen";
    public static final String PROFILE_SET_UP_ACTIVITY="ProfileSetUpScreen";
    public static final String SPLASH_ACTIVITY="SplashScreen";
    public static final String REFERRAL_ACTIVITY="ReferralScreen";
    public static final String REFERAL_CODE_SCANNER_ACTIVITY="ReferralCodeScannerScreen";

    public static final String NOTI_BADGE_BROADCAST_INTENT = "com.keoeoetech.pyopyomay.NOTI_BADGE_INTENT";
    public static final String NOTI_BADGE_BROADCAST_MESSAGE = "com.keoeoetech.pyopyomay.NOTI_BADGE_MESSAGE";

}
