package com.keoeoetech.pyopyomay.helper;


/*
 Created by minthittun on 1/10/2016.
 */
public class NumConvertHelper {

    public static String getMyanmarNum(String number) {
        char[] charInt = String.valueOf(number).toCharArray();
        String result = "";

        for (char temp : charInt) {
            if (String.valueOf(temp).equals("1")) {
                result += "၁";
            } else if (String.valueOf(temp).equals("2")) {
                result += "၂";
            } else if (String.valueOf(temp).equals("3")) {
                result += "၃";
            } else if (String.valueOf(temp).equals("4")) {
                result += "၄";
            } else if (String.valueOf(temp).equals("5")) {
                result += "၅";
            } else if (String.valueOf(temp).equals("6")) {
                result += "၆";
            } else if (String.valueOf(temp).equals("7")) {
                result += "၇";
            } else if (String.valueOf(temp).equals("8")) {
                result += "၈";
            } else if (String.valueOf(temp).equals("9")) {
                result += "၉";
            } else if (String.valueOf(temp).equals("0")) {
                result += "၀";
            } else {
                result += temp;
            }
        }

        return result;
    }

    public static String getMyanmarNum(int number) {
        char[] charInt = String.valueOf(number).toCharArray();
        String result = "";

        for (char temp : charInt) {
            if (String.valueOf(temp).equals("1")) {
                result += "၁";
            } else if (String.valueOf(temp).equals("2")) {
                result += "၂";
            } else if (String.valueOf(temp).equals("3")) {
                result += "၃";
            } else if (String.valueOf(temp).equals("4")) {
                result += "၄";
            } else if (String.valueOf(temp).equals("5")) {
                result += "၅";
            } else if (String.valueOf(temp).equals("6")) {
                result += "၆";
            } else if (String.valueOf(temp).equals("7")) {
                result += "၇";
            } else if (String.valueOf(temp).equals("8")) {
                result += "၈";
            } else if (String.valueOf(temp).equals("9")) {
                result += "၉";
            } else if (String.valueOf(temp).equals("0")) {
                result += "၀";
            } else {
                result += temp;
            }
        }

        return result;
    }

    public static String getMyanmarNum(float number) {
        char[] charInt = String.valueOf(number).toCharArray();
        String result = "";

        for (char temp : charInt) {
            if (String.valueOf(temp).equals("1")) {
                result += "၁";
            } else if (String.valueOf(temp).equals("2")) {
                result += "၂";
            } else if (String.valueOf(temp).equals("3")) {
                result += "၃";
            } else if (String.valueOf(temp).equals("4")) {
                result += "၄";
            } else if (String.valueOf(temp).equals("5")) {
                result += "၅";
            } else if (String.valueOf(temp).equals("6")) {
                result += "၆";
            } else if (String.valueOf(temp).equals("7")) {
                result += "၇";
            } else if (String.valueOf(temp).equals("8")) {
                result += "၈";
            } else if (String.valueOf(temp).equals("9")) {
                result += "၉";
            } else if (String.valueOf(temp).equals("0")) {
                result += "၀";
            } else if (String.valueOf(temp).equals(".")) {
                result += ".";
            } else {
                result += temp;
            }
        }

        return result;
    }

    public static String getMyanmarNum(double number) {
        char[] charInt = String.valueOf(number).toCharArray();
        String result = "";

        for (char temp : charInt) {
            if (String.valueOf(temp).equals("1")) {
                result += "၁";
            } else if (String.valueOf(temp).equals("2")) {
                result += "၂";
            } else if (String.valueOf(temp).equals("3")) {
                result += "၃";
            } else if (String.valueOf(temp).equals("4")) {
                result += "၄";
            } else if (String.valueOf(temp).equals("5")) {
                result += "၅";
            } else if (String.valueOf(temp).equals("6")) {
                result += "၆";
            } else if (String.valueOf(temp).equals("7")) {
                result += "၇";
            } else if (String.valueOf(temp).equals("8")) {
                result += "၈";
            } else if (String.valueOf(temp).equals("9")) {
                result += "၉";
            } else if (String.valueOf(temp).equals("0")) {
                result += "၀";
            } else if (String.valueOf(temp).equals(".")) {
                result += ".";
            } else {
                result += temp;
            }
        }

        return result;
    }

    public static String getEngDate(String date) {
        char[] charInt = date.toCharArray();
        String result = "";

        for (char temp : charInt) {
            if (String.valueOf(temp).equals("၁")) {
                result += "1";
            } else if (String.valueOf(temp).equals("၂")) {
                result += "2";
            } else if (String.valueOf(temp).equals("၃")) {
                result += "3";
            } else if (String.valueOf(temp).equals("၄")) {
                result += "4";
            } else if (String.valueOf(temp).equals("၅")) {
                result += "5";
            } else if (String.valueOf(temp).equals("၆")) {
                result += "6";
            } else if (String.valueOf(temp).equals("၇")) {
                result += "7";
            } else if (String.valueOf(temp).equals("၈")) {
                result += "8";
            } else if (String.valueOf(temp).equals("၉")) {
                result += "9";
            } else if (String.valueOf(temp).equals("၀")) {
                result += "0";
            } else if (String.valueOf(temp).equals("-")) {
                result += "-";
            } else if (String.valueOf(temp).equals(" ")) {
                result += " ";
            } else if (String.valueOf(temp).equals(":")) {
                result += ":";
            } else if (String.valueOf(temp).equals("T")) {
                result += "T";
            } else if (String.valueOf(temp).equals("0")) {
                result += "0";
            } else if (String.valueOf(temp).equals("-")) {
                result += "-";
            } else if (String.valueOf(temp).equals("_")) {
                result += "_";
            }
        }

        return result;
    }

}
