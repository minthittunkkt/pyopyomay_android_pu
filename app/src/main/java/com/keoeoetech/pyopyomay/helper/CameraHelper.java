package com.keoeoetech.pyopyomay.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import com.keoeoetech.pyopyomay.interfaces.CameraCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by minthit on 3/17/2015.
 * Removed warning code by AAM on 29-Jun-17
 */
public class CameraHelper {

    //private static String tempPhotoFileName;

    public static String TEMP_FOLDER_NAME = "PyoPyoMayAppTemp";
    public static String FOLDER_NAME = "PyoPyoMayApp";
    private static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();
    private static SharePreferenceHelper sharePreferenceHelper;
    private CameraCallback cameraCallback;


    public CameraHelper(Context context) {
        sharePreferenceHelper = new SharePreferenceHelper(context);
    }

    private static boolean isPureAscii(String v) {
        return asciiEncoder.canEncode(v);
    }

    public static String getDateTImeStampForFileName() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

        String result = simpleDateFormat.format(Calendar.getInstance().getTime());

        if (isPureAscii(result)) {
            return result;
        } else {
            return NumConvertHelper.getEngDate(result);
        }

    }

    public static void deleteFile(String selectedFilePath) {
        File file = new File(selectedFilePath);
        boolean deleted = file.delete();

        if (deleted) {
            sharePreferenceHelper.setTempPhotoName("");
            sharePreferenceHelper.setTempPhotoPath("");
        }

    }

    public void registerCallback(CameraCallback callbackClass) {
        cameraCallback = callbackClass;
    }

    private double getDegree(int orientation) {
        double degree = 0;

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            degree = 90;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            degree = 180;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            degree = 270;
            //Toast.makeText(context, "Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_UNDEFINED) {
            degree = 0.2;
            //Toast.makeText(context, "Undefined Degree = " + degree, Toast.LENGTH_SHORT).show();
        } else if (orientation == ExifInterface.ORIENTATION_NORMAL) {
            degree = 0.2;
            //Toast.makeText(context, "Normal Degree = " + degree, Toast.LENGTH_SHORT).show();
        }
        return degree;
    }


    public void saveImage(Bitmap newBitmap) {
        try {
            File pictureDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FOLDER_NAME);

            if (!pictureDirectory.exists()) {
                pictureDirectory.mkdirs();
            }

            File file = new File(pictureDirectory.getAbsolutePath() + File.separator + getDateTImeStampForFileName() + ".jpg");
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(file));

            cameraCallback.getFilePath(file.getPath());


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public Bitmap rotateBitmap(Bitmap original, String picturePath) {

        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(picturePath);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int orientation = 0;
        if (exifInterface != null) {
            orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        }

        Matrix matrix = new Matrix();
        matrix.setRotate((float) getDegree(orientation));


        return Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);

    }
}