package com.keoeoetech.pyopyomay.helper;

import androidx.annotation.ColorRes;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Chit Moe Aung on 5/7/2018.
 **/

public class SnackbarHelper {

    public static void showSnack(View attachView, String textStringRes, @ColorRes int backgroundColorRes, @ColorRes int textColorRes) {
        Snackbar snackbar = Snackbar.make(attachView, textStringRes, Snackbar.LENGTH_LONG);

        Snackbar.SnackbarLayout sbView = (Snackbar.SnackbarLayout) snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(attachView.getContext(), backgroundColorRes));
        TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(attachView.getContext(), textColorRes));
        textView.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5.0f, attachView.getContext().getResources().getDisplayMetrics()), 1.0f);

        snackbar.show();
    }

}
