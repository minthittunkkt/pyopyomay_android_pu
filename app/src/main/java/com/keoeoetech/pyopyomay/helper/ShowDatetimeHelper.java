package com.keoeoetech.pyopyomay.helper;

import android.text.format.DateUtils;

import java.util.Calendar;

/**
 * Created by hello on 6/1/17.
 */

public class ShowDatetimeHelper {

    private MyDateFormat myDateFormat;

    public ShowDatetimeHelper()
    {
        myDateFormat = new MyDateFormat();
    }

    public String getDatetime(String datetime) throws java.text.ParseException {

        long dt;
        if(datetime.contains("T"))
        {
            dt = myDateFormat.DATE_FORMAT_YMD_HMS.parse(datetime.replace("T", " ")).getTime();
        }
        else
        {
            dt = myDateFormat.DATE_FORMAT_YMD_HMS.parse(datetime).getTime();
        }

        long dt_now = myDateFormat.DATE_FORMAT_YMD_HMS.parse(myDateFormat.DATE_FORMAT_YMD_HMS.format(Calendar.getInstance().getTime())).getTime();
        CharSequence result = DateUtils.getRelativeTimeSpanString(dt, dt_now, DateUtils.MINUTE_IN_MILLIS);

        return result.toString();
    }


}
