package com.keoeoetech.pyopyomay.helper;


import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmSchema;

/**
 * Created by hello on 12/19/17.
 */

public class MyRealmMigration implements io.realm.RealmMigration {



    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if(schema.get("PyoPyoMayUserModel").hasField("FacebookId") != true)
        {
            schema.get("PyoPyoMayUserModel").addField("FacebookId", String.class);
        }


        oldVersion++;

    }

    private boolean isSchemaExist(RealmSchema schema, String schemaName)
    {
        if(schema.get(schemaName) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
