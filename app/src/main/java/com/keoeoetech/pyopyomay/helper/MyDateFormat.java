package com.keoeoetech.pyopyomay.helper;

import java.text.SimpleDateFormat;

public class MyDateFormat {

    public SimpleDateFormat DATE_FORMAT_YMD;
    public SimpleDateFormat DATE_FORMAT_YMD_HMS;
    public SimpleDateFormat DATE_FORMAT_DMY;
    public SimpleDateFormat DATE_FORMAT_M;
    public SimpleDateFormat DATE_FORMAT_Y;
    public SimpleDateFormat DATE_FORMAT_DMY_TEXT;

    public MyDateFormat() {
        DATE_FORMAT_YMD = new SimpleDateFormat("yyyy-MM-dd");
        DATE_FORMAT_YMD_HMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DATE_FORMAT_DMY = new SimpleDateFormat("dd/MM/yyyy");
        DATE_FORMAT_M = new SimpleDateFormat("MM");
        DATE_FORMAT_Y = new SimpleDateFormat("yyyy");
        DATE_FORMAT_DMY_TEXT = new SimpleDateFormat("dd MMMM, yyyy");
    }

    public String removeTfromServerDate(String datetime) {

        return datetime.replace("T", " ");
    }
}