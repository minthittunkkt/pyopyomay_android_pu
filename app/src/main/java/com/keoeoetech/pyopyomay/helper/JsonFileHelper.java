package com.keoeoetech.pyopyomay.helper;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by minthittun on 1/28/2016.
 */
public class JsonFileHelper {


    public static String getQuzzesFromJson(Context context) throws IOException {

        InputStream is = context.getAssets().open("quizzes.json");

        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        String json = new String(buffer, "UTF-8");
        return json;

    }


    public static String getCountryDialCodeListFromJson(Context context) throws IOException {

        InputStream is = context.getAssets().open("country_dial_code.json");

        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        String json = new String(buffer, "UTF-8");
        return json;

    }
}
