package com.keoeoetech.pyopyomay.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ImageView;

import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;

public class CustomDialogHelper {

    private Dialog myDialog;
    private ImageView img_icon;
    private MyanButton btn_ok;
    private MyanTextView tv_message;

    public CustomDialogHelper(Context context)
    {
        myDialog = new Dialog(context);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.custom_dialog);
        myDialog.getWindow().
                setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(false);

        img_icon = myDialog.findViewById(R.id.img_icon);
        tv_message = myDialog.findViewById(R.id.tv_message);
        btn_ok = myDialog.findViewById(R.id.btn_ok);
        /*btn_ok.setMyanmarText(TextDictionaryHelper.
                getText(context,TextDictionaryHelper.TEXT_BTN_UPDATE));*/
    }

    public void showDialog(int icon, String message, String buttonText)
    {
        btn_ok.setMyanmarText(buttonText);
        img_icon.setImageResource(icon);
        tv_message.setMyanmarText(message);
        myDialog.show();
    }

    public void hideDialog()
    {
        myDialog.dismiss();
    }

    public MyanButton getBtnOk() {
        return btn_ok;
    }


}
