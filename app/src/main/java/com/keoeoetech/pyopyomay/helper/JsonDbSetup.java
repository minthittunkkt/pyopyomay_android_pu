package com.keoeoetech.pyopyomay.helper;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.keoeoetech.pyopyomay.model.AnswersModel;
import com.keoeoetech.pyopyomay.model.CountryDialCodeModel;
import com.keoeoetech.pyopyomay.model.LessonModel;
import com.keoeoetech.pyopyomay.model.LessonQuestionAnswersModel;
import com.keoeoetech.pyopyomay.model.QuestionModel;

import java.io.IOException;
import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by HELLO on 6/23/2016.
 */
public class JsonDbSetup {

    private Context context;
    private Realm realm;

    public JsonDbSetup(Context context) {
        this.context = context;
        realm = Realm.getDefaultInstance();
    }

    public boolean isDbsetupDone() {
        long lessonCount = realm.where(LessonModel.class).count();
        long questionCount = realm.where(QuestionModel.class).count();
        long answerCount = realm.where(AnswersModel.class).count();

        if (lessonCount != 0 && questionCount != 0 && answerCount != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void jsonDatasetup() throws IOException {

        Gson gson = new Gson();
        LessonQuestionAnswersModel lessonQuestionAnswersModel = gson.fromJson(JsonFileHelper.
                        getQuzzesFromJson(context),
                new TypeToken<LessonQuestionAnswersModel>() {
                }.getType());

        ArrayList<LessonModel> lessonList = lessonQuestionAnswersModel.getLesson();
        ArrayList<QuestionModel> qustionList = lessonQuestionAnswersModel.getQuestion();
        ArrayList<AnswersModel> answerList = lessonQuestionAnswersModel.getAnswers();


        for (int i = 0; i < lessonList.size(); i++) {
            LessonModel lessonModel = lessonList.get(i);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(lessonModel);
            realm.commitTransaction();
        }

        for (int i = 0; i < qustionList.size(); i++) {
            QuestionModel questionModel = qustionList.get(i);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(questionModel);
            realm.commitTransaction();
        }

        for (int i = 0; i < answerList.size(); i++) {
            AnswersModel lessonModel = answerList.get(i);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(lessonModel);
            realm.commitTransaction();
        }

    }


    public ArrayList<CountryDialCodeModel> getCountryDialCodeList() throws IOException {

        Gson gson = new Gson();
        ArrayList<CountryDialCodeModel> resultList = gson.fromJson(JsonFileHelper.
                        getCountryDialCodeListFromJson(context),
                new TypeToken<ArrayList<CountryDialCodeModel>>() {
                }.getType());


        return resultList;
    }


}
