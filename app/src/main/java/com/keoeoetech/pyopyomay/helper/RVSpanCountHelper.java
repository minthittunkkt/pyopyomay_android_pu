package com.keoeoetech.pyopyomay.helper;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

/**
 * Created by Wai Phyo Aung on 1/12/2017.
 **/

public class RVSpanCountHelper {

    public static int getSpanCount(WindowManager windowManager) {
        DisplayMetrics dm = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        double wi = (double) width / (double) dm.xdpi;
        double hi = (double) height / (double) dm.ydpi;

        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);

        Log.d("WPA", "screenDiagonalInches: " + screenInches);

        int finalResult = (int) screenInches;

        if (finalResult < 7) {
            return 2;
        } else if (finalResult == 7) {
            return 3;
        } else {
            return 4;
        }
    }

}
