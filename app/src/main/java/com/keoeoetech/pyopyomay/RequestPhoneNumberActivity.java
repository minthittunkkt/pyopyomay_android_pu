package com.keoeoetech.pyopyomay;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.keoeoetech.pyopyomay.adapter.CountryDialCodeAdapter;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanBoldTextView;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanEditText;
import com.keoeoetech.pyopyomay.helper.JsonDbSetup;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import java.io.IOException;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestPhoneNumberActivity extends BaseActivity {

    @BindView(R.id.tv_phone)
    MyanBoldTextView tv_phone;
    @BindView(R.id.edt_phone)
    MyanEditText edt_phone;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.btn_login)
    MyanButton btn_login;

    private CountryDialCodeAdapter countryDialCodeAdapter;

    private PhoneNumberUtil phoneNumberUtil;
    private TelephonyManager telephonyManager;
    private JsonDbSetup jsonDbSetup;
    private Call<PyoPyoMayUserModel> pyoPyoMayUserModelCall;

    private ProgressDialog progressDialog;
    ServiceHelper.ApiService service;
    private SharePreferenceHelper sharePreferenceHelper;
    private String phoneNo;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_request_phone_number;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {
        init();

        btn_login.setOnClickListener(v -> {


            if (!edt_phone.getText().toString().equals("")) {

                if (validatePhone(edt_phone.getText().toString())) {

                    //Log.d("Phone Number in RPN: ", edt_phone.getText().toString());

                    try {
                        String callingCode = jsonDbSetup.getCountryDialCodeList().get(spinner_country.getSelectedItemPosition()).getCallingCode();
                        char first = edt_phone.getText().toString().charAt(0);
                        if (first == '0') {
                            phoneNo = edt_phone.getText().toString().substring(1);
                        } else {
                            phoneNo = edt_phone.getText().toString();
                        }

                        String finalPhoneNo = "+" + callingCode + "" + phoneNo;


                        progressDialog.show();
                        pyoPyoMayUserModelCall = service.getByPhone(finalPhoneNo);
                        pyoPyoMayUserModelCall.enqueue(new Callback<PyoPyoMayUserModel>() {
                            @Override
                            public void onResponse(Call<PyoPyoMayUserModel> call, Response<PyoPyoMayUserModel> response) {

                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    PyoPyoMayUserModel pyoPyoMayUserModel = response.body();
                                    Realm.getDefaultInstance().beginTransaction();
                                    Realm.getDefaultInstance().copyToRealmOrUpdate(pyoPyoMayUserModel);
                                    Realm.getDefaultInstance().commitTransaction();

                                    sharePreferenceHelper.setLogin(pyoPyoMayUserModel.getPhoneNumber(), pyoPyoMayUserModel.getMemberId());
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    }
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<PyoPyoMayUserModel> call, Throwable t) {

                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


                else
                {
                    Toast.makeText(getApplicationContext(), "Invalid Phone Number", Toast.LENGTH_SHORT).show();
                }
            }

        });

    }

    private void init() {
        setupToolbar(false);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading ....");
        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        //facebookId=getIntent().getStringExtra("FacebookID");

        phoneNumberUtil = PhoneNumberUtil.getInstance();
        telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        jsonDbSetup = new JsonDbSetup(this);

        countryDialCodeAdapter = new CountryDialCodeAdapter(this, R.layout.item_country_spinner);
        spinner_country.setAdapter(countryDialCodeAdapter);
        try {
            countryDialCodeAdapter.addAll(jsonDbSetup.getCountryDialCodeList());
            selectValue(spinner_country, getCountryCodeFromSim());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getCountryCodeFromSim() {
        try {
            return telephonyManager.getSimCountryIso();
        } catch (Exception e) {
            return "";
        }
    }

    private void selectValue(Spinner spinner, Object value) throws IOException {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (jsonDbSetup.getCountryDialCodeList().get(i).getCode().toUpperCase().equals(value.toString().toUpperCase())) {
                spinner.setSelection(i);
                break;
            }
        }
    }


    public static boolean validatePhone (String phoneNo) {

        if ((phoneNo.matches("^[0][9][2-9][0-9]{6,11}$")))
            return true;
        else
            return false;


    }


}

