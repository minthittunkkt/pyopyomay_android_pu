package com.keoeoetech.pyopyomay;

import android.app.ProgressDialog;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanEditText;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.MyProgressDialog;
import com.keoeoetech.pyopyomay.helper.OTPServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.model.OTPCodeModel;
import com.keoeoetech.pyopyomay.otp.AppSignatureHashHelper;
import com.keoeoetech.pyopyomay.receiver.SmsBroadcastReceiver;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPCodeActivity extends BaseActivity implements SmsBroadcastReceiver.OTPReceiverInterface {

    @BindView(R.id.edt_otp_code)
    MyanEditText edt_otp_code;
    @BindView(R.id.tv_timer)
    MyanTextView tv_timer;
    @BindView(R.id.btn_resent_otp)
    MyanButton btn_resent_otp;

    private OTPCodeModel otpCodeModel;
    private OTPServiceHelper.ApiService service;
    private Call<OTPCodeModel> otpCodeModelCall;
    private MyProgressDialog progressDialog;
    private String phone;
    private String hashkey;
    private SharePreferenceHelper sharePreferenceHelper;
    private AppSignatureHashHelper appSignatureHashHelper;
    private SmsBroadcastReceiver smsReceiver;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_otpcode;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
    }

    private void init() {
        setupToolbar(true);
        setupToolbarText(" ");
        getSupportActionBar().hide();
        phone = getIntent().getStringExtra("phone");
        progressDialog = new MyProgressDialog();
        otpCodeModel = new OTPCodeModel();
        service = OTPServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        appSignatureHashHelper = new AppSignatureHashHelper(this);
        startSMSListener();
        requestOTPcode(phone);

    }

    private void startSMSListener() {
        try {
            smsReceiver = new SmsBroadcastReceiver();
            smsReceiver.setOtpReceiveInterface(this);

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
            this.registerReceiver(smsReceiver, intentFilter);

            SmsRetrieverClient client = SmsRetriever.getClient(this);

            Task<Void> task = client.startSmsRetriever();
            task.addOnSuccessListener(aVoid -> {
                // API successfully started
            });

            task.addOnFailureListener(e -> {
                // Fail to start API
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void requestOTPcode(String phone) {
        progressDialog.show(this,"Loading...");

        if (appSignatureHashHelper.getAppSignatures().get(0).isEmpty()) {
            hashkey = "ABlcQHwZykT";
        } else {
            hashkey = appSignatureHashHelper.getAppSignatures().get(0);
        }
        otpCodeModelCall = service.getRequestCode(phone, hashkey);
        otpCodeModelCall.enqueue(new Callback<OTPCodeModel>() {
            @Override
            public void onResponse(Call<OTPCodeModel> call, Response<OTPCodeModel> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    OTPCodeModel otpCodeModel = response.body();
                    if (otpCodeModel != null && otpCodeModel.getCode().equals("002")) {

                        Toast.makeText(OTPCodeActivity.this, "OTP code request success. ", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(OTPCodeActivity.this, "OTP code request failed. ", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<OTPCodeModel> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(OTPCodeActivity.this, "OTP code request failed.", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onOTPReceived(String otp) {
        String[] part = otp.split(":");
        if (part.length == 2) {
            String[] partOne = part[1].split(" ");
            if (partOne.length == 3) {
                String code = partOne[1];
                edt_otp_code.setText(code);
                sentOTPToApi(phone, code);
            }
        }
    }

    private void sentOTPToApi(String phone, String code) {

        otpCodeModelCall=service.getVerifyCode(phone, Integer.parseInt(code));
        otpCodeModelCall.enqueue(new Callback<OTPCodeModel>() {
            @Override
            public void onResponse(Call<OTPCodeModel> call, Response<OTPCodeModel> response) {

                if (response.isSuccessful()) {
                    OTPCodeModel otpCodeModel = response.body();
                    if (otpCodeModel != null && otpCodeModel.getCode().equals("002")) {

                        Toast.makeText(OTPCodeActivity.this, "OTP code sent success. ", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(OTPCodeActivity.this, "OTP code sent failed. ", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<OTPCodeModel> call, Throwable t) {
                t.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(OTPCodeActivity.this, "Opt code sent failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onOTPTimeOut() {
        Toast.makeText(getApplicationContext(), "Time out",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOTPReceivedError(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(smsReceiver);
    }

}
