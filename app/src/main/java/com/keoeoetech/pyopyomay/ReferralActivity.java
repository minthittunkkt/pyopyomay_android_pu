package com.keoeoetech.pyopyomay;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;

import net.glxn.qrgen.android.QRCode;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;

public class ReferralActivity extends BaseActivity {

    @BindView(R.id.pg_bar)
    ProgressBar pg_bar;

    @BindView(R.id.web_view)
    WebView web_view;

    @BindView(R.id.qr_code)
    ImageView qr_code;

    private SharePreferenceHelper sharePreferenceHelper;
    private String imei;
    private String mURL;
    private String mQuery;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_referral;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Referral (ညွှန်းသူ)");

        PyoPyoMay.getInstance().trackScreenView
                (PyoPyoMayConstant.REFERRAL_ACTIVITY);

        sharePreferenceHelper = new SharePreferenceHelper(this);
        imei = Settings.Secure.getString(getApplicationContext()
                        .getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Bitmap myBitmap = QRCode.from(sharePreferenceHelper.getPhoneNo()
                + "," + imei).bitmap();
        qr_code.setImageBitmap(myBitmap);

        mURL = PyoPyoMayConstant.REFERRAL_URL;
        try{
            mQuery = URLEncoder.encode(sharePreferenceHelper.getPhoneNo(),
                    "UTF-8");
        }catch(UnsupportedEncodingException e){ // Catch the encoding exception
            e.printStackTrace();
        }
        mURL += mQuery;

        web_view.getSettings().setLoadsImagesAutomatically(true);
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web_view.loadUrl(mURL);
        web_view.setWebViewClient(new myURl());

        web_view.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress) {
                if(progress < 100 && pg_bar.getVisibility() == ProgressBar.GONE){
                    pg_bar.setVisibility(ProgressBar.VISIBLE);
                }

                pg_bar.setProgress(progress);
                if(progress == 100) {
                    pg_bar.setVisibility(ProgressBar.GONE);
                }
            }
        });

        Log.d("referralUrl", mURL);

    }

    public class myURl extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_referral, menu);
        setupOptionMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_scan_qr_code){

            Intent intent = new Intent(getApplicationContext(), ReferralCodeScannerActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (!checkWriteExternalStoragePermission() && !checkReadExternalStoragePermission()
                && !checkCameraPermission() && checkReadPhoneStatePermission()) {
            ActivityCompat.requestPermissions(ReferralActivity.this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.CAMERA}, 1);
        }

        web_view.reload();
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkReadPhoneStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}
