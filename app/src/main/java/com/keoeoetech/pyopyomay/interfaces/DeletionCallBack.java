package com.keoeoetech.pyopyomay.interfaces;

import com.keoeoetech.pyopyomay.model.NotificationModel;

public  interface DeletionCallBack {

    void delete(NotificationModel model);

}
