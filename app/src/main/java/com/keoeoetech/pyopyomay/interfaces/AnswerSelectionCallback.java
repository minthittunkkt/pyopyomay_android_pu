package com.keoeoetech.pyopyomay.interfaces;


import com.keoeoetech.pyopyomay.model.AnswersViewModel;

/**
 * Created by hello on 4/13/17.
 */

public interface AnswerSelectionCallback {

    void select(int position, AnswersViewModel model);

}
