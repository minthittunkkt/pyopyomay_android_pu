package com.keoeoetech.pyopyomay;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanEditText;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileUpdateActivity extends BaseActivity {

    @BindView(R.id.edt_name)
    MyanEditText edt_name;

    @BindView(R.id.edt_dob)
    MyanEditText edt_dob;

    @BindView(R.id.btn_update)
    MyanButton btn_update;

    private SharePreferenceHelper sharePreferenceHelper;
    private MyDateFormat myDateFormat;
    private ProgressDialog progressDialog;

    Call<PyoPyoMayUserModel> callUser;
    Call<PyoPyoMayUserModel> callUpdate;
    ServiceHelper.ApiService service;

    PyoPyoMayUserModel oldModel;
    int mYear, mMonth, mDay;
    int age;
    DatePickerDialog datePickerDialog;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile_update;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();
        getData();

        btn_update.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_BTN_UPDATE));
        edt_dob.setOnClickListener(v -> {

            if(edt_dob.getMyanmarText()== null) {

                final Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, 1990);
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
            }

            else {
                final Calendar c = Calendar.getInstance();
                try {
                    c.setTime(myDateFormat.DATE_FORMAT_DMY.parse(edt_dob.getMyanmarText()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                Log.d("THN","DOB Year : "+ mYear);

            }


            datePickerDialog = new DatePickerDialog(ProfileUpdateActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT,

                    (view, year, monthOfYear, dayOfMonth) -> {


                        edt_dob.setMyanmarText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + (year));


                        Log.d("THN","DOb year :"+mYear);

                        if (getAgeGroup(edt_dob.getMyanmarText()) == "") {
                            edt_dob.setMyanmarText("");
                        }


                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            datePickerDialog.show();

        });

        btn_update.setOnClickListener(v -> {

            PyoPyoMayUserModel pyoPyoMayUserModel = getDataFromControl();

            if (isValid()) {
                progressDialog.show();
                callUpdate = service.updateMemberProfile(pyoPyoMayUserModel);
                callUpdate.enqueue(new Callback<PyoPyoMayUserModel>() {
                    @Override
                    public void onResponse(Call<PyoPyoMayUserModel> call, Response<PyoPyoMayUserModel> response) {

                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            Realm.getDefaultInstance().beginTransaction();
                            Realm.getDefaultInstance().copyToRealmOrUpdate(response.body());
                            Realm.getDefaultInstance().commitTransaction();

                            //Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplication(), TextDictionaryHelper.getText(getApplicationContext(), TextDictionaryHelper.TEXT_PROFILE_SETUP), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<PyoPyoMayUserModel> call, Throwable t) {

                        progressDialog.dismiss();

                    }
                });

            }


        });


    }

    private void init() {
        setupToolbar(true);
        setupToolbarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_PROFILE_SETUP));

        PyoPyoMay.getInstance().trackScreenView
                (PyoPyoMayConstant.PROFILE_SET_UP_ACTIVITY);

        edt_name.setHint(MyanTextProcessor.processText(this, TextDictionaryHelper.getText(getApplicationContext(), TextDictionaryHelper.TEXT_ENTER_NAME)));
        edt_dob.setHint(MyanTextProcessor.processText(this, TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_ENTER_BIRTHDAY)));
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading .....");

        service = ServiceHelper.getClient(this);
        sharePreferenceHelper = new SharePreferenceHelper(this);
        myDateFormat = new MyDateFormat();

    }

    private void getData() {
        progressDialog.show();
        callUser = service.getByMemberId(sharePreferenceHelper.getLoginMemberId());
        callUser.enqueue(new Callback<PyoPyoMayUserModel>() {
            @Override
            public void onResponse(Call<PyoPyoMayUserModel> call, Response<PyoPyoMayUserModel> response) {

                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    oldModel = response.body();
                    bindData(response.body());
                }
            }

            @Override
            public void onFailure(Call<PyoPyoMayUserModel> call, Throwable t) {

                progressDialog.dismiss();

            }
        });
    }

    private void bindData(PyoPyoMayUserModel model) {
        edt_name.setMyanmarText(model.getMemberName());

        if (model.getDOB() != null) {
            if (!model.getDOB().equals("")) {
                try {
                    edt_dob.setMyanmarText(myDateFormat.DATE_FORMAT_DMY.format(myDateFormat.DATE_FORMAT_YMD.parse(model.getDOB().toString())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private PyoPyoMayUserModel getDataFromControl() {

        PyoPyoMayUserModel model = new PyoPyoMayUserModel();

        if (oldModel != null) {
            model = oldModel;
        }

        model.setMemberName(edt_name.getMyanmarText());
        try {
            model.setDOB(myDateFormat.DATE_FORMAT_YMD.format(myDateFormat.DATE_FORMAT_DMY.parse(edt_dob.getText().toString())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        model.setUserType(getAgeGroup(edt_dob.getText().toString()));

        Log.d("ProfileInfo", "Name: " + model.getMemberName() + "\nDOB: " + model.getDOB() + "\nAge: " + getAgeGroup(edt_dob.getText().toString()));

        return model;
    }

    private String getAgeGroup(String dob) {
        final Calendar c = Calendar.getInstance();
        try {
            c.setTime(myDateFormat.DATE_FORMAT_DMY.parse(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String agegroup = "";
        int age = getAge(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        if (age < 12) {

        } else {
            if (age == 12 || age <= 15) {
                agegroup = "youngteen";
            } else if (age == 16 || age <= 19) {
                agegroup = "oldteen";
            } else if (age >= 20) {
                agegroup = "adult";
            }
        }

        return agegroup;
    }

    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }


    private boolean isValid() {

        if (edt_name.getMyanmarText().equals("")) {
            Toast.makeText(this, MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_PERIOD_DATA_FILL)), Toast.LENGTH_SHORT).show();

            return false;
        }
        if (edt_dob.getMyanmarText().equals("")) {
            Toast.makeText(this, MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_PERIOD_DATA_FILL)), Toast.LENGTH_SHORT).show();

            return false;
        }
        if (age < 12) {
            System.out.println("Age " + age);

            Toast.makeText(getApplicationContext(), MyanTextProcessor.processText(getApplicationContext(),
                    TextDictionaryHelper.getText(getApplicationContext(),
                            TextDictionaryHelper.TEXT_AGE)), Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;

    }

}
