package com.keoeoetech.pyopyomay;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;

import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.ShowDatetimeHelper;
import com.keoeoetech.pyopyomay.model.NotificationModel;

import java.text.ParseException;

import butterknife.BindView;
import io.realm.Realm;

public class NotificationDetailActivity extends BaseActivity{

    @BindView(R.id.tv_content)
    MyanTextView tv_content;

    @BindView(R.id.tv_datetime)
    MyanTextView tv_datetime;

    private ShowDatetimeHelper showDatetimeHelper;

    private Realm realm;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notification_detail;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        getDetail(getIntent().getStringExtra("id"));

    }

    private void init()
    {
        setupToolbar(true);
        setupToolbarText("Notification");

        realm = Realm.getDefaultInstance();
        showDatetimeHelper = new ShowDatetimeHelper();
    }

    private void getDetail(String id)
    {
        NotificationModel model = realm.where(NotificationModel.class)
                .equalTo("id", id).findFirst();

        realm.beginTransaction();
        model.setSeen(true);
        realm.copyToRealmOrUpdate(model);
        realm.commitTransaction();


        tv_content.setMyanmarText(model.getContent());
        try {
            tv_datetime.setMyanmarText(showDatetimeHelper.getDatetime(model.getDatetime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            if(getIntent().getBooleanExtra("inapp", false))
            {
                finish();
            }
            else
            {
                Intent intent = new Intent(this, MainActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                startActivity(intent);
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        if(getIntent().getBooleanExtra("inapp", false))
        {
            super.onBackPressed();
        }
        else
        {
            Intent intent = new Intent(this, MainActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            startActivity(intent);
        }

    }
}
