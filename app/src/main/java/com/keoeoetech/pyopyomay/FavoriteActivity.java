package com.keoeoetech.pyopyomay;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.keoeoetech.pyopyomay.adapter.FavoriteAdapter;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.dialog.MySavedArticlesDeleteDialog;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;
import com.keoeoetech.pyopyomay.model.ArticleModel;

import java.util.List;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmResults;

public class FavoriteActivity extends BaseActivity implements FavoriteAdapter.MySavedArticleClickListener {

    @BindView(R.id.rv_favorite)
    RecyclerView rv_favorite;
    FavoriteAdapter favoriteAdapter;

    @BindView(R.id.savePostEmptyView)
    RelativeLayout savePostEmptyView;
    @BindView(R.id.tv_empty)
    MyanTextView tv_empty;
    private Realm realm;

    private RealmResults<ArticleModel> results;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_favorite;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

        getAllSavedNewsFeed();
    }

    private void init() {

        setupToolbar(true);

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.FAVORITE_FRAGMENT);

        realm = Realm.getDefaultInstance();

        favoriteAdapter = new FavoriteAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_favorite.setLayoutManager(linearLayoutManager);
        rv_favorite.setAdapter(favoriteAdapter);
        favoriteAdapter.setListener(this);

    }

    private void getAllSavedNewsFeed() {

        results = realm.where(ArticleModel.class).findAll();

        if (!results.isEmpty()) {
            setupFavoriteData(results);
        }

        results.addChangeListener(articleModels -> {
            Log.d("RM", "onChange: " + articleModels.size());

            setupFavoriteData(articleModels);
        });
    }

    private void setupFavoriteData(List<ArticleModel> articleModels) {
        favoriteAdapter.clear();

        for (ArticleModel model : articleModels) {

            favoriteAdapter.add(model);

            Log.d("CMA", "getAllSavedNewsFeed: " + model.getTitle());
        }

        if (articleModels.size() == 0) {
            //favoriteAdapter.showEmptyView(R.layout.empty_layout);

            tv_empty.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_EMPTY_DATA));
            savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRemoveButtonClicked(final ArticleModel model) {


        PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.FAVORITE_FRAGMENT,
                PyoPyoMayConstant.FAVORITE_DELETE_CLICK,
                String.valueOf(model.getUniqueKey()));

        Log.i("CMA", "onRemoveButtonClicked: " + model.getTitle());
        MySavedArticlesDeleteDialog savedArticlesDeleteDialog = new MySavedArticlesDeleteDialog();
        savedArticlesDeleteDialog.setListener(() -> {
            final ArticleModel results = realm.where(ArticleModel.class).equalTo("UniqueKey",
                    model.getUniqueKey()).findFirst();

            if (results != null) {


                realm.executeTransaction(realm -> {
                    results.deleteFromRealm();
                    favoriteAdapter.clear();
                    favoriteAdapter.notifyDataSetChanged();

                });
            } else {
                Log.d("THN", "onRemoveButtonClicked: deleted ID not found");
            }
        });

        savedArticlesDeleteDialog.show(getSupportFragmentManager(),
                savedArticlesDeleteDialog.getClass().getName());

    }


    @Override
    public void onResume() {
        super.onResume();

        if (results.size() == 0) {
            //favoriteAdapter.showEmptyView(R.layout.empty_layout);

            tv_empty.setMyanmarText(TextDictionaryHelper.getText(this, TextDictionaryHelper.TEXT_EMPTY_DATA));
            savePostEmptyView.setVisibility(View.VISIBLE);

        } else {
            savePostEmptyView.setVisibility(View.GONE);
        }
    }

}
