package com.keoeoetech.pyopyomay.dialog;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.github.chrisbanes.photoview.PhotoView;

import com.keoeoetech.pyopyomay.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Chit Moe Aung on 5/14/2018.
 **/

public class ImageViewDialog extends DialogFragment {

    private static final String ARG_NEWS_FEED_IMAGE = "arg_news_feed_image";

    private PhotoView photoView;
    private ProgressBar loadingProgress;

    private String imageUrl;

    public static ImageViewDialog newInstance(String imageUrl) {
        Bundle args = new Bundle();
        args.putString(ARG_NEWS_FEED_IMAGE, imageUrl);

        //System.out.println("Image Url :" + imageUrl);

        ImageViewDialog dialog = new ImageViewDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);

        imageUrl = getArguments().getString(ARG_NEWS_FEED_IMAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_news_feed_image, container);

        init(view);
        loadImage();

        return view;
    }

    private void init(View view) {
        photoView = view.findViewById(R.id.dialog_news_feed_image_pv_image);
        loadingProgress = view.findViewById(R.id.dialog_news_feed_image_loading_progress);
    }

    private void loadImage() {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(getActivity())
                    .load(imageUrl.trim())
                    .error(R.mipmap.place_holder)
                    .into(photoView, new ImageLoaderCallback(loadingProgress) {
                        @Override
                        public void onSuccess() {
                            if (this.progressBar != null) {
                                this.progressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError() {
                            if (this.progressBar != null) {
                                this.progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
        }
    }

    private class ImageLoaderCallback implements Callback {

        ProgressBar progressBar;

        ImageLoaderCallback(ProgressBar progressBar) {
            this.progressBar = progressBar;
        }

        @Override
        public void onSuccess() {
            // Do Nothing
        }

        @Override
        public void onError() {
            // Do Nothing
        }
    }
}
