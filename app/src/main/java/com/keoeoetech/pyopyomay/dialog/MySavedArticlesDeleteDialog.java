package com.keoeoetech.pyopyomay.dialog;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.TextDictionaryHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MySavedArticlesDeleteDialog extends DialogFragment implements View.OnClickListener {

    @BindView(R.id.tv_message)
    MyanTextView tv_message;
    @BindView(R.id.img_icon)
    ImageView img_icon;
    @BindView(R.id.btn_ok)
    MyanButton btn_ok;
    @BindView(R.id.btn_cancel)
    MyanButton btn_cancel;

    private MySavedArticlesDeleteDialog dialog;


    private OnMySavedArticlesDeleteDialogListener listener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_with_ok_cancel_button,
                container);
        ButterKnife.bind(this, view);

        init();
        setData();

        return view;
    }

    private void setData() {

        img_icon.setImageResource(R.mipmap.ask_delete_512);
        tv_message.setMyanmarText(TextDictionaryHelper.getText(getContext(), TextDictionaryHelper.TEXT_DIALOG_TITLE));
        btn_ok.setMyanmarText(TextDictionaryHelper.getText(getContext().getApplicationContext(), TextDictionaryHelper.TEXT_BTN_OK));
        btn_cancel.setMyanmarText(TextDictionaryHelper.getText(getContext().getApplicationContext(), TextDictionaryHelper.TEXT_BTN_CANCEL));

    }

    private void init() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        btn_ok.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
    }

    public void setListener(OnMySavedArticlesDeleteDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v == btn_cancel) {
            dismiss();
        } else if (v == btn_ok) {
            listener.onDeleteClicked();
            dismiss();
        }
    }

    public interface OnMySavedArticlesDeleteDialogListener {
        void onDeleteClicked();
    }
}