package com.keoeoetech.pyopyomay.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.keoeoetech.pyopyomay.NotificationDetailActivity;
import com.keoeoetech.pyopyomay.R;
import com.keoeoetech.pyopyomay.custom_control.MyanTextProcessor;
import com.keoeoetech.pyopyomay.helper.MyDateFormat;
import com.keoeoetech.pyopyomay.model.NotificationModel;

import java.util.Calendar;
import java.util.UUID;

import io.realm.Realm;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant.NOTI_TYPE_FCM;


public class FireBaseMessagingService extends FirebaseMessagingService {

    private static int count = 0;
    private MyDateFormat myDateFormat;
    private static final String CHANNEL_ID
            = "com.singhajit.notificationDemo.channelId";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {

            Log.d("Remote Message", remoteMessage.getData().get("message"));

            myDateFormat = new MyDateFormat();

            NotificationModel model = new NotificationModel();
            model.setId(UUID.randomUUID().toString());
            model.setContent(remoteMessage.getData().get("message"));
            model.setDatetime(myDateFormat.DATE_FORMAT_YMD_HMS.format(Calendar.getInstance().getTime()));
            model.setSeen(false);
            model.setType(NOTI_TYPE_FCM);

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();

            createNotification(remoteMessage.getData().get("message"), model.getId());
        }
    }

    private void createNotification(String messageBody, String id) {
        Intent intent = new Intent(this, NotificationDetailActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("inapp", false);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultIntent = PendingIntent.getActivity(this,
                0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setAutoCancel(true);

        Notification notification = builder
                .setContentTitle("Notification message")
                .setContentText(MyanTextProcessor.processText(getApplicationContext(),messageBody))
                .setTicker("New Message Alert!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(resultIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notification);


       /* Uri notificationSoundURI = RingtoneManager.getDefaultUri
                (RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, "channel_id")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notification message")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(notificationSoundURI)
                        .setContentIntent(resultIntent);*/


        /*NotificationCompat.Builder mNotificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Notification message")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(notificationSoundURI)
                        .setContentIntent(resultIntent);
*/
        /*NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);*/
        // Notification Channel is required for Android O and above
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "channel_id", "channel_name", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("channel description");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }
*/

        /*notificationManager.notify(0, notificationBuilder.build());*/

    }

}
