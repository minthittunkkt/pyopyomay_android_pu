package com.keoeoetech.pyopyomay;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.keoeoetech.pyopyomay.application.PyoPyoMay;
import com.keoeoetech.pyopyomay.common.BaseActivity;
import com.keoeoetech.pyopyomay.custom_control.MyanButton;
import com.keoeoetech.pyopyomay.custom_control.MyanTextView;
import com.keoeoetech.pyopyomay.helper.PyoPyoMayConstant;
import com.keoeoetech.pyopyomay.helper.ServiceHelper;
import com.keoeoetech.pyopyomay.helper.SharePreferenceHelper;
import com.keoeoetech.pyopyomay.model.PyoPyoMayUserModel;

import butterknife.BindView;
import retrofit2.Call;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.tv_version)
    MyanTextView tv_version;

    @BindView(R.id.btn_login)
    MyanButton btn_login;

   /* @BindView(R.id.btn_fb_login)
    MyanButton btn_fb_login;

    @BindView(R.id.login_button)
    LoginButton loginButton;*/

    private SharePreferenceHelper sharePreferenceHelper;
    private ServiceHelper.ApiService service;
    private Call<PyoPyoMayUserModel> pyoPyoMayUserModelCall;
    String phoneNo;
    String id;

    private ProgressDialog progressDialog;
    //CallbackManager callbackManager;
    private static final String EMAIL = "email";


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void setUpContents(Bundle savedInstanceState) {

        init();

         /*callbackManager = CallbackManager.Factory.create();
       btn_fb_login.setOnClickListener(v -> {

            if (v == btn_fb_login) {
                loginButton.performClick();
            }
        });


        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                String facebookID=loginResult.getAccessToken().getUserId();
                Intent intent = new Intent(getApplicationContext(), RequestPhoneNumberActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                intent.putExtra("FacebookID",facebookID);
                startActivity(intent);



            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });*/

        btn_login.setOnClickListener(v -> {

            PyoPyoMay.getInstance().trackEvent(PyoPyoMayConstant.LOGIN_ACTIVITY,
                    PyoPyoMayConstant.LOGIN_BUTTON_CLICK, "Login Button Click");
            final Intent intent = new Intent(LoginActivity.this,
                    OTPLoginActivity.class);
            startActivity(intent);

            /*final Intent intent = new Intent(LoginActivity.this,
                    AccountKitActivity.class);
            AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                    new AccountKitConfiguration.AccountKitConfigurationBuilder
                            (LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN);
            UIManager uiManager;
            uiManager = new SkinManager(
                    SkinManager.Skin.CONTEMPORARY,
                    ContextCompat.getColor(LoginActivity.this,
                            R.color.colorPrimaryDark),
                    R.color.colorPrimaryDark,
                    SkinManager.Tint.BLACK,
                    0.05);
            uiManager.setThemeId(R.style.AppTheme);

            configurationBuilder.setUIManager(uiManager);


            intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                    configurationBuilder.build());
            startActivityForResult(intent, 111);
*/
        });

    }

    private void init() {
        setupToolbar(false);
        setupToolbarText(" ");
        getSupportActionBar().hide();

        PyoPyoMay.getInstance().trackScreenView(PyoPyoMayConstant.LOGIN_ACTIVITY);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading ....");

        sharePreferenceHelper = new SharePreferenceHelper(this);
        service = ServiceHelper.getClient(this);

       /* AccountKit.initialize(this, () -> {

        });*/

        try {
            tv_version.setMyanmarText(getString(R.string.txt_version) + " " + getVersionName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        checkPlayServices(this);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage = "";
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Login Success";
                    getAccount();
                }
            }
            //Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void getAccount() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {

                PhoneNumber phoneNumber = account.getPhoneNumber();
                phoneNo = phoneNumber.toString();

                char firstChar = phoneNo.charAt(0);
                if (firstChar == '+') {
                    phoneNo = phoneNo;
                } else {
                    phoneNo = "+" + phoneNo;
                }

                progressDialog.show();
                pyoPyoMayUserModelCall = service.getByPhone(phoneNo);
                pyoPyoMayUserModelCall.enqueue(new Callback<PyoPyoMayUserModel>() {
                    @Override
                    public void onResponse(Call<PyoPyoMayUserModel> call, Response<PyoPyoMayUserModel> response) {

                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            PyoPyoMayUserModel pyoPyoMayUserModel = response.body();
                            Realm.getDefaultInstance().beginTransaction();
                            Realm.getDefaultInstance().copyToRealmOrUpdate(pyoPyoMayUserModel);
                            Realm.getDefaultInstance().commitTransaction();

                            sharePreferenceHelper.setLogin(pyoPyoMayUserModel.getPhoneNumber(), pyoPyoMayUserModel.getMemberId());
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PyoPyoMayUserModel> call, Throwable t) {

                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


            }

            @Override
            public void onError(AccountKitError accountKitError) {
                Log.e("Account Kit", accountKitError.toString());

            }
        });
    }

*/
    private String getVersionName() throws PackageManager.NameNotFoundException {
        PackageManager manager = this.getPackageManager();
        PackageInfo info = manager.getPackageInfo(
                this.getPackageName(), 0);

        return info.versionName;
    }

    public boolean checkPlayServices(Context context) {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int resultCode = api.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (api.isUserResolvableError(resultCode))
                api.getErrorDialog(((Activity) context), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            else {
                Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

}
